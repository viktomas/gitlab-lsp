# GitLab Language Server - TypeScript (Experiment)

The GitLab Language Server provides a common interface for other IDE extensions
to build out GitLab functionality. Currently, the language server
provides support for:

- GitLab Duo Code Suggestions
- GitLab Duo Chat (coming soon)

For bugs and feature requests, open a
[new issue](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/-/issues/new).

[[_TOC_]]

## Introduction

LSP is a technology that provides an abstraction layer between tools that provide
analysis results (language servers) and the "consumer" IDEs (language clients).
It provides a generic interface to provide analysis results in LSP-enabled IDEs
so that the analysis only has to be implemented once while multiple IDEs can benefit
from the analysis.

This is an LSP-based language-server that serves
[GitLab AI Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html)
to LSP-enabled IDEs. Currently, the server supports LSP protocol version `3.17`.
[Language Server Protocol Specification - 3.17](https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/).

This language server leverages the
[`textDocument/completion`](https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#textDocument_completion)
LSP method to serve code suggestions. For most editors this hooks into the intelliSense
functionality which can be often invoked by means of the `[Ctrl]+[SPACE]` `[Ctrl]+[N]` shortcuts.

## Running the LSP

The LSP Client should be responsible for starting the server process and setting up
the connection. The LSP server supports multiple communication methods and it's
required to provide one of them at startup:

- **IPC** - only for communication with Node process - the Server should be a
  Node app. The server is not published yet as an NPM package though, but you can
  use the `out/node/main.js` module if you've compiled the project locally for testing purposes.

  ```shell
    node 'out/node/main.js' --node-ipc
  ```

- **TCP Socket** - provide a socket port when starting the server process. The client
  should be waiting on that port for the server to connect.

  ```shell
    /opt/gitlab-lsp --socket=6789
  ```

- **STDIO**

  ```shell
    /opt/gitlab-lsp --stdio
  ```

- **Named Pipes** - the Language Server will be listening as a named pipe server for the client to connect.

  ```shell
   /opt/gitlab-lsp --pipe=pipeName
  ```

Please, [check for reference and inspiration](https://github.com/microsoft/vscode-languageserver-node/blob/main/client/src/node/main.ts#L339) how the `vscode-languageclient` Node JS library is handling server startup.

## User (Client Developer) documentation

- Download the language server binary from the
  [Package Registry page](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/-/packages/).
  For every release you can download the binary that matches your OS and architecture.
  Binaries are suffixed with `<os>-<arch>` from the list of assets linked in the release.
- Place the binary anywhere on your filesystem. The configuration in this documentation
  assumes that the binary is located under `/opt/gitlab-lsp`.
- The language server requires an access token to authenticate with the GitLab API.
  Create a Personal Access Token (PAT) with the `api` scope or
  OAuth token with the same scope. You'll need to provide this token to the server
  sending the
  [`didChangeConfiguration` notification](https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#workspace_didChangeConfigurationworkspace/didChangeConfiguration)
  from the client to the server after the server is started.
  [Check the details here](docs/supported_messages.md#didchangeconfiguration).
- Make the binary executable using this command in your terminal:

  ```shell
    chmod +x opt/gitlab-lsp
  ```

- The client implementation will depend on your text-editor or IDE. Check the next section.

### Allowing the binary to run on macOS

macOS users might encounter a message saying the binary was killed as soon as they start it in a terminal. This
happens because of a security feature called Gatekeeper. To allow the binary to run, you need to clear its extended
attributes and ad-hoc sign it, which establishes trust with Gatekeeper.

Run these commands in your terminal and enter your password if prompted:

```shell
sudo xattr -c opt/gitlab-lsp
codesign --force -s - opt/gitlab-lsp
```

## LSP client configuration

- [VS Code](https://gitlab.com/shekharpatnaik/gitlab-lsp-test-extension/-/blob/main/client/src/extension.ts?ref_type=heads#L36-87)

_This section is still in progress..._

## Node version

We use the node version defined in the
[Microsoft/vscode project](https://github.com/microsoft/vscode/blob/main/.nvmrc).
However, we also have to make sure that we keep the *major* version the same as
what is supported by the `pkg-fetch` module.

The `pkg-fetch` module provides the node version used by `pkg` when packaging the
language server. Each version of `pkg-fetch` has a static set of node builds.
The versions can be looked up on the
[`pkg-fetch` releases page](https://github.com/vercel/pkg-fetch/releases) of the GitHub project.

To upgrade the version of node in use (make sure that the major version is the same
for `pkg` and the development version):

1. update `pkg` version (if there is a new one)
   1. Upgrade the `pkg-fetch` module by manually editing the `package.json` file's `overrides` section.
      - Note: To get the updated package, make sure to `rm -rf node_modules` and run `npm install`.
   1. Check the [`pkg-fetch` releases page](https://github.com/vercel/pkg-fetch/releases)
      for the node version. Make sure to verify `macos-arm64` builds exist for the selected version.
   1. Update `.gitlab-ci.yml` (`test-pkg-node`) with the new version number
1. update development/VS Code version
   1. Update `.tool-versions` file with the new version number
   1. Update `.gitlab-ci.yml` (`default`) with the new version number
   1. Update `src/tests/int/node_version.test.ts` with the correct version number
   1. Update `package.json` `scripts` section where we reference target node version like this `--target=node18.17`
1. Add a changelog entry

## Development setup

To debug or add new features to the language server:

1. Clone this repo
1. Run `npm install`
1. Compile the server code with `npm run compile` or `npm run watch` to compile when the changes are made

### Watch Mode

Watch mode rebuilds the project automatically with every file change. Optionally, it can also update `gitlab-lsp` dependency in VS Code. For details on implementation, see the [watch mode script](scripts/watcher/watch.ts).

We'll use VS Code as an example. Run the following command:

```shell
npm run watch -- --editor=vscode
```

Then, in VS Code, you can restart your extension development host, and it will automatically have the latest server changes.

#### Script parameters

- `-- --editor=vscode` *(optional)* - use this argument if you want the script to run `yalc` to automatically update `gitlab-lsp` in your VS Code Extension
- `VSCODE_EXTENSION_RELATIVE_PATH` environment variable **(optional)** - Use it if you set `-- --editor=vscode` and the relative path to VS Code Extension is not `../gitlab-vscode-extension`

Supported editors:

- `vscode`

### Starting the server with test VS Code extension

It is easier to test the LSP server together with the test LSP client. Refer to
the [test LSP client (VS Code extension) docs](https://gitlab.com/shekharpatnaik/gitlab-lsp-test-extension#running-the-client)
to see how to setup the test client to run and start the server.

### Starting with other client or without the client

To start the server separately run `npm run debug` or:

```shell
node --inspect=6010 ./out/node/main.js --socket=6789
```

Your client should establish the socket connection and wait on port `6789` for the server to connect.

### Debugging the server

You can do Language Server debugging in multiple ways:

- [Start the Language Server](#start-the-language-server)
- [Connect to the Language Server in the VS Code extension](#connect-to-ls-in-the-vs-code-extension)
- [Connect to the Language Server using Chrome Developer Tools](#connect-to-ls-using-chrome-developer-tools)

#### Start the Language Server

1. Open the project in VS Code.
1. Run the **Start server** launch task.

The launch task starts a Language Server that listens on port `6789` for a client connection,
and connects VS Code debugger to it.

#### Connect to LS in the VS Code extension

Prerequisites:

- If connecting with the VS Code extension, you must have the `code` command configured. See
  [Launching VS Code from the command line](https://code.visualstudio.com/docs/setup/mac#_launching-from-the-command-line) for more information.

1. [Set up the extension project](https://gitlab.com/gitlab-org/gitlab-vscode-extension/blob/main/docs/developer/language-server.md)
   for LS development.
1. [Start the VS Code extension](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/blob/main/CONTRIBUTING.md?ref_type=heads#step-4-running-the-extension-in-desktop-vs-code) in development mode.
1. Use the command line to open this project in VS Code, replacing the path with
   the path of your VS Code Extension project:

   ```shell
   GITLAB_WORKFLOW_PATH=/Users/tomas/workspace/gitlab-vscode-extension code .
   ```

   This command is important. It maps the bundled sources in the extension with the source code in this project.

1. Run the **Attach to VS Code Extension** launch task.

#### Connect to LS using Chrome Developer Tools

Prerequisites:

- Your server must be a Node module, not an executable.

1. Either:
   - [Start the VS Code extension](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/blob/main/CONTRIBUTING.md?ref_type=heads#step-4-running-the-extension-in-desktop-vs-code) in development mode, or
   - [Start the server](#starting-with-other-client-or-without-the-client).
1. Open [`chrome://inspect`](chrome://inspect) in a Chrome browser. For more info and
   debugging tools, see [Inspector Clients](https://nodejs.org/en/docs/guides/debugging-getting-started#inspector-clients)
   in the Node.js documentation.
1. Select **Configure**, and ensure your target host and port are listed. Use `localhost:6010`.
1. Select **Open dedicated DevTools for Node**.
1. Add the `debugger` statement, or add `console.log` in your server code.
1. Recompile after you change the code.

When you start the Language Server, the inspector pauses on the breakpoints.

### Linting documentation

This project follows the documentation guidelines of GitLab and uses `markdownlint`
and `vale` to improve the content added and make sure it adheres to our guidelines.
You can find information on
[installing the linters](https://docs.gitlab.com/ee/development/documentation/testing.html#install-linters) or
[integrating with your editor](https://docs.gitlab.com/ee/development/documentation/testing.html#configure-editors)
in the GitLab docs.

## Bundling

We need to create a self-contained bundled JavaScript file that is started by
VS Code. We use `esbuild` to bundle all the dependencies into one file located in
`out/node/main-bundle.js`.

## Packaging

Run:

```shell
npm run package
```

Your binary will be available in the `bin` folder of the project

## Releasing

Releases consist of deploying an npm package and a corresponding generic package to
the [package registry](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/-/packages).

To release a new version of the packages:

1. Create a new branch `git checkout -b 2023-11-06-release`
1. Run `npm version major/minor/patch`  use `major`/`minor`/`patch` as defined by semantic versioning.
    - This created a new version, updated changelog, and tagged the commit.
1. Push the branch and the tag `git push && git push origin v3.4.5` (assuming you just created `v3.4.5`).
1. Create Merge Request from the `2023-11-06-release` branch, name it `Release LS version 3.4.5` and merge it (no review is necessary as no code has changed).

### Communicate updates to downstream extensions

Updates are communicated to downstream projects via integration epics. An epic
contains upgrade issues for each internal project that consumes the Language Server.
[Example](https://gitlab.com/groups/gitlab-org/editor-extensions/-/epics/2).

Once per week, if there are any new releases that benefit downstream projects
since the last integration epic, a new integration epic will be created.
