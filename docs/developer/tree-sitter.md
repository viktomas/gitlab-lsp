# Tree Sitter

To add support for a new language:

1. Copy the new `tree-sitter-*.wasm` file into `vendor/grammars/`.
1. Update `src/common/parser/grammars` to include the language in `TREE_SITTER_LANGUAGES`.
1. Confirm `npm run compile` succeeds.
1. Confirm `npm run test:unit` succeeds.
1. Confirm `npm run bundle` succeeds.
1. Confirm `npm run package` succeeds.
1. Open an MR and confirm the integration test job passes.
