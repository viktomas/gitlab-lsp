# Contributing to the GitLab Language Server

## Developer Certificate of Origin + License

By contributing to GitLab B.V., You accept and agree to the following terms and
conditions for Your present and future Contributions submitted to GitLab B.V.
Except for the license granted herein to GitLab B.V. and recipients of software
distributed by GitLab B.V., You reserve all right, title, and interest in and to
Your Contributions. All Contributions are subject to the following DCO + License
terms.

[DCO + License](https://gitlab.com/gitlab-org/dco/blob/master/README.md)

All Documentation content that resides under the [docs/ directory](/docs) of this
repository is licensed under Creative Commons:
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

_This notice should stay as the first item in the CONTRIBUTING.md file._

---

Thank you for your interest in contributing to the GitLab Extension for Visual Studio! This guide details how to contribute
to this extension in a way that is easy for everyone. These are mostly guidelines, not rules.
Use your best judgment, and feel free to propose changes to this document in a merge request.

## Code of conduct

We want to create a welcoming environment for everyone who is interested in contributing.
Visit our [Code of Conduct page](https://about.gitlab.com/community/contribute/code-of-conduct/)
to learn more about our commitment to an open and welcoming environment.

## Getting started

### Reporting issues

[Create a new issue from the **Bug** template](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/-/issues/new?issuable_template=Bug).
Follow the instructions in the template.

### Proposing features

[Create a new issue from the **Feature Proposal** template](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/-/issues/new?issuable_template=Feature%20Proposal).
Follow the instructions in the template.

### Your first code contribution?

Read about the extension architecture in the documentation.

For newcomers to the project, check the issues
[labeled with **Accepting merge requests**](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/-/issues?label_name[]=Accepting%20merge%20requests)
to find issues you can potentially work on..
