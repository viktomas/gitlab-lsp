# [4.3.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v4.2.2...v4.3.0) (2024-03-20)


### Features

* Enable streaming of code generation for Java files ([0e26db6](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/0e26db6e2c93668fb516ed0f4e6db619236f2bc4))
* Streaming telemetry ([9919259](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/9919259352a7720347f51f6572774798b89cc162))
* Update the code suggestions context to v2.6.0 ([7819cac](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/7819cac7e53b379e8672d1f0414dcd78db88a148))



## [4.2.2](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v4.2.1...v4.2.2) (2024-03-04)


### Bug Fixes

* improve streaming error logging ([c6bf219](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/c6bf2195adef27da49cdd8d5b3a358b36bc0dcea))



## [4.2.1](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v4.2.0...v4.2.1) (2024-03-01)


### Bug Fixes

* Remove stacktrace from logs omitting an error object ([2b23022](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/2b2302280341de906d8210308df7a122c37ca3ec))



# [4.2.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v4.1.0...v4.2.0) (2024-03-01)


### Features

* improve HTTP error reporting ([fee28f3](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/fee28f3f8e47d40888deb71d2f5983b1cded701f))



# [4.1.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v4.0.0...v4.1.0) (2024-02-29)


### Features

* Support configuration of http agent certificate options ([f1a37f9](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/f1a37f9ce3b0a114f1619325e1c812375327c7f7))



# [4.0.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.33.0...v4.0.0) (2024-02-07)


### chore

* remove support for deprecated code suggestion endpoints ([a23a164](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/a23a16495e19a9010e2a3bb905d6f77382297f5e))


### Features

* Add circuit breaking to streaming ([8123e20](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/8123e2037e1f5ea01a22d86b3b49fc4b5ae11eae))


### BREAKING CHANGES

* Previously we used a different api endpoint
for the code suggestions whereas configured instance version
was lower than 16.3. That endpoint will soon become unavailable
and the code suggestion will be supported only for the versions >= 16.8.0



# [3.33.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.32.0...v3.33.0) (2024-01-25)



# [3.32.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.31.0...v3.32.0) (2024-01-16)


### Bug Fixes

* Support ignoring certificate errors ([59400e8](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/59400e83afcff80d57aff7fa32ae42effdcabb0d))



# [3.31.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.30.0...v3.31.0) (2024-01-11)


### Features

* Move streaming decision to the LS ([bd7ec64](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/bd7ec646f88ee386d5ab118c78d35b124ffa56e5))



# [3.30.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.29.1...v3.30.0) (2024-01-09)


### Bug Fixes

* Improve generation intent detection ([11ee891](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/11ee8917534c0599d540457ac48d44c4b84fc5b4))



## [3.29.1](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.29.0...v3.29.1) (2024-01-05)



# [3.29.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.28.0...v3.29.0) (2023-12-21)


### Features

* Support Web IDE and VSCode browser environments ([42a5ee9](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/42a5ee9e8aaaf553bcdbf5de81a98ba4c242627e))



# [3.28.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.27.1...v3.28.0) (2023-12-20)


### Features

* streaming debouncing ([4d74e18](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/4d74e18ae168d28d7eea012af9127739d15536d2))



## [3.27.1](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.27.0...v3.27.1) (2023-12-19)


### Bug Fixes

* Catch async errors in streaming handler ([c04a228](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/c04a228702d92b12eed99c59ce92edfd26016f6f))


### Features

* Detect completion intent ([9727729](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/972772963ce2c6510e32b8e3f25b259a401d95e6))
* improve completion for neovim ([84b8376](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/84b83768dc500f8b10ac2a2ced482f97d3896e8f))



# [3.27.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.26.0...v3.27.0) (2023-12-19)


### Bug Fixes

* Abstract away platform specific parser initialization ([d2b36a1](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/d2b36a190e7b3c3b01096c84c17fb8f89afd75bf))


### Features

* **code_suggestions:** Added streaming to code suggestions ([04771d6](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/04771d6611c76ba71f28f43a87eacde8378ed1fc))



# [3.26.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.25.0...v3.26.0) (2023-12-18)



# [3.25.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.24.2...v3.25.0) (2023-12-18)


### Bug Fixes

* Fix misplaced anchor ([cd9eaff](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/cd9eaffb860a02c359ab28c502c87aaea21efecb))
* Promote dayjs to dependency from dev dependencies ([beb7188](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/beb718833a40731253ac8e7c89af57e36e330913))


### Features

* **tree-sitter:** Assume suggestion intent based on completion context ([82317eb](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/82317eb2f8f714f73c054cee05f415de6e72ae34))



## [3.24.2](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.24.1...v3.24.2) (2023-12-14)

### Bug fixes

* Reverts [d411ae7](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/d411ae7c3c04273427a73750520ee0e0950b8855) as it caused a cache issue. ([c1c9d7e](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/c1c9d7e5eef40cb2b3bf0c76fa4dc0b10c0a58d3))

## [3.24.1](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.24.0...v3.24.1) (2023-12-13)


### Bug Fixes

* Send abort signal to api ([d411ae7](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/d411ae7c3c04273427a73750520ee0e0950b8855))



# [3.24.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.23.0...v3.24.0) (2023-12-12)


### Features

* Include project path in code suggestion requests ([b3be3e2](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/b3be3e29216cfcf3e485897c72a8d7d96f0b9267))



# [3.23.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.22.0...v3.23.0) (2023-12-12)


### Features

* Do not request suggestion with completion context text mismatch ([4ba34cd](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/4ba34cd07951117d08aa27711beadadb7ec232f4))



# [3.22.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.21.0...v3.22.0) (2023-12-08)


### Features

* **cache:** discard cache entries upon second retrieval ([ed8d2d7](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/ed8d2d74c9cd676a4ab93beb48d540081d7500c6))
* Enable the Code Suggestions cache be default ([af8795a](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/af8795a73242682660c30410d8be342096823422))
* Support execution through npx ([6fbe9f8](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/6fbe9f8c7f97663c5a11599a1007f852fb34d3ec))



# [3.21.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.20.1...v3.21.0) (2023-12-08)


### Bug Fixes

* improve token check logging behavior ([7dd7d5d](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/7dd7d5d79ad237b6f58225f3d6e66d5d0b851da7))


### Features

* add telemetry for cache hits ([e15443c](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/e15443c231edaa7e521b617373230c38fba870f5))
* add timestamped and formatted logger ([de06343](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/de063432f51ba9d9221262541d361bf61df66ac4))
* assume token type by length heuristic during token check ([b679202](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/b6792021765be3d8533fec5d7a54cc4a16d24b15))
* debug log all http fetches ([bfe5a9c](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/bfe5a9c6e2dc7b40e84779f8364601fe1b256f20))
* make log levels filterable ([fc424fc](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/fc424fc409033e09cbbe5ee6770c78d71ffe1e99))



## [3.20.2](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.20.1...v3.20.2) (2023-12-06)


### Bug Fixes

* improve token check logging behavior ([7dd7d5d](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/7dd7d5d79ad237b6f58225f3d6e66d5d0b851da7))


### Features

* assume token type by length heuristic during token check ([b679202](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/b6792021765be3d8533fec5d7a54cc4a16d24b15))



## [3.20.1](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.19.0...v3.20.1) (2023-12-06)

### Bug Fixes

- support non `glpat-` prefixed tokens ([aa37dc3](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/aa37dc33ed03a39eedc3ce86639b390923e124a9))

### Features

- Use completion context in code suggestions ([0ce196a](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/0ce196ada688b012c38467b276ceae38a91e87e4))

# [3.20.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.19.0...v3.20.0) (2023-12-04)

### Features

- add cache configuration ([63c4679](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/63c46794a8cf7fa6fa96527a53d4e76d133b6522))
- add caching of code suggestions response ([a84428c](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/a84428cbbdac54773867f4a196738d908cdedb80))

# [3.19.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.18.1...v3.19.0) (2023-12-03)

### Bug Fixes

- Revert Reject inline completions with intellisense ([614d895](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/614d895fb0c4ecf589812759d32467802d079b10))

## [3.18.1](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.18.0...v3.18.1) (2023-12-01)

### Bug Fixes

- the LS only needs api scope, read_user is redundant ([ea4a9ee](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/ea4a9ee71afedebd527ce4f9d3c7f573d5f3e042))

# [3.18.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.17.0...v3.18.0) (2023-11-30)

### Features

- Reject inline completions with intellisense context ([7726054](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/7726054f9831001b9d090c3bb055953a59bb3b3c))

# [3.17.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.16.1...v3.17.0) (2023-11-29)

### Bug Fixes

- inline completion items missing range ([259b2c8](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/259b2c895ff12a756404e1dcf677ab4f92bcafc5))

### Features

- add debouncing and cancellation to LS ([93a33c5](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/93a33c5903fb0de10a59e3ef1392da00cf32f112))
- **telemetry:** only autoreject if client sends accepted events ([9e0d8cf](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/9e0d8cf0aa911afd95a0399f8e2498af251c5ba6))

## [3.16.1](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.16.0...v3.16.1) (2023-11-24)

- Exports check token notification type for the VS Code Extension

# [3.16.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.15.0...v3.16.0) (2023-11-24)

### Bug Fixes

- Fix browser build ([f9f65d9](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/f9f65d9ab2231faa3d94b47641c3cddb8dca1ec1))
- remove hello notification ([015304d](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/015304db38bc85944889f73dee68fbe3899a7551))

### Features

- Add Circuit Breaker ([f933c55](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/f933c55adf9fe8946ffbc1a4e35b8378ef053825))
- Notify Clients about API error/recovery ([34027eb](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/34027ebe6b98489554503f252aa452e84095ea2f))
- remove unnecessary console log ([83f16be](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/83f16bea76d3cf4ae91979616405d7dd12a29c58))
- **telemetry:** Implement "suggestion rejected" logic ([5d7815f](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/5d7815f752e705b7cc12ca77b99aedaa8a0b8d3f))

# [3.15.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.14.0...v3.15.0) (2023-11-13)

### Bug Fixes

- Restore missing ajv dependency ([208a6ad](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/208a6ad24b38deb30a6a3d1f91491e99d241bcc7))

### Features

- Handle empty suggestion text on the LS side ([e1162f2](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/e1162f2f6cdd11422de7359ef7eeaf983dd849e7))

# [3.14.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.13.0...v3.14.0) (2023-11-10)

### Features

- add accept suggestion command ([a2b3e7c](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/a2b3e7c2ab7919fb5a1977c06fea2b1210c761cf))

# [3.13.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.12.1...v3.13.0) (2023-11-09)

- Swap a node dependency that would prevent web contexts from using the bundle (!94)

## [3.12.1](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.12.0...v3.12.1) (2023-11-06)

- No functional changes, only testing the new release process (!81)

## v3.12.0

- Allow clients to track `suggestion_not_provided` (!83)

## v3.11.0

- Track `language_server_version` with telemetry events (!78)

## v3.10.0

- Support `textDocument/inlineCompletion` message (!75)

## v3.9.0

- Only provide suggestions when cursor is at or near end of a line. (!61)
- Validate Snowplow events against the schema before tracking (!55)

## v3.8.0

- Fix issue where partial config updates used wrong values (!65) (also refactors LS configuration)

## v3.7.0

- Support using an HTTP proxy for connections (!35)
- Fix duplicated Snowplow events (!67)

## v3.6.0

- Bundle whole LS into one JS file to be used in VS Code (!62)

## v3.5.0

- Update Snowplow event `code_suggestions_context` schema to v2-1-0 (!64)
- Make Secret redaction respect the config setting, also enable it in the browser build (!57)

## v3.4.0

- Handle better error response from the Code Suggestions server (!56)

## v3.3.0

- Don't make suggestions for short content (!30)
- asdf `.tool-versions` file added for nodejs v18.16.0 (!47)

## v3.2.0

- Send all console messages to STDERR (!45)
- Disable snowplow events when host cannot be resolved (!45)
- Update `code_suggestions_context` schema to `v2-0-1` (!43)

## v3.1.0

- Add `suggestion_not_provided` telemetry event when suggestions are returned empty (!38)
- Allow Client to detect the `suggestion_shown` event (!38)

## v3.0.0

- Use custom `ide` and `extension` initialization parameters for telemetry (!32)

## v2.2.1

- Rely on `model.lang` value in the response from Code Suggestions server to set `language` property for the Code Suggestions telemetry context (!34)

## v2.2.0

- Enable Code Suggestions telemetry by default (!41)

## v2.1.0

- Send the relative file path to the Code suggestions server (!29)
- Update `appId` for Snowplow tracking (!36)

## v2.0.0

- Add Snowplow tracking library (!25)
- Add Code Suggestions telemetry (!27)
- Move all Client settings handling to the `DidChangeConfiguration` notification handler (!27)

## v1.0.0

- Update `token/check` notification to `$/gitlab/token/check` (!17)
- Document required and optional messages for server & client (!17)
- Document initialize capabilities (!17)
- Check that `completion` is supported by the client (!17)

## v0.0.8

- Bumping version to keep packages in sync (!22)

## v0.0.7

- Revert `re2` usage as it was causing issues with some platforms (!20)

## v0.0.6

- Fix npm package publishing
- Refactor TS build to accommodate WebWorker LSP

## v0.0.5

- Start publishing an npm package

## v0.0.4

- Add new code suggestions endpoint (!12)
- Add token check (!13)
- Subscribe to document sync events and publish diagnostics (empty for now) (!15)
- Use `re2` to work with Gitleaks regex (!16)

## v0.0.3

- Documenting server startup configuration and capabilities (!8)
- Bug fix for the code suggestions (!8)

## v0.0.2

- Easier build and publish (!10)
- Refactor for browser entrypoint (!6)
- Add secrets redaction (!7)

## v0.0.1

- Base version
