import fetch from 'cross-fetch';
import http from 'http';
import https from 'https';
import { Fetch } from './fetch';
import { ProxyAgent } from 'proxy-agent';
import { createFakePartial } from '../common/test_utils/create_fake_partial';
import fs from 'fs';
import { log } from '../common/log';

jest.mock('cross-fetch');

const fakeBuffer = createFakePartial<Buffer>({});
const readFileSyncSpy = jest.spyOn(fs, 'readFileSync');
readFileSyncSpy.mockImplementation(() => fakeBuffer);
const logErrorSpy = jest.spyOn(log, 'error');

describe('LsFetch', () => {
  const env = process.env;

  beforeEach(() => {
    process.env = {
      ...env,
      http_proxy: undefined,
      https_proxy: undefined,
    };
  });

  afterEach(() => {
    process.env = env;
    (fetch as jest.Mock).mockClear();
    readFileSyncSpy.mockClear();
    logErrorSpy.mockClear();
  });

  describe('fetch', () => {
    describe('not initialized', () => {
      it('should throw Error', async () => {
        const lsFetch = new Fetch();

        await expect(lsFetch.fetch('https://gitlab.com/')).rejects.toThrowError(
          'LsFetch not initialized. Make sure LsFetch.initialize() was called.',
        );
      });
    });

    describe('request Methods', () => {
      const testCases: ['get' | 'post' | 'put' | 'delete', string, number][] = [
        ['get', 'GET', 200],
        ['post', 'POST', 500],
        ['put', 'PUT', 404],
        ['delete', 'DELETE', 403],
      ];

      test.each(testCases)(
        'should call cross-fetch fetch for %s',
        async (method, expectedMethod, expectedStatusCode) => {
          const lsFetch = new Fetch();
          await lsFetch.initialize();

          const response = { status: expectedStatusCode, json: jest.fn().mockResolvedValue({}) };
          (fetch as jest.Mock).mockResolvedValueOnce(response);

          await lsFetch[method]('https://gitlab.com/');
          expect(fetch).toHaveBeenCalled();

          const [url, init] = (fetch as jest.Mock).mock.calls[0];
          expect(init.method).toBe(expectedMethod);
          expect(url).toBe('https://gitlab.com/');
        },
      );
    });

    describe('agents', () => {
      let subject: Fetch;

      describe.each`
        expected         | expectedAgent  | expectedUrl             | http_proxy                    | https_proxy
        ${'ProxyAgent'}  | ${ProxyAgent}  | ${'http://gdk.local'}   | ${'http://proxy.local:8080/'} | ${'https://proxy.local:8443/'}
        ${'ProxyAgent'}  | ${ProxyAgent}  | ${'https://github.com'} | ${'http://proxy.local:8080/'} | ${'https://proxy.local:8443/'}
        ${'http.Agent'}  | ${http.Agent}  | ${'http://gdk.local'}   | ${''}                         | ${''}
        ${'https.Agent'} | ${https.Agent} | ${'https://github.com'} | ${''}                         | ${''}
      `('$expected', ({ expectedAgent, expectedUrl, http_proxy, https_proxy }) => {
        beforeEach(async () => {
          process.env = {
            ...env,
            http_proxy,
            https_proxy,
          };

          subject = new Fetch();
          await subject.initialize();

          (fetch as jest.Mock).mockResolvedValue({
            status: 200,
            json: jest.fn().mockResolvedValue({}),
          });
        });

        it(`should fetch ${expectedUrl} given proxy settings`, async () => {
          await subject.get(expectedUrl);
          expect(fetch).toHaveBeenCalled();

          const [url, init] = (fetch as jest.Mock).mock.calls[0];
          expect(init.agent).toBeInstanceOf(expectedAgent);
          expect(init.agent).toEqual(
            expect.objectContaining({
              options: expect.objectContaining({}),
            }),
          );
          expect(url).toBe(expectedUrl);
        });

        if (expectedUrl.startsWith('https:')) {
          it(`should include https related agent options`, async () => {
            await subject.get(expectedUrl);

            expect(fetch).toHaveBeenNthCalledWith(
              1,
              expectedUrl,
              expect.objectContaining({
                agent: expect.objectContaining({
                  options: expect.objectContaining({
                    rejectUnauthorized: true,
                  }),
                }),
              }),
            );
          });

          it(`should support updating https related agent options`, async () => {
            await subject.get(expectedUrl);

            expect(fetch).toHaveBeenNthCalledWith(
              1,
              expectedUrl,
              expect.objectContaining({
                agent: expect.objectContaining({
                  options: expect.objectContaining({
                    rejectUnauthorized: true,
                  }),
                }),
              }),
            );

            await subject.updateAgentOptions({
              ignoreCertificateErrors: true,
              ca: 'abc',
              cert: 'def',
              certKey: 'ghi',
            });
            await subject.get(expectedUrl);

            expect(fetch).toHaveBeenNthCalledWith(
              2,
              expectedUrl,
              expect.objectContaining({
                agent: expect.objectContaining({
                  options: expect.objectContaining({
                    rejectUnauthorized: false,
                    ca: fakeBuffer,
                    cert: fakeBuffer,
                    key: fakeBuffer,
                  }),
                }),
              }),
            );
          });

          it(`should not pass through unset agent options`, async () => {
            await subject.updateAgentOptions({
              ignoreCertificateErrors: false,
              ca: '',
              cert: undefined,
              // certKey not present
            });
            await subject.get(expectedUrl);

            const options = (fetch as jest.Mock).mock.calls[0][1].agent.options;
            expect(options).toEqual(expect.objectContaining({ rejectUnauthorized: true }));
            expect(options).toEqual(expect.not.objectContaining({ ca: expect.anything() }));
            expect(options).toEqual(expect.not.objectContaining({ cert: expect.anything() }));
            expect(options).toEqual(expect.not.objectContaining({ key: expect.anything() }));
          });
        }
      });
    });

    describe('certificate options', () => {
      const expectedUrl = 'https://github.com';
      const error = new Error('hello');
      let subject: Fetch;

      beforeEach(async () => {
        subject = new Fetch();
        await subject.initialize();
        readFileSyncSpy.mockImplementation(() => {
          throw error;
        });
      });

      it.each([['ca'], ['cert'], ['certKey']])(
        "should log an error when file access to '%s' is not possible",
        async (property) => {
          await subject.updateAgentOptions({
            ignoreCertificateErrors: false,
            [property]: 'abc',
          });
          await subject.get(expectedUrl);

          expect(logErrorSpy).toHaveBeenCalledWith(
            'Error reading https agent options from file',
            error,
          );
        },
      );

      it('should NOT log an error when no file access error has occured', async () => {
        await subject.updateAgentOptions({
          ignoreCertificateErrors: false,
        });
        await subject.get(expectedUrl);

        expect(logErrorSpy).not.toHaveBeenCalledWith(
          'Error reading https agent options from file',
          error,
        );
      });
    });
  });
});
