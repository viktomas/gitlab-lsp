import { SuggestionContext } from '../../common/suggestion_client';
import { TreeSitterParser } from '../../common/tree_sitter';
import { TREE_SITTER_LANGUAGES } from './languages';
import { DesktopTreeSitterParser } from './parser';
import * as intentDetectionExamples from './test_examples/intent_detection';
import type { IntentDetectionExamples } from './test_examples/intent_detection';

const SUGGESTION_CONTEXTS: { [key: string]: IntentDetectionExamples } = intentDetectionExamples;

const XML_COMPLETION_CONTEXT: SuggestionContext = {
  document: {
    prefix: '<div>',
    suffix: '</div>',
    filename: 'completion.xml',
    position: {
      line: 0,
      character: 5,
    },
  },
};

const XML_GENERATION_CONTEXT: SuggestionContext = {
  document: {
    prefix: '<!-- Create a block a few attributes hello world. -->',
    suffix: '',
    filename: 'generation.xml',
    position: {
      line: 0,
      character: 53,
    },
  },
};

describe('TreeSitterParser', () => {
  let subject: TreeSitterParser;

  beforeEach(async () => {
    subject = new DesktopTreeSitterParser();
    await subject.init();
  });

  describe('getIntent', () => {
    it('getIntent for an unsupported language returns completion', async () => {
      const actual = await subject.getIntent(XML_COMPLETION_CONTEXT);
      expect(actual).toBe(undefined);
    });

    it('getIntent for an unsupported language returns generation', async () => {
      const actual = await subject.getIntent(XML_GENERATION_CONTEXT);
      expect(actual).toBe(undefined);
    });

    Object.keys(SUGGESTION_CONTEXTS).forEach((language) => {
      const contexts = SUGGESTION_CONTEXTS[language];

      it.each(contexts.completion)(`detects completion intent in ${language}`, async (context) => {
        const actual = await subject.getIntent(context);

        expect(actual).toBe('completion');
      });

      it.each(contexts.generation)(`detects generation intent in ${language}`, async (context) => {
        const actual = await subject.getIntent(context);
        expect(actual).toBe('generation');
      });
    });

    it('has coverage for all supported languages', () => {
      expect(Object.keys(SUGGESTION_CONTEXTS)).toEqual(
        TREE_SITTER_LANGUAGES.map((language) => language.name),
      );
    });
  });
});
