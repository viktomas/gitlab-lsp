import dedent from 'dedent';
import { IntentDetectionExamples } from './types';

const examples: IntentDetectionExamples = {
  completion: [
    {
      document: {
        prefix: 'def sayHello(name)',
        suffix: '}',
        filename: 'completion.rb',
        position: {
          line: 0,
          character: 18,
        },
      },
    },
  ],
  generation: [
    {
      document: {
        prefix: '# Create a function that says hello world.\n',
        suffix: '',
        filename: 'generation.rb',
        position: {
          line: 1,
          character: 0,
        },
      },
    },
    {
      document: {
        prefix: dedent`
        # Create a function that says hello world.

        def sayHello(name)
          puts "Hello #{name}!"
        end
        `,
        suffix: '',
        filename: 'generation.rb',
        position: {
          line: 1,
          character: 0,
        },
      },
    },
  ],
};

export default examples;
