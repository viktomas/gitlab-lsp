import { IntentDetectionExamples } from './types';
import dedent from 'dedent';

const examples: IntentDetectionExamples = {
  completion: [
    {
      document: {
        prefix: '',
        suffix: '',
        filename: 'completion.js',
        position: {
          line: 3,
          character: 0,
        },
      },
    },
    {
      document: {
        prefix: 'function sayHello() {',
        suffix: '}',
        filename: 'completion.js',
        position: {
          line: 0,
          character: 21,
        },
      },
    },
  ],
  generation: [
    {
      document: {
        prefix: '// Create a function that says hello world.\n',
        suffix: '',
        filename: 'generation.js',
        position: {
          line: 1,
          character: 0,
        },
      },
    },
    {
      document: {
        prefix: dedent`
        function checkA11YDefaultState() {
          cy.visitStory('base/label');

          // Generate a new cy command

          cy.glCheckA11y();
        }
        `,
        suffix: '',
        filename: 'generation.js',
        position: {
          line: 4,
          character: 0,
        },
      },
    },
  ],
};

export default examples;
