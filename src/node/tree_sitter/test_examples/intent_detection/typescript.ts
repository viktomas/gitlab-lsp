import dedent from 'dedent';
import { IntentDetectionExamples } from './types';

const examples: IntentDetectionExamples = {
  completion: [
    {
      document: {
        prefix: 'const sayHello: FunnyFunction = () => {',
        suffix: '}',
        filename: 'completion.ts',
        position: {
          line: 0,
          character: 39,
        },
      },
    },
    {
      document: {
        prefix: dedent`
        // sayHello is a function that greets you
        const sayHello: FunnyFunction = () => {',
        `,
        suffix: '}',
        filename: 'completion.ts',
        position: {
          line: 1,
          character: 41,
        },
      },
    },
  ],
  generation: [
    {
      document: {
        prefix: '// Create a function that says hello world.\n',
        suffix: '',
        filename: 'generation.ts',
        position: {
          line: 1,
          character: 0,
        },
      },
    },
    {
      document: {
        prefix: dedent`
        //This is a comment

        const sayHello: FunnyFunction = () => {
        `,
        suffix: '}',
        filename: 'generation.ts',
        position: {
          line: 1,
          character: 0,
        },
      },
    },
  ],
};

export default examples;
