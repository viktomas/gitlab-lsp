import { IntentDetectionExamples } from './types';
import dedent from 'dedent';

const examples: IntentDetectionExamples = {
  completion: [
    {
      document: {
        prefix: dedent`
        class Completion {
           public static
        `,
        suffix: '\n}',
        filename: 'Completion.java',
        position: {
          line: 2,
          character: 18,
        },
      },
    },
    {
      document: {
        prefix:
          'public class MultiLineBlockCompletion {\n/* this is a\nmulti-line code comment. */\n\npublic static void ',
        suffix: '}',
        filename: 'MultiLineBlockCompletion.java',
        position: {
          line: 5,
          character: 19,
        },
      },
    },
  ],
  generation: [
    {
      document: {
        prefix: '// Create a main method which prints hello world.\n',
        suffix: '',
        filename: 'Generation.java',
        position: {
          line: 1,
          character: 0,
        },
      },
    },
    {
      document: {
        prefix: dedent`
        class HelloWorldLineCommentGeneration {
          public static void main(String[] args) {
            // Print hello world and make me a sandwich.
        `,
        suffix: '   }\n}',
        filename: 'HelloWorldLineCommentGeneration.java',
        position: {
          line: 4,
          character: 0,
        },
      },
    },
    {
      document: {
        prefix: dedent`
        class HelloWorldMultiLineCommentGeneration {
          public static void main(String[] args) {
            // Print hello world and make me a sandwich.
            // Add anchovies please!
        `,
        suffix: '   }\n}',
        filename: 'HelloWorldMultiLineCommentGeneration.java',
        position: {
          line: 5,
          character: 0,
        },
      },
    },
    {
      document: {
        prefix: dedent`
        class HelloWorldBlockCommentGeneration {
          public static void main(String[] args) {
            /* Print "this is a comment" to standard out. */
        `,
        suffix: '   }\n}',
        filename: 'HelloWorldBlockCommentGeneration.java',
        position: {
          line: 4,
          character: 0,
        },
      },
    },
    {
      document: {
        prefix:
          'public class MultiLineBlockGeneration {\n/* this is a\nmulti-line code comment.\n\n*/\n',
        suffix: '\n}',
        filename: 'MultiLineBlockGeneration.java',
        position: {
          line: 5,
          character: 0,
        },
      },
    },
  ],
};

export default examples;
