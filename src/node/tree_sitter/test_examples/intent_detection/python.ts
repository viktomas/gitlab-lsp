import dedent from 'dedent';
import type { IntentDetectionExamples } from './types';

const examples: IntentDetectionExamples = {
  completion: [
    {
      document: {
        prefix: 'def sayHello():',
        suffix: '',
        filename: 'completion.py',
        position: {
          line: 0,
          character: 15,
        },
      },
    },
  ],
  generation: [
    {
      document: {
        prefix: '# Create a function that says hello world.\n',
        suffix: '',
        filename: 'generation.py',
        position: {
          line: 1,
          character: 0,
        },
      },
    },
    {
      document: {
        prefix: dedent`
        # Create a function that says hello world.


        def sayHello():
          print("Hello World!")
        `,
        suffix: '',
        filename: 'generation.py',
        position: {
          line: 2,
          character: 0,
        },
      },
    },
  ],
};

export default examples;
