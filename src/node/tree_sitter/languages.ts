import * as path from 'path';
import { TreeSitterLanguage } from '../../common/tree_sitter';

/*!
 * The GitLab Language Server (@gitlab-org/gitlab-lsp) project includes the following binary distributions of tree-sitter grammars:
 *
 * tree-sitter-go.wasm:
 *   Copyright (c) 2014 Max Brunsfeld
 *   Licensed under The MIT License (MIT) - https://github.com/tree-sitter/tree-sitter-go/blob/master/LICENSE
 * tree-sitter-java.wasm:
 *   Copyright (c) 2017 Ayman Nadeem
 *   Licensed under The MIT License (MIT) - https://github.com/tree-sitter/tree-sitter-java/blob/master/LICENSE
 * tree-sitter-javascript.wasm:
 *   Copyright (c) 2014 Max Brunsfeld
 *   Licensed under The MIT License (MIT) - https://github.com/tree-sitter/tree-sitter-javascript/blob/master/LICENSE
 * tree-sitter-python.wasm:
 *   Copyright (c) 2016 Max Brunsfeld
 *   Licensed under The MIT License (MIT) - https://github.com/tree-sitter/tree-sitter-python/blob/master/LICENSE
 * tree-sitter-ruby.wasm:
 *   Copyright (c) 2016 Rob Rix
 *   Licensed under The MIT License (MIT) - https://github.com/tree-sitter/tree-sitter-ruby/blob/master/LICENSE
 * tree-sitter-typescript:
 *   Copyright (c) 2017 GitHub
 *   Licensed under The MIT License (MIT) - https://github.com/tree-sitter/tree-sitter-typescript/blob/master/LICENSE
 */
export const TREE_SITTER_LANGUAGES: TreeSitterLanguage[] = [
  {
    name: 'go',
    extensions: ['.go'],
    wasmPath: path.join(__dirname, '../../../vendor/grammars/tree-sitter-go.wasm'),
  },
  {
    name: 'java',
    extensions: ['.java'],
    wasmPath: path.join(__dirname, '../../../vendor/grammars/tree-sitter-java.wasm'),
  },
  {
    name: 'javascript',
    extensions: ['.js'],
    wasmPath: path.join(__dirname, '../../../vendor/grammars/tree-sitter-javascript.wasm'),
  },
  {
    name: 'python',
    extensions: ['.py'],
    wasmPath: path.join(__dirname, '../../../vendor/grammars/tree-sitter-python.wasm'),
  },
  {
    name: 'ruby',
    extensions: ['.rb'],
    wasmPath: path.join(__dirname, '../../../vendor/grammars/tree-sitter-ruby.wasm'),
  },
  {
    name: 'typescript',
    extensions: ['.ts'],
    wasmPath: path.join(__dirname, '../../../vendor/grammars/tree-sitter-typescript.wasm'),
  },
];
