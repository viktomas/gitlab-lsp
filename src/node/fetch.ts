import fetch from 'cross-fetch';
import { getProxySettings } from 'get-proxy-settings';
import { ProxyAgent } from 'proxy-agent';
import { isEqual } from 'lodash';
import { IFetch, FetchBase, FetchAgentOptions } from '../common/fetch';
import https from 'https';
import http from 'http';
import { log } from '../common/log';
import fs from 'fs';

const httpAgent = new http.Agent({
  keepAlive: true,
});

export interface LsRequestInit extends RequestInit {
  agent: ProxyAgent | https.Agent | http.Agent;
}

interface LsAgentOptions {
  rejectUnauthorized: boolean;
  ca?: Buffer;
  cert?: Buffer;
  key?: Buffer;
}

/**
 * Wrap fetch to support proxy configurations
 */
export class Fetch extends FetchBase implements IFetch {
  #proxy?: ProxyAgent;
  #userProxy?: string;

  #initialized: boolean = false;
  #httpsAgent: https.Agent;
  #agentOptions: Readonly<LsAgentOptions>;

  constructor(userProxy?: string) {
    super();

    this.#agentOptions = {
      rejectUnauthorized: true,
    };
    this.#userProxy = userProxy;
    this.#httpsAgent = this.createHttpsAgent();
  }

  async initialize(): Promise<void> {
    // Set http_proxy and https_proxy environment variables
    // which will then get picked up by the ProxyAgent and
    // used.
    try {
      const proxy = await getProxySettings();
      if (proxy?.http) {
        process.env.http_proxy = `${proxy.http.protocol}://${proxy.http.host}:${proxy.http.port}`;
        log.debug(`fetch: Detected http proxy through settings: ${process.env.http_proxy}`);
      }

      if (proxy?.https) {
        process.env.https_proxy = `${proxy.https.protocol}://${proxy.https.host}:${proxy.https.port}`;
        log.debug(`fetch: Detected https proxy through settings: ${process.env.https_proxy}`);
      }

      if (!proxy?.http && !proxy?.https) {
        log.debug(`fetch: Detected no proxy settings`);
      }
    } catch (err) {
      log.warn('Unable to load proxy settings', err);
    }

    if (this.#userProxy) {
      log.debug(`fetch: Detected user proxy ${this.#userProxy}`);
      process.env.http_proxy = this.#userProxy;
      process.env.https_proxy = this.#userProxy;
    }

    if (process.env.http_proxy || process.env.https_proxy) {
      log.info(
        `fetch: Setting proxy to https_proxy=${process.env.https_proxy}; http_proxy=${process.env.http_proxy}`,
      );
      this.#proxy = this.createProxyAgent();
    }

    this.#initialized = true;
  }

  async destroy(): Promise<void> {
    this.#proxy?.destroy();
  }

  async fetch(input: RequestInfo | URL, init?: RequestInit): Promise<Response> {
    if (!this.#initialized) {
      throw new Error('LsFetch not initialized. Make sure LsFetch.initialize() was called.');
    }

    return this.fetchLogged(input, {
      ...init,
      agent: this.getAgent(input),
    });
  }

  async updateAgentOptions({
    ignoreCertificateErrors,
    ca,
    cert,
    certKey,
  }: FetchAgentOptions): Promise<void> {
    let fileOptions = {};
    try {
      fileOptions = {
        ...(ca ? { ca: fs.readFileSync(ca) } : {}),
        ...(cert ? { cert: fs.readFileSync(cert) } : {}),
        ...(certKey ? { key: fs.readFileSync(certKey) } : {}),
      };
    } catch (err) {
      log.error('Error reading https agent options from file', err);
    }

    const newOptions: LsAgentOptions = {
      rejectUnauthorized: !ignoreCertificateErrors,
      ...fileOptions,
    };

    if (isEqual(newOptions, this.#agentOptions)) {
      // new and old options are the same, nothing to do
      return;
    }

    this.#agentOptions = newOptions;
    this.#httpsAgent = this.createHttpsAgent();
    if (this.#proxy) {
      this.#proxy = this.createProxyAgent();
    }
  }

  private createHttpsAgent(): https.Agent {
    const agentOptions = {
      ...this.#agentOptions,
      keepAlive: true,
    };
    log.debug(
      `fetch: https agent with options initialized ${JSON.stringify(
        this.sanitizeAgentOptions(agentOptions),
      )}.`,
    );
    return new https.Agent(agentOptions);
  }

  private createProxyAgent(): ProxyAgent {
    log.debug(
      `fetch: proxy agent with options initialized ${JSON.stringify(
        this.sanitizeAgentOptions(this.#agentOptions),
      )}.`,
    );
    return new ProxyAgent(this.#agentOptions);
  }

  private async fetchLogged(input: RequestInfo | URL, init: LsRequestInit): Promise<Response> {
    const start = Date.now();
    const url = this.extractURL(input);

    if (init.agent === httpAgent) {
      log.debug(`fetch: request for ${url} made with http agent.`);
    } else {
      const type = init.agent === this.#proxy ? 'proxy' : 'https';
      log.debug(`fetch: request for ${url} made with ${type} agent.`);
    }

    try {
      const resp = await fetch(input, init);
      const duration = Date.now() - start;

      log.debug(`fetch: request to ${url} returned HTTP ${resp.status} after ${duration} ms`);

      return resp;
    } catch (e) {
      const duration = Date.now() - start;
      log.debug(`fetch: request to ${url} threw an exception after ${duration} ms`);
      log.error(`fetch: request to ${url} failed with:`, e);
      throw e;
    }
  }

  private getAgent(input: RequestInfo | URL): ProxyAgent | https.Agent | http.Agent {
    if (this.#proxy) {
      return this.#proxy;
    } else if (input.toString().startsWith('https://')) {
      return this.#httpsAgent;
    }

    return httpAgent;
  }

  private extractURL(input: RequestInfo | URL): string {
    if (input instanceof URL) {
      return input.toString();
    }

    if (typeof input === 'string') {
      return input;
    }

    return input.url;
  }

  private sanitizeAgentOptions(agentOptions: LsAgentOptions) {
    const { ca, cert, key, ...options } = agentOptions;
    return {
      ...options,
      ...(ca ? { ca: '<hidden>' } : {}),
      ...(cert ? { cert: '<hidden>' } : {}),
      ...(key ? { key: '<hidden>' } : {}),
    };
  }
}
