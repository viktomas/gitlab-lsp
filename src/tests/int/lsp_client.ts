import fs from 'node:fs';
import { spawn, ChildProcessWithoutNullStreams } from 'child_process';
import {
  createMessageConnection,
  StreamMessageReader,
  StreamMessageWriter,
} from 'vscode-jsonrpc/node';
import {
  CompletionItem,
  CompletionList,
  CompletionParams,
  DidCloseTextDocumentParams,
  DidChangeTextDocumentParams,
  DidChangeConfigurationParams,
  DidOpenTextDocumentParams,
  InitializedParams,
  InitializeError,
  InitializeParams,
  InitializeResult,
  RequestType,
  NotificationType,
  RequestType1,
  MessageConnection,
  URI,
  MarkupKind,
} from 'vscode-languageserver';
import { ChangeConfigOptions, CustomInitializeParams } from '../../common/message_handler';
import { createFakePartial } from '../../common/test_utils/create_fake_partial';

if (process.env.GITLAB_TEST_TOKEN == undefined || process.env.GITLAB_TEST_TOKEN.length < 4) {
  throw new Error('Error, GITLAB_TEST_TOKEN not set to a GitLab PAT.');
}

export const GITLAB_TEST_TOKEN = process.env.GITLAB_TEST_TOKEN;

const workspaceFolderURI: URI = 'file://base/path';
export const DEFAULT_INITIALIZE_PARAMS = createFakePartial<CustomInitializeParams>({
  processId: process.pid,
  capabilities: {
    textDocument: {
      completion: {
        completionItem: {
          documentationFormat: [MarkupKind.PlainText],
          insertReplaceSupport: false,
        },
        completionItemKind: {
          valueSet: [1], // text
        },
        contextSupport: false,
        insertTextMode: 2, // adjust indentation
      },
    },
  },
  clientInfo: {
    name: 'lsp_client',
    version: '0.0.1',
  },
  workspaceFolders: [
    {
      name: 'test',
      uri: workspaceFolderURI,
    },
  ],
  initializationOptions: {
    ide: {
      name: 'lsp_client',
      version: '0.0.1',
      vendor: 'gitlab',
    },
  },
});

/**
 * Low level language server client for integration tests
 */
export class LspClient {
  private childProcess: ChildProcessWithoutNullStreams;
  private connection: MessageConnection;
  private gitlabToken: string;
  private gitlabBaseUrl: string = 'https://gitlab.com';

  public childProcessConsole: string[] = [];

  /**
   * Spawn language server and create an RPC connection.
   *
   * @param gitlabToken GitLab PAT to use for code suggestions
   */
  constructor(gitlabToken: string) {
    this.gitlabToken = gitlabToken;

    const command = process.env.LSP_COMMAND ?? 'node';
    const args = process.env.LSP_ARGS?.split(' ') ?? ['./out/node/main.js', '--stdio'];
    console.log(`Running LSP using command \`${command} ${args.join(' ')}\` `);

    const file = command == 'node' ? args[0] : command;
    expect(file).not.toBe(undefined);
    expect({ file, exists: fs.existsSync(file) }).toEqual({ file, exists: true });

    this.childProcess = spawn(command, args);
    this.childProcess.stderr.on('data', (chunk: Buffer | string) => {
      const chunkString = chunk.toString('utf8');
      this.childProcessConsole = this.childProcessConsole.concat(chunkString.split('\n'));
      process.stderr.write(chunk);
    });

    // Use stdin and stdout for communication:
    this.connection = createMessageConnection(
      new StreamMessageReader(this.childProcess.stdout),
      new StreamMessageWriter(this.childProcess.stdin),
    );

    this.connection.listen();
    this.connection.onError(function (err) {
      console.error(err);
      expect(err).not.toBeTruthy();
    });
  }

  /**
   * Make sure to call this method to shutdown the LS rpc connection
   */
  public dispose() {
    this.connection.end();
  }

  /**
   * Send the LSP 'initialize' message.
   *
   * @param initializeParams Provide custom values that override defaults. Merged with defaults.
   * @returns InitializeResponse object
   */
  public async sendInitialize(
    initializeParams?: CustomInitializeParams,
  ): Promise<InitializeResult> {
    const params = Object.assign({}, DEFAULT_INITIALIZE_PARAMS, initializeParams);
    const request = new RequestType<InitializeParams, InitializeResult, InitializeError>(
      'initialize',
    );
    const response = await this.connection.sendRequest(request, params);
    return response;
  }

  /**
   * Send the LSP 'initialized' notification
   */
  public async sendInitialized(options?: InitializedParams | undefined): Promise<void> {
    const defaults = {};
    const params = Object.assign({}, defaults, options);
    const request = new NotificationType<InitializedParams>('initialized');
    await this.connection.sendNotification(request, params);
  }

  /**
   * Send the LSP 'workspace/didChangeConfiguration' notification
   */
  public async sendDidChangeConfiguration(
    options?: ChangeConfigOptions | undefined,
  ): Promise<void> {
    const defaults = createFakePartial<ChangeConfigOptions>({
      settings: {
        token: this.gitlabToken,
        baseUrl: this.gitlabBaseUrl,
        codeCompletion: {
          enableSecretRedaction: true,
        },
        telemetry: {
          enabled: false,
        },
      },
    });

    const params = Object.assign({}, defaults, options);
    const request = new NotificationType<DidChangeConfigurationParams>(
      'workspace/didChangeConfiguration',
    );

    await this.connection.sendNotification(request, params);
  }

  public async sendTextDocumentDidOpen(
    uri: string,
    languageId: string,
    version: number,
    text: string,
  ): Promise<void> {
    const params = {
      textDocument: {
        uri,
        languageId,
        version,
        text,
      },
    };

    const request = new NotificationType<DidOpenTextDocumentParams>('textDocument/didOpen');
    await this.connection.sendNotification(request, params);
  }

  public async sendTextDocumentDidClose(uri: string): Promise<void> {
    const params = {
      textDocument: {
        uri,
      },
    };

    const request = new NotificationType<DidCloseTextDocumentParams>('textDocument/didClose');
    await this.connection.sendNotification(request, params);
  }

  /**
   * Send LSP 'textDocument/didChange' using Full sync method notification
   *
   * @param uri File URL. Should include the workspace path in the url
   * @param version Change version (incrementing from 0)
   * @param text Full contents of the file
   */
  public async sendTextDocumentDidChangeFull(
    uri: string,
    version: number,
    text: string,
  ): Promise<void> {
    const params = {
      textDocument: {
        uri: uri,
        version: version,
      },
      contentChanges: [{ text: text }],
    };

    const request = new NotificationType<DidChangeTextDocumentParams>('textDocument/didChange');
    await this.connection.sendNotification(request, params);
  }

  public async sendTextDocumentCompletion(
    uri: string,
    line: number,
    character: number,
  ): Promise<CompletionItem[] | CompletionList | null> {
    const params: CompletionParams = {
      textDocument: {
        uri: uri,
      },
      position: {
        line: line,
        character: character,
      },
      context: {
        triggerKind: 1, // invoked
      },
    };

    const request = new RequestType1<
      CompletionParams,
      CompletionItem[] | CompletionList | null,
      void
    >('textDocument/completion');
    return await this.connection.sendRequest(request, params);
  }
}
