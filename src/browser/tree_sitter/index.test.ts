import Parser = require('web-tree-sitter');
import { ConfigProvider } from '../../common';
import { BrowserTreeSitterParser } from './index';
import { TreeSitterParserLoadState } from '../../common/tree_sitter';

jest.mock('web-tree-sitter');

type ParserInitOptions = { locateFile: (scriptName: string) => string };

describe('BrowserTreeSitterParser', () => {
  let subject: BrowserTreeSitterParser;
  let configProvider: ConfigProvider;
  const baseAssetsUrl = 'http://localhost/assets/tree-sitter/';

  describe('init', () => {
    beforeEach(async () => {
      configProvider = new ConfigProvider();
      configProvider.set('baseAssetsUrl', baseAssetsUrl);

      subject = new BrowserTreeSitterParser(configProvider);
    });

    it('initializes languages list with supported languages', async () => {
      await subject.init();

      const actual = subject.getLanguages();
      expect(actual).toEqual([
        {
          extensions: ['.go'],
          name: 'go',
          wasmPath: 'http://localhost/assets/tree-sitter/vendor/grammars/tree-sitter-go.wasm',
        },
        {
          extensions: ['.java'],
          name: 'java',
          wasmPath: 'http://localhost/assets/tree-sitter/vendor/grammars/tree-sitter-java.wasm',
        },
        {
          extensions: ['.js'],
          name: 'javascript',
          wasmPath:
            'http://localhost/assets/tree-sitter/vendor/grammars/tree-sitter-javascript.wasm',
        },
        {
          extensions: ['.py'],
          name: 'python',
          wasmPath: 'http://localhost/assets/tree-sitter/vendor/grammars/tree-sitter-python.wasm',
        },
        {
          extensions: ['.rb'],
          name: 'ruby',
          wasmPath: 'http://localhost/assets/tree-sitter/vendor/grammars/tree-sitter-ruby.wasm',
        },
        {
          extensions: ['.ts'],
          name: 'typescript',
          wasmPath:
            'http://localhost/assets/tree-sitter/vendor/grammars/tree-sitter-typescript.wasm',
        },
      ]);
    });

    it('initializes the Treesitter parser with a locateFile function', async () => {
      let options: ParserInitOptions | undefined;

      jest.mocked(Parser.init).mockImplementationOnce(async (_options?: object) => {
        options = _options as ParserInitOptions;
      });

      await subject.init();

      expect(options?.locateFile).toBeDefined();
      expect(options?.locateFile('tree-sitter.wasm')).toBe(`${baseAssetsUrl}node/tree-sitter.wasm`);
    });

    it.each`
      initCall                                                             | loadState
      ${() => jest.mocked(Parser.init).mockResolvedValueOnce()}            | ${TreeSitterParserLoadState.READY}
      ${() => jest.mocked(Parser.init).mockRejectedValueOnce(new Error())} | ${TreeSitterParserLoadState.ERRORED}
    `('sets the correct parser load state', async ({ initCall, loadState }) => {
      initCall();

      await subject.init();

      expect(subject.getLoadState()).toBe(loadState);
    });
  });
});
