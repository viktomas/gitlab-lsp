import Parser = require('web-tree-sitter');
import { ConfigProvider } from '../../common';
import { log } from '../../common/log';
import { TreeSitterParser, TreeSitterParserLoadState } from '../../common/tree_sitter';

const resolveGrammarAbsoluteUrl = (relativeUrl: string, baseAssetsUrl: string = ''): string => {
  return new URL(relativeUrl, baseAssetsUrl).href;
};

export class BrowserTreeSitterParser extends TreeSitterParser {
  #configProvider: ConfigProvider;

  constructor(configProvider: ConfigProvider) {
    super({ languages: [] });
    this.#configProvider = configProvider;
  }

  getLanguages() {
    return this.languages;
  }

  getLoadState() {
    return this.loadState;
  }

  async init(): Promise<void> {
    const baseAssetsUrl = this.#configProvider.get().baseAssetsUrl;

    this.languages = [
      {
        name: 'go',
        extensions: ['.go'],
        wasmPath: resolveGrammarAbsoluteUrl('vendor/grammars/tree-sitter-go.wasm', baseAssetsUrl),
      },
      {
        name: 'java',
        extensions: ['.java'],
        wasmPath: resolveGrammarAbsoluteUrl('vendor/grammars/tree-sitter-java.wasm', baseAssetsUrl),
      },
      {
        name: 'javascript',
        extensions: ['.js'],
        wasmPath: resolveGrammarAbsoluteUrl(
          'vendor/grammars/tree-sitter-javascript.wasm',
          baseAssetsUrl,
        ),
      },
      {
        name: 'python',
        extensions: ['.py'],
        wasmPath: resolveGrammarAbsoluteUrl(
          'vendor/grammars/tree-sitter-python.wasm',
          baseAssetsUrl,
        ),
      },
      {
        name: 'ruby',
        extensions: ['.rb'],
        wasmPath: resolveGrammarAbsoluteUrl('vendor/grammars/tree-sitter-ruby.wasm', baseAssetsUrl),
      },
      {
        name: 'typescript',
        extensions: ['.ts'],
        wasmPath: resolveGrammarAbsoluteUrl(
          'vendor/grammars/tree-sitter-typescript.wasm',
          baseAssetsUrl,
        ),
      },
    ];

    try {
      await Parser.init({
        locateFile(scriptName: string) {
          return resolveGrammarAbsoluteUrl(`node/${scriptName}`, baseAssetsUrl);
        },
      });
      log.debug('BrowserTreeSitterParser: Initialized tree-sitter parser.');
      this.loadState = TreeSitterParserLoadState.READY;
    } catch (err) {
      log.warn('BrowserTreeSitterParser: Error initializing tree-sitter parsers.', err);
      this.loadState = TreeSitterParserLoadState.ERRORED;
    }
  }
}
