import {
  ProposedFeatures,
  BrowserMessageReader,
  BrowserMessageWriter,
  createConnection,
  TextDocuments,
} from 'vscode-languageserver/browser';
import { TextDocument } from 'vscode-languageserver-textdocument';
import { Fetch } from './fetch';
import { BrowserTreeSitterParser } from './tree_sitter';
import { ConfigProvider, DocumentTransformer, setup } from '../common';
import { SecretRedactor } from '../common/secret_redaction';
import { log } from '../common/log';

const worker: Worker = self as unknown as Worker;
const messageReader = new BrowserMessageReader(worker);
const messageWriter = new BrowserMessageWriter(worker);

const configProvider = new ConfigProvider();
const connection = createConnection(ProposedFeatures.all, messageReader, messageWriter);
const documents: TextDocuments<TextDocument> = new TextDocuments(TextDocument);
const lsFetch = new Fetch();
const transformer = new DocumentTransformer(documents, [new SecretRedactor(configProvider)]);
const treeSitterParser = new BrowserTreeSitterParser(configProvider);
log.setup(configProvider);

log.info('GitLab Language Server is starting');

// FIXME: this eslint violation was introduced before we set up linting
// eslint-disable-next-line @typescript-eslint/no-floating-promises
lsFetch.initialize().then(() => {
  setup(connection, transformer, lsFetch, configProvider, {
    treeSitterParser,
  });

  // Make the text document manager listen on the connection for open, change and close text document events
  documents.listen(connection);
  // Listen on the connection
  connection.listen();

  log.info('GitLab Language Server has started');
});
