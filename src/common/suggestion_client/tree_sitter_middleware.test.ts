import { createFakePartial } from '../test_utils/create_fake_partial';
import { TreeSitterParser } from '../tree_sitter';
import {
  SuggestionClientFn,
  SuggestionClientMiddleware,
  SuggestionContext,
  SuggestionResponse,
} from './suggestion_client';
import { createTreeSitterMiddleware } from './tree_sitter_middleware';

afterEach(() => {
  jest.clearAllMocks();
});

describe('common/suggestion_client/tree_sitter_middleware', () => {
  let next: SuggestionClientFn;
  let subject: SuggestionClientMiddleware;
  let mockTreeSitterParser: TreeSitterParser;

  beforeEach(() => {
    next = jest.fn().mockResolvedValue(createFakePartial<SuggestionResponse>({ status: 1 }));
    subject = createTreeSitterMiddleware(mockTreeSitterParser);
    mockTreeSitterParser = {
      getIntent: jest.fn(),
      init: jest.fn(),
    } as unknown as TreeSitterParser;
  });

  describe('createTreeSitterMiddleware', () => {
    it('does not parse document when intent is already set', async () => {
      const context: SuggestionContext = {
        document: {
          filename: 'generation.ts',
          prefix: '// Create a class named after a tanuki.',
          suffix: '',
          position: {
            line: 0,
            character: 40,
          },
        },
        intent: 'completion',
      };
      (mockTreeSitterParser.getIntent as jest.Mock).mockReturnValue('generation');

      await subject(context, next);

      expect(mockTreeSitterParser.getIntent).not.toHaveBeenCalled();
      expect(next).toHaveBeenCalledWith(expect.objectContaining({ intent: 'completion' }));
    });

    it('parses document to determine intent', async () => {
      const context: SuggestionContext = {
        document: {
          filename: 'generation.ts',
          prefix: '// Create a class named after a tanuki.',
          suffix: '',
          position: {
            line: 0,
            character: 39,
          },
        },
      };
      (mockTreeSitterParser.getIntent as jest.Mock).mockReturnValue('generation');
      await subject(context, next);

      expect(next).toHaveBeenCalledWith(expect.objectContaining({ intent: 'generation' }));
    });
  });
});
