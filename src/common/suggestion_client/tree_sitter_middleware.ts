import { SuggestionClientMiddleware } from './suggestion_client';
import { TreeSitterParser } from '../tree_sitter';
import { log } from '../log';

export const createTreeSitterMiddleware =
  (treeSitterParser: TreeSitterParser): SuggestionClientMiddleware =>
  async (context, next) => {
    if (!context.intent) {
      try {
        const intent = await treeSitterParser.getIntent(context);
        if (intent) {
          return await next({ ...context, intent });
        }
      } catch (err) {
        log.warn('Error determining user intent for code suggestion request.', err);
      }
    }

    return next(context);
  };
