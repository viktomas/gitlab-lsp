import {
  type SuggestionClient,
  type SuggestionClientMiddleware,
  type SuggestionContext,
  type SuggestionResponse,
} from './suggestion_client';

export class SuggestionClientRouter<T extends string> implements SuggestionClient {
  // Use Record<string> (instead of Record<T>) here since it simplifies types of helpers like Object.keys
  readonly #clients: Record<string, SuggestionClient>;

  #key: T;

  constructor(
    readonly clients: Record<T, SuggestionClient>,
    initialKey: T,
  ) {
    this.#clients = clients;
    this.#key = initialKey;
  }

  routeTo(key: T) {
    this.#key = key;
  }

  getSuggestions(context: SuggestionContext): Promise<SuggestionResponse | undefined> {
    return this.#clients[this.#key].getSuggestions(context);
  }

  asMiddleware(): SuggestionClientMiddleware {
    return (context) => this.#clients[this.#key].getSuggestions(context);
  }
}
