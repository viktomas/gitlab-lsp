import { CodeSuggestionRequest, IGitLabAPI } from '../api';
import { SuggestionClient, SuggestionContext, SuggestionResponse } from './suggestion_client';

export class SuggestionClientMonolith implements SuggestionClient {
  readonly #api: IGitLabAPI;

  constructor(api: IGitLabAPI) {
    this.#api = api;
  }

  getSuggestions(context: SuggestionContext): Promise<SuggestionResponse | undefined> {
    const request: CodeSuggestionRequest = {
      prompt_version: 1,
      project_path: context.projectPath ?? '',
      project_id: -1,
      current_file: {
        content_above_cursor: context.document.prefix,
        content_below_cursor: context.document.suffix,
        file_name: context.document.filename,
      },
      intent: context.intent,
    };

    return this.#api.getCodeSuggestions(request);
  }
}
