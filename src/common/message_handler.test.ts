import {
  CancellationToken,
  CompletionItem,
  CompletionParams,
  Connection,
  InlineCompletionContext,
  InlineCompletionList,
  InlineCompletionParams,
  InlineCompletionTriggerKind,
  Position,
  Range,
  WorkspaceFolder,
} from 'vscode-languageserver';
import { ConfigProvider, IConfig } from './config';
import {
  CustomInitializeParams,
  DEBOUNCE_INTERVAL_MS,
  MessageHandler,
  START_STREAMING_COMMAND,
  SUGGESTION_ACCEPTED_COMMAND,
} from './message_handler';
import { createFakePartial } from './test_utils/create_fake_partial';
import { IGitLabAPI } from './api';
import {
  CODE_SUGGESTIONS_CATEGORY,
  SnowplowTracker,
  TRACKING_EVENTS,
} from './tracking/snowplow_tracker';
import { DocumentTransformer } from './documents';
import {
  COMPLETION_PARAMS,
  LONG_COMPLETION_CONTEXT,
  SHORT_COMPLETION_CONTEXT,
} from './test_utils/mocks';
import * as completionFilters from './suggestion/suggestion_filter';
import { SuggestionsCache } from './suggestion/suggestions_cache';
import { CIRCUIT_BREAK_INTERVAL_MS, CircuitBreaker } from './circuit_breaker/circuit_breaker';
import { AiGatewayClient } from './ai_gateway';
import { LOG_LEVEL } from './log';
import { TreeSitterParser } from './tree_sitter';
import { IFetch } from './fetch';
import { startStreaming } from './streaming_completion';
import { FetchError } from './fetch_error';
import { createFakeResponse } from './test_utils/create_fake_response';

jest.mock('./suggestion/suggestions_cache');
jest.mock('./suggestion/suggestion_filter');
jest.mock('./streaming_completion', () => ({
  startStreaming: jest.fn().mockResolvedValue([]),
}));
jest.useFakeTimers();
const TRACKING_ID = 'unique tracking id';
jest.spyOn(SnowplowTracker, 'uniqueTrackingId').mockReturnValue(TRACKING_ID);

describe('MessageHandler', () => {
  let messageHandler: MessageHandler;
  let aiGateway: AiGatewayClient;
  const treeSitterParser = createFakePartial<TreeSitterParser>({
    getIntent: jest.fn(),
  });
  const documentTransformer = createFakePartial<DocumentTransformer>({
    getContext: jest.fn(),
    get: jest.fn(),
  });

  const lsFetch = createFakePartial<IFetch>({
    fetch: jest.fn(),
    updateAgentOptions: jest.fn(),
  });

  const tracker: SnowplowTracker = createFakePartial<SnowplowTracker>({
    canClientTrackEvent: jest.fn(),
    updateSuggestionState: jest.fn(),
    setCodeSuggestionsContext: jest.fn(),
    updateCodeSuggestionsContext: jest.fn(),
    reconfigure: jest.fn(),
    setClientContext: jest.fn(),
  });

  beforeEach(() => {
    aiGateway = {
      setClientInfo: jest.fn(),
      fetchCompletions: jest.fn(),
      fetchGenerations: jest.fn(),
    };

    jest
      .mocked(completionFilters.shouldRejectCompletionWithSelectedCompletionTextMismatch)
      .mockReturnValue(false);
  });

  afterEach(() => {
    (Object.keys(tracker) as Array<keyof SnowplowTracker>).forEach((mockTrackingMethod) => {
      (tracker[mockTrackingMethod] as jest.Mock).mockReset();
    });
  });

  describe('onInitializeHandler', () => {
    const customInitializeParams: CustomInitializeParams =
      createFakePartial<CustomInitializeParams>({
        clientInfo: { name: 'Web IDE', version: '1.0.0' },
        workspaceFolders: [],
        initializationOptions: {
          baseAssetsUrl: '/assets/',
        },
      });
    let configProvider: ConfigProvider;
    let api: IGitLabAPI;

    beforeEach(() => {
      configProvider = new ConfigProvider();
      api = createFakePartial<IGitLabAPI>({
        setClientInfo: jest.fn(),
      });

      messageHandler = new MessageHandler({
        connection: createFakePartial<Connection>({}),
        api,
        aiGateway,
        tracker,
        configProvider,
        documentTransformer,
        lsFetch,
        treeSitterParser,
      });

      jest.spyOn(configProvider, 'set');

      messageHandler.onInitializeHandler(customInitializeParams);
    });

    it('stores clientInfo in configProvider, api service, and aiGateway', () => {
      expect(configProvider.set).toHaveBeenCalledWith(
        'clientInfo',
        customInitializeParams.clientInfo,
      );
      expect(api.setClientInfo).toHaveBeenCalledWith(customInitializeParams.clientInfo);
      expect(aiGateway.setClientInfo).toHaveBeenCalledWith(customInitializeParams.clientInfo);
    });

    it('stores workspaceFolders in configProvider', () => {
      expect(configProvider.set).toHaveBeenCalledWith(
        'workspaceFolders',
        customInitializeParams.workspaceFolders,
      );
    });

    it('stores baseAssetsUrl in configProvider', () => {
      expect(configProvider.set).toHaveBeenCalledWith(
        'baseAssetsUrl',
        customInitializeParams.initializationOptions.baseAssetsUrl,
      );
    });

    it.each`
      initializationOptions                                                      | clientContext
      ${{ ide: { name: 'Web IDE' }, extension: { name: 'Workflow Extension' } }} | ${{ ide: { name: 'Web IDE' }, extension: { name: 'Workflow Extension' } }}
      ${{ extension: { name: 'Workflow Extension' } }}                           | ${{ ide: customInitializeParams.clientInfo, extension: { name: 'Workflow Extension' } }}
      ${{}}                                                                      | ${{ ide: customInitializeParams.clientInfo }}
    `('initializes tracker', ({ initializationOptions, clientContext }) => {
      messageHandler.onInitializeHandler({
        ...customInitializeParams,
        initializationOptions,
      });
      expect(tracker.setClientContext).toHaveBeenCalledWith(clientContext);
    });
  });

  describe('didChangeConfigurationHandler', () => {
    let configProvider: ConfigProvider;
    let api: IGitLabAPI;

    beforeEach(async () => {
      configProvider = new ConfigProvider();
      api = createFakePartial<IGitLabAPI>({
        configureApi: jest.fn(),
        checkToken: jest.fn(),
      });
      jest.mocked(api.checkToken).mockResolvedValue({ valid: true });
      messageHandler = new MessageHandler({
        connection: createFakePartial<Connection>({}),
        api,
        aiGateway,
        tracker,
        configProvider,
        documentTransformer,
        lsFetch,
        treeSitterParser,
      });
    });

    it('can change all settings', async () => {
      const expectedConfig: IConfig = {
        baseUrl: 'https://test.url',
        codeCompletion: {
          enableSecretRedaction: false,
        },
        telemetry: {
          enabled: false,
          trackingUrl: 'https://telemetry.url',
        },
        logLevel: LOG_LEVEL.DEBUG,
        ignoreCertificateErrors: false,
        httpAgentOptions: {},
      };
      await messageHandler.didChangeConfigurationHandler({
        settings: expectedConfig,
      });

      expect(configProvider.get()).toEqual(expectedConfig);
    });

    it('uses the preexisting settings if the new config does not have them', async () => {
      const defaultConfig = new ConfigProvider().get();

      await messageHandler.didChangeConfigurationHandler({
        settings: {},
      });

      expect(configProvider.get()).toEqual(defaultConfig);
    });

    it('handles partial updates of nested properties', async () => {
      const defaultConfig = new ConfigProvider().get();
      const certKey = 'abcdef';

      await messageHandler.didChangeConfigurationHandler({
        settings: {
          telemetry: {
            enabled: false,
          },
          codeCompletion: {
            enableSecretRedaction: false,
          },
          httpAgentOptions: {
            certKey,
          },
        },
      });

      expect(configProvider.get().telemetry).toEqual({
        ...defaultConfig.telemetry,
        enabled: false,
      });
      expect(configProvider.get().codeCompletion).toEqual({
        ...defaultConfig.codeCompletion,
        enableSecretRedaction: false,
      });
      expect(configProvider.get().httpAgentOptions).toEqual({
        ...defaultConfig.httpAgentOptions,
        certKey,
      });
    });
  });

  describe('telemetryNotificationHandler', () => {
    beforeEach(() => {
      const configProvider = createFakePartial<ConfigProvider>({});
      const api = createFakePartial<IGitLabAPI>({});

      messageHandler = new MessageHandler({
        connection: createFakePartial<Connection>({}),
        api,
        aiGateway,
        tracker,
        configProvider,
        documentTransformer,
        lsFetch,
        treeSitterParser,
      });
    });
    const trackingId = 'uniqueId';
    describe.each([
      [TRACKING_EVENTS.ACCEPTED],
      [TRACKING_EVENTS.REJECTED],
      [TRACKING_EVENTS.CANCELLED],
      [TRACKING_EVENTS.SHOWN],
      [TRACKING_EVENTS.NOT_PROVIDED],
    ])('%s event', (trackingEvent: TRACKING_EVENTS) => {
      it('should be tracked when client can notify about it', async () => {
        (tracker.canClientTrackEvent as jest.Mock).mockReturnValue(true);

        await messageHandler.telemetryNotificationHandler({
          category: CODE_SUGGESTIONS_CATEGORY,
          action: trackingEvent,
          context: {
            trackingId,
          },
        });

        expect(tracker.updateSuggestionState).toHaveBeenCalledWith(trackingId, trackingEvent);
      });

      it('should NOT be tracked when client cannot notify about it', () => {
        (tracker.canClientTrackEvent as jest.Mock).mockReturnValue(false);
        expect(
          messageHandler.telemetryNotificationHandler({
            category: CODE_SUGGESTIONS_CATEGORY,
            action: trackingEvent,
            context: {
              trackingId,
            },
          }),
        );
        expect(tracker.updateSuggestionState).not.toHaveBeenCalledWith(trackingId, trackingEvent);
      });
    });
  });

  describe('completionHandler && inlineCompletionHandler', () => {
    const isAtOrNearEndOfLineListener = jest.spyOn(completionFilters, 'isAtOrNearEndOfLine');
    isAtOrNearEndOfLineListener.mockReturnValue(true);
    const mockGetCodeSuggestions = jest.fn();

    let api = createFakePartial<IGitLabAPI>({
      getCodeSuggestions: mockGetCodeSuggestions,
    });

    afterEach(() => {
      mockGetCodeSuggestions.mockReset();
    });

    describe('completion', () => {
      const mockGetContext = jest.fn().mockReturnValue(LONG_COMPLETION_CONTEXT);
      let configProvider: ConfigProvider;
      let token: CancellationToken;

      const requestCompletionNoDebounce = (
        params: CompletionParams,
        tkn: CancellationToken,
      ): Promise<CompletionItem[]> => {
        const result = messageHandler.completionHandler(params, tkn);
        jest.advanceTimersByTime(DEBOUNCE_INTERVAL_MS);
        return result;
      };

      beforeEach(async () => {
        token = createFakePartial<CancellationToken>({ isCancellationRequested: false });
        configProvider = new ConfigProvider();
        configProvider.set('token', 'abc');
        api = createFakePartial<IGitLabAPI>({
          getCodeSuggestions: mockGetCodeSuggestions,
          configureApi: jest.fn(),
          checkToken: jest.fn().mockResolvedValue({ valid: true }),
        });

        const mockedDocumentTransformer = createFakePartial<DocumentTransformer>({
          getContext: mockGetContext,
        });

        messageHandler = new MessageHandler({
          connection: createFakePartial<Connection>({
            sendNotification: jest.fn().mockResolvedValue({}),
          }),
          api,
          aiGateway,
          tracker,
          configProvider,
          lsFetch,
          documentTransformer: mockedDocumentTransformer,
          treeSitterParser,
        });
      });

      it('sends a suggestion when all setup is present', async () => {
        mockGetCodeSuggestions.mockReturnValueOnce({
          choices: [{ text: 'mock suggestion' }],
          model: {
            lang: 'js',
          },
        });

        const result = await requestCompletionNoDebounce(COMPLETION_PARAMS, token);
        expect(result.length).toEqual(1);
        const [suggestedItem] = result;
        expect(suggestedItem.insertText).toBe('mock suggestion');
        expect(suggestedItem.command).toEqual({
          title: expect.any(String),
          command: SUGGESTION_ACCEPTED_COMMAND,
          arguments: [TRACKING_ID],
        });
      });

      it('requests completion including projectPath from workspace settings', async () => {
        configProvider.set('projectPath', 'gitlab-org/gitlab-vscode-extension');
        await requestCompletionNoDebounce(COMPLETION_PARAMS, token);
        expect(api.getCodeSuggestions).toHaveBeenCalledWith(
          expect.objectContaining({ project_path: 'gitlab-org/gitlab-vscode-extension' }),
        );
      });

      it('does not send a suggestion when context is short', async () => {
        mockGetContext.mockReturnValueOnce(SHORT_COMPLETION_CONTEXT);

        const result = await requestCompletionNoDebounce(COMPLETION_PARAMS, token);

        expect(result).toEqual([]);
      });

      it('does not send a suggestion when in middle of line', async () => {
        isAtOrNearEndOfLineListener.mockReturnValueOnce(false);

        const result = await requestCompletionNoDebounce(COMPLETION_PARAMS, token);

        expect(result).toEqual([]);
      });

      describe.each`
        flag     | callsToMonolith | callsToGateway
        ${true}  | ${0}            | ${1}
        ${false} | ${1}            | ${0}
      `(
        'when codeSuggestionsClientDirectToGateway FF is $flag',
        ({ flag, callsToMonolith, callsToGateway }) => {
          beforeEach(async () => {
            await messageHandler.didChangeConfigurationHandler({
              settings: {
                featureFlags: {
                  codeSuggestionsClientDirectToGateway: flag,
                },
              },
            });
          });

          it('handles routing to monolith or gateway', async () => {
            await requestCompletionNoDebounce(COMPLETION_PARAMS, token);

            expect(api.getCodeSuggestions).toHaveBeenCalledTimes(callsToMonolith);
            expect(aiGateway.fetchCompletions).toHaveBeenCalledTimes(callsToGateway);
          });
        },
      );

      describe('Suggestions not provided', () => {
        it('should track suggestion not provided when no choices returned', async () => {
          mockGetCodeSuggestions.mockReturnValueOnce(() => ({
            choices: [],
            model: {
              lang: 'js',
            },
          }));

          const result = await requestCompletionNoDebounce(COMPLETION_PARAMS, token);
          expect(tracker.updateSuggestionState).toHaveBeenCalledWith(
            expect.any(String),
            TRACKING_EVENTS.NOT_PROVIDED,
          );
          expect(result).toEqual([]);
        });

        it('should track suggestion not provided when every choice is empty', async () => {
          mockGetCodeSuggestions.mockReturnValueOnce(() => ({
            choices: [{ text: '' }, { text: undefined }],
            model: {
              lang: 'js',
            },
          }));

          const result = await requestCompletionNoDebounce(COMPLETION_PARAMS, token);
          expect(tracker.updateSuggestionState).toHaveBeenCalledWith(
            expect.any(String),
            TRACKING_EVENTS.NOT_PROVIDED,
          );
          expect(result).toEqual([]);
        });
      });

      describe('Suggestions error', () => {
        const errorStatusCode = 400;

        const response = createFakeResponse({
          url: 'https://example.com/api/v4/project',
          status: errorStatusCode,
          text: 'Bad Request',
        });

        it('should update code suggestion context with error status', async () => {
          mockGetCodeSuggestions.mockRejectedValueOnce(new FetchError(response, 'completion'));

          await requestCompletionNoDebounce(COMPLETION_PARAMS, token);

          expect(tracker.updateCodeSuggestionsContext).toHaveBeenCalledWith(expect.any(String), {
            status: errorStatusCode,
          });
        });
      });

      describe('Circuit breaking', () => {
        const getCompletions = async () => {
          const result = await requestCompletionNoDebounce(COMPLETION_PARAMS, token);
          return result;
        };
        const turnOnCircuitBreaker = async () => {
          await getCompletions();
          await getCompletions();
          await getCompletions();
          await getCompletions();
        };

        it('starts breaking after 4 errors', async () => {
          mockGetCodeSuggestions.mockResolvedValue({
            choices: [{ text: 'mock suggestion' }],
            model: {
              lang: 'js',
            },
          });
          const successResult = await getCompletions();
          expect(successResult.length).toEqual(1);
          mockGetCodeSuggestions.mockRejectedValue(new Error('test problem'));
          await turnOnCircuitBreaker();

          mockGetCodeSuggestions.mockReset();
          mockGetCodeSuggestions.mockResolvedValue({
            choices: [{ text: 'mock suggestion' }],
            model: {
              lang: 'js',
            },
          });

          const result = await getCompletions();
          expect(result).toEqual([]);
          expect(mockGetCodeSuggestions).not.toHaveBeenCalled();
        });

        it(`fetches completions again after circuit breaker's break time elapses`, async () => {
          jest.useFakeTimers().setSystemTime(new Date(Date.now()));

          mockGetCodeSuggestions.mockRejectedValue(new Error('test problem'));
          await turnOnCircuitBreaker();

          mockGetCodeSuggestions.mockReset();
          mockGetCodeSuggestions.mockResolvedValue({
            choices: [{ text: 'mock suggestion' }],
            model: {
              lang: 'js',
            },
          });
          jest.advanceTimersByTime(CIRCUIT_BREAK_INTERVAL_MS + 1);

          const result = await getCompletions();

          expect(result).toHaveLength(1);
          expect(mockGetCodeSuggestions).toHaveBeenCalled();
        });
      });

      describe('Debouncing', () => {
        beforeEach(() => {
          mockGetCodeSuggestions.mockResolvedValue({
            choices: [{ text: 'mock suggestion' }],
            model: {
              lang: 'js',
            },
          });
        });

        it('returns empty result if token was cancelled before debounce interval', async () => {
          const testToken = { isCancellationRequested: false };

          const completionPromise = messageHandler.completionHandler(
            COMPLETION_PARAMS,
            testToken as CancellationToken,
          );
          testToken.isCancellationRequested = true;
          jest.advanceTimersByTime(DEBOUNCE_INTERVAL_MS);

          const result = await completionPromise;
          expect(result).toEqual([]);
        });

        it('continues to call API if token has not been cancelled before debounce interval', async () => {
          const testToken = { isCancellationRequested: false };
          const completionPromise = messageHandler.completionHandler(
            COMPLETION_PARAMS,
            testToken as CancellationToken,
          );
          jest.advanceTimersByTime(DEBOUNCE_INTERVAL_MS);

          const result = await completionPromise;
          expect(result.length).toEqual(1);
        });
      });
    });

    describe('inlineCompletion', () => {
      const mockGetContext = jest.fn().mockReturnValue(LONG_COMPLETION_CONTEXT);
      const mockConnection = createFakePartial<Connection>({
        sendNotification: jest.fn().mockResolvedValue({}),
      });
      let configProvider: ConfigProvider;

      const inlineCompletionParams: InlineCompletionParams = {
        ...COMPLETION_PARAMS,
        position: Position.create(1, 1),
        context: {
          triggerKind: InlineCompletionTriggerKind.Invoked,
        },
      };

      const requestInlineCompletionNoDebounce = (
        params: InlineCompletionParams,
        tkn: CancellationToken,
      ): Promise<InlineCompletionList> => {
        const result = messageHandler.inlineCompletionHandler(params, tkn);
        jest.advanceTimersByTime(DEBOUNCE_INTERVAL_MS);
        return result;
      };
      let token: CancellationToken;
      const mockGetIntent = jest.fn();

      beforeEach(async () => {
        token = createFakePartial<CancellationToken>({ isCancellationRequested: false });
        configProvider = new ConfigProvider();
        configProvider.set('token', 'abc');

        const mockedDocumentTransformer = createFakePartial<DocumentTransformer>({
          getContext: mockGetContext,
          get: jest.fn(),
        });
        messageHandler = new MessageHandler({
          connection: mockConnection,
          api,
          aiGateway,
          tracker,
          configProvider,
          documentTransformer: mockedDocumentTransformer,
          lsFetch,
          treeSitterParser: createFakePartial<TreeSitterParser>({
            getIntent: mockGetIntent,
          }),
        });
      });

      describe('Streaming', () => {
        beforeEach(async () => {
          configProvider.set('featureFlags', { streamCodeGenerations: true });
          mockGetIntent.mockResolvedValueOnce('generation');
        });

        it('should return empty response when suggestion was cancelled', async () => {
          const cancellationToken = { isCancellationRequested: false };
          const promise = requestInlineCompletionNoDebounce(
            inlineCompletionParams,
            cancellationToken as CancellationToken,
          );
          cancellationToken.isCancellationRequested = true;
          const response = await promise;

          expect(response.items).toEqual([]);
        });

        it('does not request completion suggestions', async () => {
          await requestInlineCompletionNoDebounce(inlineCompletionParams, token);
          expect(mockGetCodeSuggestions).not.toHaveBeenCalled();
        });

        it('should return empty response with streaming command', async () => {
          const response = await requestInlineCompletionNoDebounce(inlineCompletionParams, token);
          expect(response.items[0]).toMatchObject({
            insertText: '',
            command: {
              title: 'Start streaming',
              command: START_STREAMING_COMMAND,
              arguments: [expect.stringContaining('code-suggestion-stream-'), TRACKING_ID],
            },
          });
        });

        it('should start the stream', async () => {
          await requestInlineCompletionNoDebounce(inlineCompletionParams, token);
          jest.runOnlyPendingTimers();
          expect(startStreaming).toHaveBeenCalledWith(
            expect.stringContaining('code-suggestion-stream-'),
            LONG_COMPLETION_CONTEXT,
            mockConnection,
            api,
            expect.any(CircuitBreaker),
            tracker,
            TRACKING_ID,
          );
        });
      });

      describe('when inline completion context does not match selected document text', () => {
        beforeEach(() => {
          jest
            .mocked(completionFilters.shouldRejectCompletionWithSelectedCompletionTextMismatch)
            .mockReset()
            .mockReturnValueOnce(true);
        });

        it('does not request suggestions', async () => {
          const { items } = await requestInlineCompletionNoDebounce(inlineCompletionParams, token);

          expect(mockGetCodeSuggestions).not.toHaveBeenCalled();
          expect(items).toHaveLength(0);
        });
      });

      it('sends a suggestion when all setup is present', async () => {
        mockGetCodeSuggestions.mockReturnValueOnce({
          choices: [{ text: 'mock suggestion' }],
          model: {
            lang: 'js',
          },
        });

        const { items } = await requestInlineCompletionNoDebounce(inlineCompletionParams, token);

        expect(items.length).toEqual(1);
        const [suggestedItem] = items;
        expect(suggestedItem.insertText).toBe('mock suggestion');
        expect(suggestedItem.command).toEqual({
          title: expect.any(String),
          command: SUGGESTION_ACCEPTED_COMMAND,
          arguments: [TRACKING_ID],
        });
        expect(suggestedItem.range).toEqual(
          Range.create(Position.create(1, 1), Position.create(1, 1)),
        );
      });

      it('requests inline completion including projectPath from workspace settings', async () => {
        configProvider.set('projectPath', 'gitlab-org/editor-extensions/gitlab.vim');
        await requestInlineCompletionNoDebounce(inlineCompletionParams, token);
        expect(api.getCodeSuggestions).toHaveBeenCalledWith(
          expect.objectContaining({ project_path: 'gitlab-org/editor-extensions/gitlab.vim' }),
        );
      });

      it('sets cache on successful request', async () => {
        mockGetCodeSuggestions.mockReturnValueOnce({
          choices: [{ text: 'mock suggestion' }],
          model: {
            lang: 'js',
          },
        });

        const cacheMock = jest
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          .mocked(jest.mocked(SuggestionsCache).mock.instances.at(-1)!);

        const { items } = await requestInlineCompletionNoDebounce(inlineCompletionParams, token);
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const [cacheArgs] = jest.mocked(cacheMock.addToSuggestionCache).mock.calls.at(-1)!;
        expect(cacheArgs.suggestions[0]?.uniqueTrackingId).toBe(TRACKING_ID);
        expect(cacheArgs.suggestions.map((s) => s.text)).toEqual(items.map((i) => i.insertText));
        expect(cacheArgs.request.position).toEqual(inlineCompletionParams.position);
      });

      it('does not send request when data is available in cache', async () => {
        const cacheMock = jest
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          .mocked(jest.mocked(SuggestionsCache).mock.instances.at(-1)!);

        cacheMock.getCachedSuggestions.mockReturnValueOnce([
          { text: 'cached suggestion', uniqueTrackingId: 'cached id' },
        ]);

        const { items } = await requestInlineCompletionNoDebounce(inlineCompletionParams, token);

        expect(cacheMock.getCachedSuggestions).toHaveBeenCalledTimes(1);
        expect(mockGetCodeSuggestions).toHaveBeenCalledTimes(0);
        expect(items.length).toEqual(1);
        const [suggestedItem] = items;
        expect(suggestedItem.insertText).toBe('cached suggestion');
        expect(suggestedItem.command).toEqual({
          title: expect.any(String),
          command: SUGGESTION_ACCEPTED_COMMAND,
          arguments: ['cached id'],
        });
        expect(suggestedItem.range).toEqual(
          Range.create(Position.create(1, 1), Position.create(1, 1)),
        );
      });

      it('tracks cached entries with full telemetry', async () => {
        const cacheMock = jest
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          .mocked(jest.mocked(SuggestionsCache).mock.instances.at(-1)!);

        cacheMock.getCachedSuggestions.mockReturnValueOnce([
          { text: 'cached suggestion', uniqueTrackingId: 'cached id' },
        ]);

        await requestInlineCompletionNoDebounce(inlineCompletionParams, token);
        expect(tracker.setCodeSuggestionsContext).toHaveBeenCalledWith(
          'cached id',
          expect.anything(),
          'cache',
        );
        expect(tracker.updateSuggestionState).toHaveBeenCalledWith(
          'cached id',
          TRACKING_EVENTS.LOADED,
        );
        expect(tracker.updateSuggestionState).toHaveBeenCalledWith(
          'cached id',
          TRACKING_EVENTS.SHOWN,
        );
      });

      it('does not send a suggestion when context is short', async () => {
        mockGetContext.mockReturnValueOnce(SHORT_COMPLETION_CONTEXT);

        const { items } = await requestInlineCompletionNoDebounce(inlineCompletionParams, token);

        expect(items).toEqual([]);
      });

      it('does not send a suggestion when in middle of line', async () => {
        isAtOrNearEndOfLineListener.mockReturnValueOnce(false);

        const { items } = await requestInlineCompletionNoDebounce(inlineCompletionParams, token);

        expect(items).toEqual([]);
      });

      it('tracks suggestion cancelled if the suggestion request has been cancelled before API responded', async () => {
        const testToken = { isCancellationRequested: false };

        mockGetCodeSuggestions.mockImplementation(async () => {
          // simulate that request has been cancelled before API responded
          testToken.isCancellationRequested = true;
          return {
            choices: [{ text: 'mock suggestion' }],
            model: {
              lang: 'js',
            },
          };
        });

        const { items } = await requestInlineCompletionNoDebounce(
          inlineCompletionParams,
          testToken as CancellationToken,
        );

        expect(items).toEqual([]);
        expect(tracker.updateSuggestionState).toHaveBeenCalledWith(
          expect.any(String),
          TRACKING_EVENTS.CANCELLED,
        );
      });

      describe('Suggestions not provided', () => {
        it('should track suggestion not provided when no choices returned', async () => {
          mockGetCodeSuggestions.mockReturnValueOnce(() => ({
            choices: [],
            model: {
              lang: 'js',
            },
          }));

          const result = await requestInlineCompletionNoDebounce(inlineCompletionParams, token);
          expect(tracker.updateSuggestionState).toHaveBeenCalledWith(
            expect.any(String),
            TRACKING_EVENTS.NOT_PROVIDED,
          );
          expect(result.items).toEqual([]);
        });

        it('should track suggestion not provided when every choice is empty', async () => {
          mockGetCodeSuggestions.mockReturnValueOnce(() => ({
            choices: [{ text: '' }, { text: undefined }],
            model: {
              lang: 'js',
            },
          }));

          const result = await requestInlineCompletionNoDebounce(inlineCompletionParams, token);
          expect(tracker.updateSuggestionState).toHaveBeenCalledWith(
            expect.any(String),
            TRACKING_EVENTS.NOT_PROVIDED,
          );
          expect(result.items).toEqual([]);
        });
      });

      describe('Suggestions error', () => {
        const errorStatusCode = 400;

        const response = createFakeResponse({
          url: 'https://example.com/api/v4/project',
          status: errorStatusCode,
          text: 'Bad Request',
        });

        it('should update code suggestion context with error status', async () => {
          mockGetCodeSuggestions.mockRejectedValueOnce(new FetchError(response, 'completion'));

          await requestInlineCompletionNoDebounce(inlineCompletionParams, token);

          expect(tracker.updateCodeSuggestionsContext).toHaveBeenCalledWith(expect.any(String), {
            status: errorStatusCode,
          });
        });
      });

      describe('Circuit breaking', () => {
        const getCompletions = async () => {
          const result = await requestInlineCompletionNoDebounce(inlineCompletionParams, token);
          return result;
        };
        const turnOnCircuitBreaker = async () => {
          await getCompletions();
          await getCompletions();
          await getCompletions();
          await getCompletions();
        };

        describe('Completion', () => {
          it('starts breaking after 4 errors', async () => {
            mockGetCodeSuggestions.mockReset();
            mockGetCodeSuggestions.mockResolvedValue({
              choices: [{ text: 'mock suggestion' }],
              model: {
                lang: 'js',
              },
            });
            const successResult = await getCompletions();
            expect(successResult.items.length).toEqual(1);
            mockGetCodeSuggestions.mockRejectedValue(new Error('test problem'));
            await turnOnCircuitBreaker();

            mockGetCodeSuggestions.mockReset();
            mockGetCodeSuggestions.mockResolvedValue({
              choices: [{ text: 'mock suggestion' }],
              model: {
                lang: 'js',
              },
            });

            const result = await getCompletions();
            expect(result?.items).toEqual([]);
            expect(mockGetCodeSuggestions).not.toHaveBeenCalled();
          });

          it(`fetches completions again after circuit breaker's break time elapses`, async () => {
            jest.useFakeTimers().setSystemTime(new Date(Date.now()));

            mockGetCodeSuggestions.mockRejectedValue(new Error('test problem'));
            await turnOnCircuitBreaker();

            mockGetCodeSuggestions.mockReset();
            mockGetCodeSuggestions.mockResolvedValue({
              choices: [{ text: 'mock suggestion' }],
              model: {
                lang: 'js',
              },
            });
            jest.advanceTimersByTime(CIRCUIT_BREAK_INTERVAL_MS + 1);

            const result = await getCompletions();

            expect(result?.items).toHaveLength(1);
            expect(mockGetCodeSuggestions).toHaveBeenCalled();
          });
        });

        describe('Streaming', () => {
          function goIntoStreamingMode() {
            mockGetIntent.mockReset();
            configProvider.set('featureFlags', { streamCodeGenerations: true });
            mockGetIntent.mockResolvedValue('generation');
          }

          function goIntoCompletionMode() {
            mockGetIntent.mockReset();
            configProvider.set('featureFlags', { streamCodeGenerations: false });
            mockGetIntent.mockResolvedValue('completion');
          }

          it('starts breaking after 4 errors', async () => {
            (startStreaming as jest.Mock).mockClear();
            goIntoStreamingMode();
            const successResult = await getCompletions();
            expect(successResult.items.length).toEqual(1);
            jest.runOnlyPendingTimers();
            expect(startStreaming).toHaveBeenCalled();
            (startStreaming as jest.Mock).mockClear();
            goIntoCompletionMode();

            mockGetCodeSuggestions.mockRejectedValue('test problem');
            await turnOnCircuitBreaker();

            goIntoStreamingMode();

            const result = await getCompletions();
            expect(result?.items).toEqual([]);
            expect(startStreaming).not.toHaveBeenCalled();
          });

          it(`starts the stream after circuit breaker's break time elapses`, async () => {
            jest.useFakeTimers().setSystemTime(new Date(Date.now()));

            goIntoCompletionMode();
            mockGetCodeSuggestions.mockRejectedValue(new Error('test problem'));
            await turnOnCircuitBreaker();

            jest.advanceTimersByTime(CIRCUIT_BREAK_INTERVAL_MS + 1);
            goIntoStreamingMode();
            const result = await getCompletions();
            expect(result?.items).toHaveLength(1);
            jest.runOnlyPendingTimers();
            expect(startStreaming).toHaveBeenCalled();
          });
        });
      });

      describe('selection completion info', () => {
        beforeEach(() => {
          mockGetCodeSuggestions.mockReset();
          mockGetCodeSuggestions.mockResolvedValue({
            choices: [{ text: 'log("Hello world")' }],
            model: {
              lang: 'js',
            },
          });
        });

        describe('when undefined', () => {
          it('does not update choices', async () => {
            const { items } = await requestInlineCompletionNoDebounce(
              {
                ...inlineCompletionParams,
                context: createFakePartial<InlineCompletionContext>({
                  selectedCompletionInfo: undefined,
                }),
              },
              token,
            );

            expect(items[0].insertText).toBe('log("Hello world")');
          });
        });

        describe('with range and text', () => {
          it('prepends text to suggestion choices', async () => {
            const { items } = await requestInlineCompletionNoDebounce(
              {
                ...inlineCompletionParams,
                context: createFakePartial<InlineCompletionContext>({
                  selectedCompletionInfo: {
                    text: 'console.',
                    range: { start: { line: 1, character: 0 }, end: { line: 1, character: 2 } },
                  },
                }),
              },
              token,
            );

            expect(items[0].insertText).toBe('nsole.log("Hello world")');
          });
        });

        describe('with range (Array) and text', () => {
          it('prepends text to suggestion choices', async () => {
            const { items } = await requestInlineCompletionNoDebounce(
              {
                ...inlineCompletionParams,
                context: createFakePartial<InlineCompletionContext>({
                  selectedCompletionInfo: {
                    text: 'console.',
                    // NOTE: This forcefully simulates the behavior we see where range is an Array at runtime.
                    range: [
                      { line: 1, character: 0 },
                      { line: 1, character: 2 },
                    ] as unknown as Range,
                  },
                }),
              },
              token,
            );

            expect(items[0].insertText).toBe('nsole.log("Hello world")');
          });
        });
      });
    });
  });

  describe('didChangeWorkspaceFoldersHandler', () => {
    const wf1: WorkspaceFolder = { uri: '//Users/workspace/1', name: 'workspace1' };
    const wf2: WorkspaceFolder = { uri: '//Users/workspace/2', name: 'workspace2' };
    const wf3: WorkspaceFolder = { uri: '//Users/workspace/3', name: 'workspace3' };

    it('adds and removes folders', async () => {
      const configProvider = new ConfigProvider();
      const api = createFakePartial<IGitLabAPI>({});

      messageHandler = new MessageHandler({
        connection: createFakePartial<Connection>({}),
        api,
        aiGateway,
        tracker,
        configProvider,
        documentTransformer,
        lsFetch,
        treeSitterParser,
      });
      messageHandler.didChangeWorkspaceFoldersHandler({ added: [wf1, wf2], removed: [] });

      expect(configProvider.get().workspaceFolders).toHaveLength(2);
      expect(configProvider.get().workspaceFolders).toEqual([wf1, wf2]);

      messageHandler.didChangeWorkspaceFoldersHandler({ added: [wf3], removed: [wf2] });

      expect(configProvider.get().workspaceFolders).toHaveLength(2);
      expect(configProvider.get().workspaceFolders).toEqual([wf1, wf3]);
    });
  });
});
