import fetch from 'cross-fetch';

export interface FetchAgentOptions {
  ignoreCertificateErrors: boolean;
  ca?: string;
  cert?: string;
  certKey?: string;
}

export interface IFetch {
  initialize(): Promise<void>;
  destroy(): Promise<void>;
  fetch(input: RequestInfo | URL, init?: RequestInit): Promise<Response>;
  fetchBase(input: RequestInfo | URL, init?: RequestInit): Promise<Response>;
  delete(input: RequestInfo | URL, init?: RequestInit): Promise<Response>;
  get(input: RequestInfo | URL, init?: RequestInit): Promise<Response>;
  post(input: RequestInfo | URL, init?: RequestInit): Promise<Response>;
  put(input: RequestInfo | URL, init?: RequestInit): Promise<Response>;
  updateAgentOptions(options: FetchAgentOptions): Promise<void>;
}

export class FetchBase implements IFetch {
  updateRequestInit(method: string, init?: RequestInit): RequestInit {
    if (typeof init === 'undefined') {
      init = { method: method };
    } else {
      init.method = method;
    }

    return init;
  }

  async initialize(): Promise<void> {}
  async destroy(): Promise<void> {}

  async fetch(input: RequestInfo | URL, init?: RequestInit): Promise<Response> {
    return fetch(input, init);
  }

  async delete(input: RequestInfo | URL, init?: RequestInit): Promise<Response> {
    return this.fetch(input, this.updateRequestInit('DELETE', init));
  }

  async get(input: RequestInfo | URL, init?: RequestInit): Promise<Response> {
    return this.fetch(input, this.updateRequestInit('GET', init));
  }

  async post(input: RequestInfo | URL, init?: RequestInit): Promise<Response> {
    return this.fetch(input, this.updateRequestInit('POST', init));
  }

  async put(input: RequestInfo | URL, init?: RequestInit): Promise<Response> {
    return this.fetch(input, this.updateRequestInit('PUT', init));
  }

  async fetchBase(input: RequestInfo | URL, init?: RequestInit): Promise<Response> {
    const _global = typeof global === 'undefined' ? self : global;

    return _global.fetch(input, init);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  updateAgentOptions(_opts: FetchAgentOptions): Promise<void> {
    return Promise.resolve();
  }
}
