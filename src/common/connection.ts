import { CompletionItem, Connection, InlineCompletionRequest } from 'vscode-languageserver';
import { ConfigProvider } from './config';
import { GitLabAPI } from './api';
import { DocumentTransformer } from './documents';
import { IFetch } from './fetch';
import { SnowplowTracker, TELEMETRY_NOTIFICATION } from './tracking/snowplow_tracker';
import { MessageHandler } from './message_handler';
import { DefaultAiGatewayClient } from './ai_gateway';
import { TreeSitterParser } from './tree_sitter';

export function setup(
  connection: Connection,
  documents: DocumentTransformer,
  lsFetch: IFetch,
  configProvider: ConfigProvider,
  { treeSitterParser }: { treeSitterParser: TreeSitterParser },
) {
  const api = new GitLabAPI(lsFetch);
  const aiGateway = new DefaultAiGatewayClient();
  const tracker = new SnowplowTracker(lsFetch);
  const messageHandler = new MessageHandler({
    connection,
    configProvider,
    api,
    aiGateway,
    tracker,
    treeSitterParser,
    lsFetch,
    documentTransformer: documents,
  });

  connection.onInitialize(messageHandler.onInitializeHandler);

  connection.onCompletion(messageHandler.completionHandler);
  // TODO: does Visual Studio or Neovim need this? VS Code doesn't
  connection.onCompletionResolve((item: CompletionItem) => item);
  connection.onRequest(InlineCompletionRequest.type, messageHandler.inlineCompletionHandler);

  connection.onDidChangeConfiguration(messageHandler.didChangeConfigurationHandler);

  connection.onNotification(TELEMETRY_NOTIFICATION, messageHandler.telemetryNotificationHandler);

  connection.onNotification(
    'workspace/didChangeWorkspaceFolders',
    messageHandler.didChangeWorkspaceFoldersHandler,
  );

  connection.onShutdown(messageHandler.onShutdownHandler);
}
