import type { IClientInfo } from '../config';

export interface CompletionsRequest {
  // TODO: https://gitlab.com/gitlab-org/gitlab/-/issues/433433
}

export interface CompletionsResponse {
  // TODO: https://gitlab.com/gitlab-org/gitlab/-/issues/433433
}

export interface GenerationsRequest {
  // TODO: https://gitlab.com/gitlab-org/gitlab/-/issues/433433
}

export interface GenerationsResponse {
  // TODO: https://gitlab.com/gitlab-org/gitlab/-/issues/433433
}

export interface AiGatewayClient {
  setClientInfo(clientInfo: IClientInfo): void;

  fetchCompletions(request: CompletionsRequest): Promise<undefined>;

  fetchGenerations(request: GenerationsRequest): Promise<undefined>;
}

/**
 * This handles direct communication with the AiGateway
 *
 * It will receive a IGitLabApi to manage AiGateway service location and token management.
 */
export class DefaultAiGatewayClient implements AiGatewayClient {
  setClientInfo(): void {
    // TODO: https://gitlab.com/gitlab-org/gitlab/-/issues/433433
  }

  /**
   * Reaches out to `/completions` endpoint of AiGateway
   *
   */
  fetchCompletions(): Promise<undefined> {
    // TODO: https://gitlab.com/gitlab-org/gitlab/-/issues/433433
    throw new Error('[DefaultAiGatewayClient] Unimplemented method: fetchCompletions');
  }

  /**
   * Reaches out to `/generations` endpoint of AiGateway
   */
  fetchGenerations(): Promise<undefined> {
    // TODO: https://gitlab.com/gitlab-org/gitlab/-/issues/433433
    throw new Error('[DefaultAiGatewayClient] Unimplemented method: fetchGenerations');
  }
}
