import { DefaultAiGatewayClient } from './client';

describe('DefaultAiGatewayClient', () => {
  let subject: DefaultAiGatewayClient;

  beforeEach(() => {
    subject = new DefaultAiGatewayClient();
  });

  describe('setClientInfo', () => {
    it('does nothing for now', () => {
      expect(subject.setClientInfo).not.toThrow();
    });
  });

  describe('fetchCompletions', () => {
    it('throws for now', () => {
      expect(subject.fetchCompletions).toThrowError(
        '[DefaultAiGatewayClient] Unimplemented method: fetchCompletions',
      );
    });
  });

  describe('fetchGenerations', () => {
    it('throws for now', () => {
      expect(subject.fetchGenerations).toThrowError(
        '[DefaultAiGatewayClient] Unimplemented method: fetchGenerations',
      );
    });
  });
});
