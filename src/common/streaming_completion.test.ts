import { Connection } from 'vscode-languageserver';
import { createFakePartial } from './test_utils/create_fake_partial';
import {
  StreamWithId,
  StreamingCompletionResponse,
  CancelStreaming,
  startStreaming,
} from './streaming_completion';
import { IGitLabAPI } from './api';
import { waitMs } from './message_handler';
import { CircuitBreaker } from './circuit_breaker/circuit_breaker';
import { SnowplowTracker, TRACKING_EVENTS } from './tracking/snowplow_tracker';
import { FetchError } from './fetch_error';
import { createFakeResponse } from './test_utils/create_fake_response';

const uniqueTrackingId = SnowplowTracker.uniqueTrackingId();
jest.mock('./message_handler');

describe('startStreaming', () => {
  const streamId = '1';
  let mockConnection: Connection;
  let mockAPI: IGitLabAPI;
  let mockCircuitBreaker: CircuitBreaker;
  const mockContext = {
    prefix: 'beforeCursor',
    suffix: 'afterCursor',
    filename: 'test.ts',
    position: {
      line: 0,
      character: 12,
    },
  };
  let mockTracker: SnowplowTracker;

  beforeEach(() => {
    jest.mocked(waitMs).mockResolvedValue(undefined);
    mockConnection = createFakePartial<Connection>({
      onNotification: jest.fn().mockReturnValue({
        dispose: () => {},
      }),
      sendNotification: jest.fn(),
    });
    mockAPI = createFakePartial<IGitLabAPI>({
      getStreamingCodeSuggestions: jest.fn(),
    });
    mockCircuitBreaker = createFakePartial<CircuitBreaker>({
      error: jest.fn(),
      close: jest.fn(),
      isOpen: jest.fn(),
    });
    mockTracker = createFakePartial<SnowplowTracker>({
      setCodeSuggestionsContext: jest.fn(),
      updateSuggestionState: jest.fn(),
      updateCodeSuggestionsContext: jest.fn(),
    });
  });
  afterEach(() => {
    (mockTracker.updateSuggestionState as jest.Mock).mockClear();
    (mockTracker.setCodeSuggestionsContext as jest.Mock).mockClear();
  });

  it('should call the streaming API and send notifications to the client', async () => {
    mockAPI.getStreamingCodeSuggestions = jest.fn().mockImplementation(async function* () {});

    await startStreaming(
      streamId,
      mockContext,
      mockConnection,
      mockAPI,
      mockCircuitBreaker,
      mockTracker,
      uniqueTrackingId,
    );

    expect(mockAPI.getStreamingCodeSuggestions).toBeCalledTimes(1);
    expect(mockConnection.sendNotification).toBeCalledWith(StreamingCompletionResponse, {
      id: streamId,
      done: true,
    });
  });

  it('when multiple messages are send back, sends multiple notifications', async () => {
    mockAPI.getStreamingCodeSuggestions = jest.fn().mockImplementation(async function* () {
      yield 'one';
      yield 'two';
    });

    await startStreaming(
      streamId,
      mockContext,
      mockConnection,
      mockAPI,
      mockCircuitBreaker,
      mockTracker,
      uniqueTrackingId,
    );

    expect(mockConnection.sendNotification).toHaveBeenNthCalledWith(
      1,
      StreamingCompletionResponse,
      {
        id: streamId,
        completion: 'one',
        done: false,
      },
    );
    expect(mockConnection.sendNotification).toHaveBeenNthCalledWith(
      2,
      StreamingCompletionResponse,
      {
        id: streamId,
        completion: 'two',
        done: false,
      },
    );
    expect(mockConnection.sendNotification).toHaveBeenNthCalledWith(
      3,
      StreamingCompletionResponse,
      {
        id: streamId,
        done: true,
      },
    );
  });

  describe('cancellation', () => {
    let cancellationCallback: (stream: StreamWithId) => void;

    beforeEach(() => {
      mockConnection.onNotification = jest.fn().mockImplementation((notificationType, c) => {
        if (notificationType === CancelStreaming) {
          cancellationCallback = c;
        }

        return {
          dispose: () => {},
        };
      });
    });

    it('cancels streaming on `CancelStreaming` notification', async () => {
      const promise = startStreaming(
        streamId,
        mockContext,
        mockConnection,
        mockAPI,
        mockCircuitBreaker,
        mockTracker,
        uniqueTrackingId,
      );

      // manually call the callback that should be called on receiving notification
      if (cancellationCallback) {
        cancellationCallback({ id: streamId });
      }
      await promise;

      expect(mockConnection.sendNotification).toHaveBeenCalledWith(StreamingCompletionResponse, {
        id: streamId,
        done: true,
      });
    });
  });

  describe('circuit breaker', () => {
    it('should close the circuit once streaming is starting', async () => {
      mockAPI.getStreamingCodeSuggestions = jest.fn().mockImplementation(async function* () {});

      await startStreaming(
        streamId,
        mockContext,
        mockConnection,
        mockAPI,
        mockCircuitBreaker,
        mockTracker,
        uniqueTrackingId,
      );

      expect(mockCircuitBreaker.close).toHaveBeenCalled();
    });

    it('should add to errors on api error', async () => {
      mockAPI.getStreamingCodeSuggestions = jest.fn().mockImplementation(async function* () {
        yield Promise.reject(new Error('error'));
      });

      await startStreaming(
        streamId,
        mockContext,
        mockConnection,
        mockAPI,
        mockCircuitBreaker,
        mockTracker,
        uniqueTrackingId,
      );

      expect(mockCircuitBreaker.error).toHaveBeenCalled();
    });

    it('should not proceed with API calls when the circuit is open', async () => {
      mockAPI.getStreamingCodeSuggestions = jest.fn().mockImplementation(async function* () {
        yield 1;
      });

      (mockCircuitBreaker.isOpen as jest.Mock).mockReturnValue(true);

      await startStreaming(
        streamId,
        mockContext,
        mockConnection,
        mockAPI,
        mockCircuitBreaker,
        mockTracker,
        uniqueTrackingId,
      );

      expect(mockConnection.sendNotification).not.toHaveBeenCalledWith(
        StreamingCompletionResponse,
        {
          completion: 1,
          done: false,
          id: streamId,
        },
      );
    });
  });

  describe('Telemetry', () => {
    describe('Successfully completed stream', () => {
      it('should track correct events', async () => {
        mockAPI.getStreamingCodeSuggestions = jest.fn().mockImplementation(async function* () {
          yield 'one';
          yield 'two';
        });
        await startStreaming(
          streamId,
          mockContext,
          mockConnection,
          mockAPI,
          mockCircuitBreaker,
          mockTracker,
          uniqueTrackingId,
        );

        expect(mockTracker.setCodeSuggestionsContext).toHaveBeenCalledWith(
          uniqueTrackingId,
          expect.any(Object),
          'network',
          true,
        );
        expect((mockTracker.updateSuggestionState as jest.Mock).mock.calls).toEqual([
          [uniqueTrackingId, TRACKING_EVENTS.STREAM_STARTED],
          [uniqueTrackingId, TRACKING_EVENTS.SHOWN],
          [uniqueTrackingId, TRACKING_EVENTS.STREAM_COMPLETED],
        ]);
      });
    });

    describe('Errored stream', () => {
      it('should track correct events', async () => {
        const errorStatusCode = 400;
        const response = createFakeResponse({
          url: 'https://example.com/api/v4/project',
          status: errorStatusCode,
          text: 'Bad Request',
        });
        mockAPI.getStreamingCodeSuggestions = jest.fn().mockImplementation(async function* () {
          yield 'one';
          yield Promise.reject(new FetchError(response, 'completion'));
        });
        await startStreaming(
          streamId,
          mockContext,
          mockConnection,
          mockAPI,
          mockCircuitBreaker,
          mockTracker,
          uniqueTrackingId,
        );

        expect(mockTracker.setCodeSuggestionsContext).toHaveBeenCalledWith(
          uniqueTrackingId,
          expect.any(Object),
          'network',
          true,
        );

        expect(mockTracker.updateCodeSuggestionsContext).toHaveBeenCalledWith(uniqueTrackingId, {
          status: errorStatusCode,
        });
        expect((mockTracker.updateSuggestionState as jest.Mock).mock.calls).toEqual([
          [uniqueTrackingId, TRACKING_EVENTS.STREAM_STARTED],
          [uniqueTrackingId, TRACKING_EVENTS.SHOWN],
          [uniqueTrackingId, TRACKING_EVENTS.ERRORED],
        ]);
      });
    });

    describe('Cancelled stream', () => {
      let cancellationCallback: (stream: StreamWithId) => void;

      beforeEach(() => {
        mockConnection.onNotification = jest.fn().mockImplementation((notificationType, c) => {
          if (notificationType === CancelStreaming) {
            cancellationCallback = c;
          }

          return {
            dispose: () => {},
          };
        });
      });

      it('should track correct events', async () => {
        mockAPI.getStreamingCodeSuggestions = jest.fn().mockImplementation(async function* () {
          yield 'one';
          yield 'two';
        });
        const promise = startStreaming(
          streamId,
          mockContext,
          mockConnection,
          mockAPI,
          mockCircuitBreaker,
          mockTracker,
          uniqueTrackingId,
        );

        // manually call the callback that should be called on receiving notification
        if (cancellationCallback) {
          cancellationCallback({ id: streamId });
        }
        await promise;

        expect(mockTracker.setCodeSuggestionsContext).toHaveBeenCalledWith(
          uniqueTrackingId,
          expect.any(Object),
          'network',
          true,
        );
        expect((mockTracker.updateSuggestionState as jest.Mock).mock.calls).toEqual([
          [uniqueTrackingId, TRACKING_EVENTS.CANCELLED],
        ]);
      });
    });

    describe('Empty stream (no suggestion provided)', () => {
      it('should track correct events', async () => {
        mockAPI.getStreamingCodeSuggestions = jest.fn().mockImplementation(async function* () {
          yield '    ';
          yield '';
        });
        await startStreaming(
          streamId,
          mockContext,
          mockConnection,
          mockAPI,
          mockCircuitBreaker,
          mockTracker,
          uniqueTrackingId,
        );

        expect(mockTracker.setCodeSuggestionsContext).toHaveBeenCalledWith(
          uniqueTrackingId,
          expect.any(Object),
          'network',
          true,
        );
        expect((mockTracker.updateSuggestionState as jest.Mock).mock.calls).toEqual([
          [uniqueTrackingId, TRACKING_EVENTS.STREAM_STARTED],
          [uniqueTrackingId, TRACKING_EVENTS.STREAM_COMPLETED],
          [uniqueTrackingId, TRACKING_EVENTS.NOT_PROVIDED],
        ]);
      });
    });

    describe('Rejected stream', () => {
      jest.useFakeTimers();
      let cancellationCallback: (stream: StreamWithId) => void;

      beforeEach(() => {
        mockConnection.onNotification = jest.fn().mockImplementation((notificationType, c) => {
          if (notificationType === CancelStreaming) {
            cancellationCallback = c;
          }

          return {
            dispose: () => {},
          };
        });
      });

      it('should track correct events', async () => {
        mockAPI.getStreamingCodeSuggestions = jest.fn().mockImplementation(async function* () {
          yield 'one';
          yield new Promise((resolve) => {
            // manually call the callback that should be called on receiving notification
            if (cancellationCallback) {
              cancellationCallback({ id: streamId });
            }
            resolve('two');
          });
        });
        await startStreaming(
          streamId,
          mockContext,
          mockConnection,
          mockAPI,
          mockCircuitBreaker,
          mockTracker,
          uniqueTrackingId,
        );

        jest.runOnlyPendingTimers();

        expect(mockTracker.setCodeSuggestionsContext).toHaveBeenCalledWith(
          uniqueTrackingId,
          expect.any(Object),
          'network',
          true,
        );
        expect((mockTracker.updateSuggestionState as jest.Mock).mock.calls).toEqual([
          [uniqueTrackingId, TRACKING_EVENTS.STREAM_STARTED],
          [uniqueTrackingId, TRACKING_EVENTS.SHOWN],
          [uniqueTrackingId, TRACKING_EVENTS.REJECTED],
        ]);
      });
    });
  });
});
