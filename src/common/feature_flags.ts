export enum FeatureFlags {
  // Should match names found in https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/blob/main/src/common/feature_flags/constants.ts
  CodeSuggestionsClientDirectToGateway = 'codeSuggestionsClientDirectToGateway',
  StreamCodeGenerations = 'streamCodeGenerations',
}
