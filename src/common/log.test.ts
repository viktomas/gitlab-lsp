import { ConfigProvider, IConfig } from './config';
import { log, ILog, LOG_LEVEL, LogLevel } from './log';

const logFunction: jest.SpyInstance = jest.spyOn(console, 'log');

describe('logging', () => {
  const setupLogger = (logLevel: LogLevel = LOG_LEVEL.INFO) =>
    log.setup({ get: () => ({ logLevel }) as IConfig } as ConfigProvider);

  afterEach(() => {
    expect.hasAssertions();
  });

  beforeEach(() => {
    logFunction.mockClear();
  });

  const getLoggedMessage = () => logFunction.mock.calls[0][0];

  describe('log', () => {
    describe('setup', () => {
      it('shows only info logs by default', () => {
        log.debug('message');
        expect(logFunction).not.toBeCalled();

        log.info('message 2');
        expect(getLoggedMessage()).toContain(`[info]: message 2`);
      });

      it('blocks logs when more constrained level is set', () => {
        setupLogger(LOG_LEVEL.ERROR);

        log.warn('message');
        expect(logFunction).not.toBeCalled();

        log.error('message 2');
        expect(getLoggedMessage()).toContain(`[error]: message 2`);
      });

      it('allows logs when more relaxed level is set', () => {
        setupLogger(LOG_LEVEL.DEBUG);

        log.debug('message');
        expect(getLoggedMessage()).toContain(`[debug]: message`);
      });

      it('defaults to "info" when invalid level is supplied by client', () => {
        setupLogger('other' as LogLevel);

        log.debug('message');
        expect(logFunction).not.toBeCalled();

        log.info('message 2');
        expect(getLoggedMessage()).toContain(`[info]: message 2`);
      });
    });

    describe('logging', () => {
      beforeEach(() => {
        setupLogger();
      });

      it('passes the argument to the handler', () => {
        const message = 'A very bad error occurred';
        log.info(message);
        expect(logFunction).toBeCalledTimes(1);
        expect(getLoggedMessage()).toContain(`[info]: ${message}`);
      });

      type TestTuple = [keyof ILog, LogLevel];

      it('does not log debug logs by default', () => {
        log.debug('message');
        expect(logFunction).not.toBeCalled();
      });

      it.each<TestTuple>([
        ['info', LOG_LEVEL.INFO],
        ['warn', LOG_LEVEL.WARNING],
        ['error', LOG_LEVEL.ERROR],
      ])('it handles log level "%s"', (methodName, logLevel) => {
        log[methodName]('message');
        expect(getLoggedMessage()).toContain(`[${logLevel}]: message`);
        expect(getLoggedMessage()).not.toMatch(/\s+Error\s+at.*log\.[jt]s/m);
      });

      it('indents multiline messages', () => {
        log.error('error happened\nand the next line\nexplains why');
        expect(getLoggedMessage()).toContain(
          `[error]: error happened\n    and the next line\n    explains why`,
        );
      });

      describe.each`
        exception             | error                 | expectation
        ${new Error('wrong')} | ${new Error('wrong')} | ${'returns the error object '}
        ${'error'}            | ${new Error('error')} | ${'transforms string into Error'}
        ${0}                  | ${'0'}                | ${'returns JSON.stringify of the exception otherwise'}
      `('get Error from unknown exception', ({ exception, error, expectation }) => {
        it(`${expectation}`, () => {
          log.error('test message', exception);
          expect(getLoggedMessage()).toContain(error.message || error);
        });
      });
    });

    describe('log Error', () => {
      it('passes the argument to the handler', () => {
        const message = 'A very bad error occurred';
        const error = {
          message,
          stack: 'stack',
        };
        log.error(error as Error);
        expect(getLoggedMessage()).toMatch(/\[error\]: A very bad error occurred\s+stack/m);
      });
    });
  });
});
