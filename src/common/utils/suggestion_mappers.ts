import { CodeSuggestionResponseChoice } from '../api';
import {
  CompletionItem,
  CompletionItemKind,
  InlineCompletionItem,
  InlineCompletionParams,
  Range,
} from 'vscode-languageserver';
import { SUGGESTION_ACCEPTED_COMMAND } from '../message_handler';
import { sanitizeRange } from './sanitize_range';

export const completionChoiceMapper = (
  choice: CodeSuggestionResponseChoice,
  index: number,
): CompletionItem => ({
  label: `GitLab Suggestion ${index + 1}: ${choice.text}`,
  kind: CompletionItemKind.Text,
  insertText: choice.text,
  detail: choice.text,
  command: {
    title: 'Accept suggestion',
    command: SUGGESTION_ACCEPTED_COMMAND,
    arguments: [choice.uniqueTrackingId],
  },
  data: {
    index,
    trackingId: choice.uniqueTrackingId,
  },
});

export const inlineCompletionChoiceMapper = (params: InlineCompletionParams) => {
  const completionInfo = params.context.selectedCompletionInfo;
  let rangeDiff = 0;

  if (completionInfo) {
    const range = sanitizeRange(completionInfo.range);
    rangeDiff = range.end.character - range.start.character;
  }

  return (choice: CodeSuggestionResponseChoice): InlineCompletionItem => ({
    insertText: completionInfo
      ? `${completionInfo.text.substring(rangeDiff)}${choice.text}`
      : choice.text,
    range: Range.create(params.position, params.position),
    command: {
      title: 'Accept suggestion',
      command: SUGGESTION_ACCEPTED_COMMAND,
      arguments: [choice.uniqueTrackingId],
    },
  });
};
