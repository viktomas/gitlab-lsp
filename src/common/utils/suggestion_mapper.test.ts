import { completionChoiceMapper, inlineCompletionChoiceMapper } from './suggestion_mappers';
import {
  CompletionItem,
  CompletionItemKind,
  InlineCompletionParams,
  Position,
  Range,
} from 'vscode-languageserver';
import { CodeSuggestionResponseChoice } from '../api';
import { SUGGESTION_ACCEPTED_COMMAND } from '../message_handler';
import { COMPLETION_PARAMS } from '../test_utils/mocks';

describe('completionChoiceMapper', () => {
  it('should map returned suggestion to `CompletionItem`', () => {
    const uniqueTrackingId = 'testTrackingId';
    const text = 'Some suggestion text';
    const choice: CodeSuggestionResponseChoice = {
      text,
      uniqueTrackingId,
    };
    const index = 0;
    const completionItem: CompletionItem = completionChoiceMapper(choice, index);

    expect(completionItem).toEqual(
      expect.objectContaining({
        label: `GitLab Suggestion 1: ${text}`,
        kind: CompletionItemKind.Text,
        insertText: text,
        command: {
          title: 'Accept suggestion',
          command: SUGGESTION_ACCEPTED_COMMAND,
          arguments: [uniqueTrackingId],
        },
        data: {
          index,
          trackingId: uniqueTrackingId,
        },
      }),
    );
  });
});

describe('inlineCompletionChoiceMapper', () => {
  it('should map returned suggestion to `InlineCompletionItem`', () => {
    const position = Position.create(1, 1);
    const inlineCompletionParams: InlineCompletionParams = {
      ...COMPLETION_PARAMS,
      context: {
        triggerKind: CompletionItemKind.Text,
      },
      position,
    };
    const uniqueTrackingId = 'testTrackingId';
    const text = 'Some suggestion text';
    const choice: CodeSuggestionResponseChoice = {
      text,
      uniqueTrackingId,
    };

    const mapperFn = inlineCompletionChoiceMapper(inlineCompletionParams);
    const inlineCompletionItem = mapperFn(choice);

    expect(inlineCompletionItem).toEqual(
      expect.objectContaining({
        insertText: 'Some suggestion text',
        range: Range.create(position, position),
        command: {
          title: 'Accept suggestion',
          command: SUGGESTION_ACCEPTED_COMMAND,
          arguments: [uniqueTrackingId],
        },
      }),
    );
  });
});
