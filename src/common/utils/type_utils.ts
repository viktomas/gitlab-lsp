import { CompletionContext, InlineCompletionContext } from 'vscode-languageserver-protocol';

export type OptionalCompletionContext = InlineCompletionContext | CompletionContext | undefined;

export function isInlineCompletionContext(
  context: OptionalCompletionContext,
): context is InlineCompletionContext {
  return context !== undefined && 'selectedCompletionInfo' in context;
}
