import { InitializeParams, WorkspaceFolder } from 'vscode-languageserver';
import { DEFAULT_TRACKING_ENDPOINT, ITelemetryOptions } from './tracking/snowplow_tracker';
import { GITLAB_API_BASE_URL } from './api';
import { LogLevel, LOG_LEVEL } from './log';

export type IClientInfo = InitializeParams['clientInfo'];

export interface ICodeCompletionConfig {
  enableSecretRedaction?: boolean;
}

export interface ISuggestionsCacheOptions {
  enabled?: boolean;
  maxSize?: number;
  ttl?: number;
  prefixLines?: number;
  suffixLines?: number;
}

export interface IHttpAgentOptions {
  ca?: string;
  cert?: string;
  certKey?: string;
}

export interface IConfig {
  /** GitLab API URL used for getting code suggestions */
  baseUrl?: string;
  /** Full project path. */
  projectPath?: string;
  /** PAT or OAuth token used to authenticate to GitLab API */
  token?: string;
  /** The base URL for language server assets in the client-side extension */
  baseAssetsUrl?: string;
  clientInfo?: IClientInfo;
  codeCompletion?: ICodeCompletionConfig;
  telemetry?: ITelemetryOptions;
  /** Config used for caching code suggestions */
  suggestionsCache?: ISuggestionsCacheOptions;
  workspaceFolders?: WorkspaceFolder[] | null;
  /** Collection of Feature Flag values which are sent from the client */
  featureFlags?: Record<string, boolean>;
  logLevel?: LogLevel;
  ignoreCertificateErrors?: boolean;
  httpAgentOptions?: IHttpAgentOptions;
}
/**
 * Config provider manages user configuration (e.g. baseUrl) and application state (e.g. codeCompletion.enabled)
 * TODO: Maybe in the future we would like to separate these two
 */
export class ConfigProvider {
  #config: IConfig;

  constructor() {
    this.#config = {
      baseUrl: GITLAB_API_BASE_URL,
      codeCompletion: {
        enableSecretRedaction: true,
      },
      telemetry: {
        enabled: true,
        trackingUrl: DEFAULT_TRACKING_ENDPOINT,
      },
      logLevel: LOG_LEVEL.INFO,
      ignoreCertificateErrors: false,
      httpAgentOptions: {},
    };
  }

  get(): IConfig {
    return this.#config;
  }
  /**
   * set sets the **top-level** property of the config
   * the new value completely overrides the old one
   */
  set<K extends keyof IConfig>(key: K, value: IConfig[K]) {
    this.#config[key] = value;
  }

  /**
   * merge adds `newConfig` properties into existing config, if the
   * property is present in both old and new config, `newConfig`
   * properties take precedence.
   *
   * This method replaces whole top-level properties. **It doesn't
   * perform deep merge.**
   *
   */
  merge(newConfig: Partial<IConfig>) {
    this.#config = { ...this.#config, ...newConfig };
  }
}
