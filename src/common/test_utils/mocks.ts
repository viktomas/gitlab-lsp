import { CodeSuggestionResponse } from '../api';
import { IDocContext } from '..';
import { TextDocumentIdentifier, TextDocumentPositionParams } from 'vscode-languageserver';
import { Position } from 'vscode-languageserver-textdocument';
import { CustomInitializeParams } from '../message_handler';

export const FILE_INFO: IDocContext = {
  filename: 'example.ts',
  prefix: 'const x = 10;',
  suffix: 'console.log(x);',
  position: {
    line: 0,
    character: 13,
  },
};

export const CODE_SUGGESTIONS_RESPONSE: CodeSuggestionResponse = {
  choices: [
    { text: 'choice1', uniqueTrackingId: 'ut1' },
    { text: 'choice2', uniqueTrackingId: 'ut2' },
  ],
  status: 200,
};

export const INITIALIZE_PARAMS: CustomInitializeParams = {
  clientInfo: {
    name: 'Visual Studio Code',
    version: '1.82.0',
  },
  capabilities: { textDocument: { completion: {}, inlineCompletion: {} } },
  rootUri: '/',
  initializationOptions: {},
  processId: 1,
};

export const EMPTY_COMPLETION_CONTEXT: IDocContext = {
  prefix: '',
  suffix: '',
  filename: 'test.js',
  position: {
    line: 0,
    character: 0,
  },
};

export const SHORT_COMPLETION_CONTEXT: IDocContext = {
  prefix: 'abc',
  suffix: 'def',
  filename: 'test.js',
  position: {
    line: 0,
    character: 3,
  },
};

export const LONG_COMPLETION_CONTEXT: IDocContext = {
  prefix: 'abc 123',
  suffix: 'def 456',
  filename: 'test.js',
  position: {
    line: 0,
    character: 7,
  },
};

const textDocument: TextDocumentIdentifier = { uri: '/' };
const position: Position = { line: 0, character: 0 };

export const COMPLETION_PARAMS: TextDocumentPositionParams = { textDocument, position };
