import { v4 as uuidv4 } from 'uuid';
import Ajv from 'ajv-draft-04';
import { SelfDescribingJson, StructuredEvent } from '@snowplow/tracker-core';
import { Snowplow } from './snowplow/snowplow';
import { IClientInfo } from '../config';
import { IFetch } from '../fetch';
import { IDocContext } from '../documents';
import * as CodeSuggestionContextSchema from './schemas/code_suggestion_context-2-6-0.json';
import * as IdeExtensionContextSchema from './schemas/ide_extension_version-1-1-0.json';
import * as SnowplowMetaSchema from './schemas/snowplow_schema-1-0-0.json';
import { version as lsVersion } from './ls_info.json';
import { log } from '../log';

/*
Custom notifications need to be sent from Client to Server to notify about the event:
suggestion_accepted, suggestion_rejected, suggestion_cancelled

The server knows when event happens:
suggestion_requested, suggestion_loaded, suggestion_not_provided, suggestion_error, suggestion_shown

---------------------------------------------------------------------------------
***************Suggestion tracking*******************************************
suggestion_requested: Sent when the IDE extension requests a suggestion
suggestion_loaded: Sent when the suggestion request came back
suggestion_not_provided: Sent when the suggestion request came back empty
suggestion_error: Sent when the suggestion request leads to an error
suggestion_shown: Sent when the suggestion is started to be shown to the user
suggestion_accepted: Sent when the suggestion was accepted
suggestion_rejected: Sent when the suggestion was rejected
suggestion_cancelled: Sent when the suggestion request was canceled
stream_started: Tracked for generations only when the first chunk of stream was received.
stream_completed: Tracked for generations only when the stream has completed.
---------------------------------------------------------------------------------
 */
export enum TRACKING_EVENTS {
  REQUESTED = 'suggestion_requested',
  LOADED = 'suggestion_loaded',
  NOT_PROVIDED = 'suggestion_not_provided',
  SHOWN = 'suggestion_shown',
  ERRORED = 'suggestion_error',
  ACCEPTED = 'suggestion_accepted',
  REJECTED = 'suggestion_rejected',
  CANCELLED = 'suggestion_cancelled',
  STREAM_STARTED = 'suggestion_stream_started',
  STREAM_COMPLETED = 'suggestion_stream_completed',
}

const END_STATE_NO_TRANSITIONS_ALLOWED: TRACKING_EVENTS[] = [];

const endStatesGraph = new Map<TRACKING_EVENTS, TRACKING_EVENTS[]>([
  [TRACKING_EVENTS.ERRORED, END_STATE_NO_TRANSITIONS_ALLOWED],
  [TRACKING_EVENTS.CANCELLED, END_STATE_NO_TRANSITIONS_ALLOWED],
  [TRACKING_EVENTS.NOT_PROVIDED, END_STATE_NO_TRANSITIONS_ALLOWED],
  [TRACKING_EVENTS.REJECTED, END_STATE_NO_TRANSITIONS_ALLOWED],
  [TRACKING_EVENTS.ACCEPTED, END_STATE_NO_TRANSITIONS_ALLOWED],
]);

const nonStreamingSuggestionStateGraph = new Map<TRACKING_EVENTS, TRACKING_EVENTS[]>([
  [TRACKING_EVENTS.REQUESTED, [TRACKING_EVENTS.LOADED, TRACKING_EVENTS.ERRORED]],
  [
    TRACKING_EVENTS.LOADED,
    [TRACKING_EVENTS.SHOWN, TRACKING_EVENTS.CANCELLED, TRACKING_EVENTS.NOT_PROVIDED],
  ],
  [TRACKING_EVENTS.SHOWN, [TRACKING_EVENTS.ACCEPTED, TRACKING_EVENTS.REJECTED]],
  ...endStatesGraph,
]);

export const streamingSuggestionStateGraph = new Map<TRACKING_EVENTS, TRACKING_EVENTS[]>([
  [
    TRACKING_EVENTS.REQUESTED,
    [TRACKING_EVENTS.STREAM_STARTED, TRACKING_EVENTS.ERRORED, TRACKING_EVENTS.CANCELLED],
  ],
  [
    TRACKING_EVENTS.STREAM_STARTED,
    [TRACKING_EVENTS.SHOWN, TRACKING_EVENTS.ERRORED, TRACKING_EVENTS.STREAM_COMPLETED],
  ],
  [
    TRACKING_EVENTS.SHOWN,
    [
      TRACKING_EVENTS.ERRORED,
      TRACKING_EVENTS.STREAM_COMPLETED,
      TRACKING_EVENTS.ACCEPTED,
      TRACKING_EVENTS.REJECTED,
    ],
  ],
  [
    TRACKING_EVENTS.STREAM_COMPLETED,
    [TRACKING_EVENTS.ACCEPTED, TRACKING_EVENTS.REJECTED, TRACKING_EVENTS.NOT_PROVIDED],
  ],
  ...endStatesGraph,
]);

const endStates = [...endStatesGraph].map(([state]) => state);

const SAAS_INSTANCE_URL = 'https://gitlab.com';
export const TELEMETRY_NOTIFICATION = '$/gitlab/telemetry';
export const CODE_SUGGESTIONS_CATEGORY = 'code_suggestions';

const GC_TIME = 60000;

export interface ITelemetryOptions {
  enabled?: boolean;
  baseUrl?: string;
  trackingUrl?: string;
  actions?: Array<{ action: TRACKING_EVENTS }>;
}

export interface IIDEInfo {
  name: string;
  version: string;
  vendor: string;
}

export interface IClientContext {
  ide?: IIDEInfo;
  extension?: IClientInfo;
}

export enum GitlabRealm {
  saas = 'saas',
  selfManaged = 'self-managed',
}

export interface ISnowplowCodeSuggestionContext {
  schema: string;
  data: {
    prefix_length?: number;
    suffix_length?: number;
    language?: string | null;
    gitlab_realm?: GitlabRealm;
    model_engine?: string | null;
    model_name?: string | null;
    api_status_code?: number | null;
    suggestion_source?: 'cache' | 'network';
    gitlab_instance_id?: string | null;
    gitlab_saas_duo_pro_namespace_ids: number[] | null;
    is_streaming?: boolean;
  };
}

export interface ICodeSuggestionModel {
  lang: string;
  engine: string;
  name: string;
}

interface ISnowplowClientContext {
  schema: string;
  data: {
    ide_name?: string | null;
    ide_vendor?: string | null;
    ide_version?: string | null;
    extension_name?: string | null;
    extension_version?: string | null;
    language_server_version?: string | null;
  };
}

interface ISnowplowCodeSuggestionContextUpdate {
  model: ICodeSuggestionModel;
  status: number;
}

export const DEFAULT_TRACKING_ENDPOINT = 'https://snowplow.trx.gitlab.net';

const DEFAULT_SNOWPLOW_OPTIONS = {
  appId: 'gitlab_ide_extension',
  timeInterval: 5000,
  maxItems: 10,
};

export const EVENT_VALIDATION_ERROR_MSG = `Telemetry event context is not valid  - event won't be tracked.`;
export const TELEMETRY_DISABLED_WARNING_MSG =
  'GitLab Duo Code Suggestions telemetry is disabled. Please consider enabling telemetry to help improve our service.';

export class SnowplowTracker {
  private snowplow: Snowplow;
  #ajv = new Ajv({ strict: false });
  private options: ITelemetryOptions = {
    enabled: true,
    baseUrl: SAAS_INSTANCE_URL,
    trackingUrl: DEFAULT_TRACKING_ENDPOINT,
    // the list of events that the client can track themselves
    actions: [],
  };
  private clientContext: ISnowplowClientContext = {
    schema: 'iglu:com.gitlab/ide_extension_version/jsonschema/1-1-0',
    data: {},
  };
  private lsFetch: IFetch;

  private gitlabRealm: GitlabRealm = GitlabRealm.saas;
  private codeSuggestionsContextMap = new Map<string, ISnowplowCodeSuggestionContext>();
  #codeSuggestionStates = new Map<string, TRACKING_EVENTS>();

  constructor(lsFetch: IFetch, { trackingUrl = DEFAULT_TRACKING_ENDPOINT } = {}) {
    this.lsFetch = lsFetch;
    this.snowplow = Snowplow.getInstance(this.lsFetch, {
      ...DEFAULT_SNOWPLOW_OPTIONS,
      endpoint: trackingUrl,
      enabled: this.isEnabled.bind(this),
    });

    this.options.trackingUrl = trackingUrl;
    this.#ajv.addMetaSchema(SnowplowMetaSchema);
  }

  public async reconfigure({ baseUrl, enabled, trackingUrl, actions }: ITelemetryOptions) {
    if (typeof enabled !== 'undefined') {
      this.options.enabled = enabled;

      if (this.options.enabled === false) {
        log.warn(TELEMETRY_DISABLED_WARNING_MSG);
      }
    }

    if (baseUrl) {
      this.options.baseUrl = baseUrl;
      this.gitlabRealm = baseUrl.endsWith(SAAS_INSTANCE_URL)
        ? GitlabRealm.saas
        : GitlabRealm.selfManaged;
    }

    if (trackingUrl && this.options.trackingUrl !== trackingUrl) {
      await this.snowplow.stop();
      this.snowplow.destroy();
      this.snowplow = Snowplow.getInstance(this.lsFetch, {
        ...DEFAULT_SNOWPLOW_OPTIONS,
        endpoint: trackingUrl,
        enabled: this.isEnabled.bind(this),
      });
    }

    if (actions) {
      this.options.actions = actions;
    }
  }

  public isEnabled(): boolean {
    return Boolean(this.options.enabled);
  }

  public setClientContext(context: IClientContext) {
    this.clientContext.data = {
      ide_name: context?.ide?.name ?? null,
      ide_vendor: context?.ide?.vendor ?? null,
      ide_version: context?.ide?.version ?? null,
      extension_name: context?.extension?.name ?? null,
      extension_version: context?.extension?.version ?? null,
      language_server_version: lsVersion ?? null,
    };
  }

  public setCodeSuggestionsContext(
    uniqueTrackingId: string,
    suggestion: IDocContext | undefined,
    suggestionSource: 'cache' | 'network' = 'network',
    isStreaming: boolean = false,
  ) {
    if (suggestionSource === 'cache') {
      log.debug(`Telemetry: Retrieved suggestion from cache`);
    } else {
      log.debug(`Telemetry: Received request to create a new suggestion`);
    }

    // Only auto-reject if client is set up to track accepted and not rejected events.
    if (
      this.canClientTrackEvent(TRACKING_EVENTS.ACCEPTED) &&
      !this.canClientTrackEvent(TRACKING_EVENTS.REJECTED)
    ) {
      this.rejectOpenedSuggestions();
    }

    setTimeout(() => {
      if (this.codeSuggestionsContextMap.has(uniqueTrackingId)) {
        this.codeSuggestionsContextMap.delete(uniqueTrackingId);
        this.#codeSuggestionStates.delete(uniqueTrackingId);
      }
    }, GC_TIME);

    this.codeSuggestionsContextMap.set(uniqueTrackingId, {
      schema: 'iglu:com.gitlab/code_suggestions_context/jsonschema/2-6-0',
      data: {
        suffix_length: suggestion?.suffix.length ?? 0,
        prefix_length: suggestion?.prefix.length ?? 0,
        gitlab_realm: this.gitlabRealm,
        model_engine: null,
        model_name: null,
        language: null,
        api_status_code: null,
        suggestion_source: suggestionSource,
        gitlab_instance_id: null,
        gitlab_saas_duo_pro_namespace_ids: null,
        is_streaming: isStreaming,
      },
    });
    this.#codeSuggestionStates.set(uniqueTrackingId, TRACKING_EVENTS.REQUESTED);

    this.trackCodeSuggestionsEvent(TRACKING_EVENTS.REQUESTED, uniqueTrackingId).catch((e) =>
      log.warn('could not track telemetry', e),
    );

    log.debug(`Telemetry: New suggestion ${uniqueTrackingId} has been requested`);
  }

  public updateCodeSuggestionsContext(
    uniqueTrackingId: string,
    contextUpdate: Partial<ISnowplowCodeSuggestionContextUpdate>,
  ) {
    const context = this.codeSuggestionsContextMap.get(uniqueTrackingId);
    const { model, status } = contextUpdate;

    if (context) {
      if (model) {
        context.data.language = model.lang ?? null;
        context.data.model_engine = model.engine ?? null;
        context.data.model_name = model.name ?? null;
      }

      if (status) {
        context.data.api_status_code = status;
      }

      this.codeSuggestionsContextMap.set(uniqueTrackingId, context);
    }
  }

  public canClientTrackEvent(event: TRACKING_EVENTS) {
    return this.options.actions?.some(({ action }) => action === event);
  }

  private async trackCodeSuggestionsEvent(eventType: TRACKING_EVENTS, uniqueTrackingId: string) {
    if (!this.isEnabled()) {
      return;
    }

    const event: StructuredEvent = {
      category: CODE_SUGGESTIONS_CATEGORY,
      action: eventType,
      label: uniqueTrackingId,
    };

    try {
      const contexts: SelfDescribingJson[] = [this.clientContext];
      const codeSuggestionContext = this.codeSuggestionsContextMap.get(uniqueTrackingId);

      if (codeSuggestionContext) {
        contexts.push(codeSuggestionContext);
      }

      const suggestionContextValid = this.#ajv.validate(
        CodeSuggestionContextSchema,
        codeSuggestionContext?.data,
      );
      if (!suggestionContextValid) {
        log.warn(EVENT_VALIDATION_ERROR_MSG);
        log.debug(JSON.stringify(this.#ajv.errors, null, 2));
        return;
      }
      const ideExtensionContextValid = this.#ajv.validate(
        IdeExtensionContextSchema,
        this.clientContext?.data,
      );
      if (!ideExtensionContextValid) {
        log.warn(EVENT_VALIDATION_ERROR_MSG);
        log.debug(JSON.stringify(this.#ajv.errors, null, 2));
        return;
      }

      await this.snowplow.trackStructEvent(event, contexts);
    } catch (error) {
      log.warn(`Failed to track telemetry event: ${eventType}`, error);
    }
  }

  public rejectOpenedSuggestions() {
    log.debug(`Telemetry: Reject all opened suggestions`);

    this.#codeSuggestionStates.forEach((state, uniqueTrackingId) => {
      if (endStates.includes(state)) {
        return;
      }

      this.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.REJECTED);
    });
  }

  public updateSuggestionState(uniqueTrackingId: string, newState: TRACKING_EVENTS): void {
    const state = this.#codeSuggestionStates.get(uniqueTrackingId);
    if (!state) {
      log.debug(`Telemetry: The suggestion with ${uniqueTrackingId} can't be found`);
      return;
    }

    const isStreaming = Boolean(
      this.codeSuggestionsContextMap.get(uniqueTrackingId)?.data.is_streaming,
    );

    const allowedTransitions = isStreaming
      ? streamingSuggestionStateGraph.get(state)
      : nonStreamingSuggestionStateGraph.get(state);

    if (!allowedTransitions) {
      log.debug(
        `Telemetry: The suggestion's ${uniqueTrackingId} state ${state} can't be found in state graph`,
      );
      return;
    }

    if (!allowedTransitions.includes(newState)) {
      log.debug(
        `Telemetry: Unexpected transition from ${state} into ${newState} for ${uniqueTrackingId}`,
      );
      // Allow state to update to ACCEPTED despite 'allowed transitions' constraint.
      if (newState !== TRACKING_EVENTS.ACCEPTED) {
        return;
      }

      log.debug(
        `Telemetry: Conditionally allowing transition to accepted state for ${uniqueTrackingId}`,
      );
    }
    this.#codeSuggestionStates.set(uniqueTrackingId, newState);

    this.trackCodeSuggestionsEvent(newState, uniqueTrackingId).catch((e) =>
      log.warn('could not track telemetry', e),
    );

    log.debug(`Telemetry: ${uniqueTrackingId} transisted from ${state} to ${newState}`);
  }

  static uniqueTrackingId(): string {
    return uuidv4();
  }
}
