import {
  IClientContext,
  SnowplowTracker,
  TRACKING_EVENTS,
  GitlabRealm,
  ICodeSuggestionModel,
  DEFAULT_TRACKING_ENDPOINT,
} from './snowplow_tracker';
import { IDocContext } from '..';
import { Snowplow } from './snowplow/snowplow';
import { FetchBase, IFetch } from '../fetch';

const mockTrackStructEvent = jest.fn();
jest.mock('./snowplow/snowplow', () => {
  const mockGetInstance = jest.fn().mockImplementation(() => ({
    trackStructEvent: mockTrackStructEvent,
    stop: jest.fn(),
    destroy: jest.fn(),
  }));
  return {
    Snowplow: {
      getInstance: mockGetInstance,
    },
  };
});

jest.useFakeTimers();
jest.mock('./ls_info.json', () => ({ version: '1-0-0' }));

const mockSchemaValidateFn = jest.fn();
jest.mock('ajv-draft-04', () => {
  return {
    __esModule: true,
    default: jest.fn().mockImplementation(() => {
      return {
        validate: mockSchemaValidateFn,
        addMetaSchema: jest.fn(),
      };
    }),
  };
});
const mockSuggestion: IDocContext = {
  prefix: 'beforeCursor',
  suffix: 'afterCursor',
  filename: 'test.ts',
  position: {
    line: 0,
    character: 12,
  },
};

describe('SnowplowTracker', () => {
  let snowplowTracker: SnowplowTracker;
  let lsFetch: IFetch;

  beforeEach(() => {
    lsFetch = new FetchBase();
    snowplowTracker = new SnowplowTracker(lsFetch);
    mockSchemaValidateFn.mockReturnValue({ valid: true });
  });

  afterEach(() => {
    jest.clearAllMocks();
    jest.runAllTimers();
  });

  describe('Reconfigure', () => {
    it('telemetry should be enabled by default', () => {
      expect(snowplowTracker.isEnabled()).toBe(true);
    });

    it('should be able to toggle telemetry', async () => {
      await snowplowTracker.reconfigure({ enabled: false });
      expect(snowplowTracker.isEnabled()).toBe(false);

      snowplowTracker.setCodeSuggestionsContext('1', mockSuggestion);
      expect(mockTrackStructEvent).not.toHaveBeenCalled();
      await snowplowTracker.reconfigure({ enabled: true });
      expect(snowplowTracker.isEnabled()).toBe(true);
      snowplowTracker.setCodeSuggestionsContext('1', mockSuggestion);
      expect(mockTrackStructEvent).toHaveBeenCalled();
    });

    it('should update `baseUrl` if provided but not the `enabled`', async () => {
      expect(snowplowTracker.isEnabled()).toBe(true);
      await snowplowTracker.reconfigure({ baseUrl: 'http://test.com' });
      expect(snowplowTracker.isEnabled()).toBe(true);
    });

    it('should recreate the Snowplow instance when a new trackingUrl is provided', async () => {
      expect(Snowplow.getInstance).toHaveBeenCalledWith(
        expect.anything(),
        expect.objectContaining({
          endpoint: DEFAULT_TRACKING_ENDPOINT,
        }),
      );
      await snowplowTracker.reconfigure({ trackingUrl: 'http://new.tracking.com' });

      expect(Snowplow.getInstance).toHaveBeenLastCalledWith(
        expect.anything(),
        expect.objectContaining({
          endpoint: 'http://new.tracking.com',
        }),
      );
    });
  });

  describe('Events', () => {
    let uniqueTrackingId: string;

    beforeEach(() => {
      uniqueTrackingId = SnowplowTracker.uniqueTrackingId();
    });

    const setRequestedState = () => {
      uniqueTrackingId = SnowplowTracker.uniqueTrackingId();
      snowplowTracker.setCodeSuggestionsContext(uniqueTrackingId, mockSuggestion);
    };

    const setLoadedState = () => {
      setRequestedState();
      snowplowTracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.LOADED);
    };

    const setShownState = () => {
      setLoadedState();
      snowplowTracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.SHOWN);
    };

    const setErroredState = () => {
      setRequestedState();
      snowplowTracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.ERRORED);
    };

    const setCancelledState = () => {
      setLoadedState();
      snowplowTracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.CANCELLED);
    };

    const setNotProvidedState = () => {
      setLoadedState();
      snowplowTracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.NOT_PROVIDED);
    };

    const setRejectedState = () => {
      setShownState();
      snowplowTracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.REJECTED);
    };

    const setAcceptedState = () => {
      setShownState();
      snowplowTracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.ACCEPTED);
    };

    const stateFactories: Array<[TRACKING_EVENTS, () => void]> = [
      [TRACKING_EVENTS.REQUESTED, setRequestedState],
      [TRACKING_EVENTS.LOADED, setLoadedState],
      [TRACKING_EVENTS.SHOWN, setShownState],
      [TRACKING_EVENTS.ERRORED, setErroredState],
      [TRACKING_EVENTS.CANCELLED, setCancelledState],
      [TRACKING_EVENTS.NOT_PROVIDED, setNotProvidedState],
      [TRACKING_EVENTS.REJECTED, setRejectedState],
      [TRACKING_EVENTS.ACCEPTED, setAcceptedState],
    ];

    it.each(stateFactories)('should track the %s event', (eventType, stateFactory) => {
      stateFactory();

      snowplowTracker.updateSuggestionState(uniqueTrackingId, eventType);
      expect(mockTrackStructEvent).toHaveBeenCalledWith(
        expect.objectContaining({ action: eventType }),
        expect.arrayContaining([
          expect.objectContaining({ schema: expect.any(String), data: expect.any(Object) }),
        ]),
      );
    });
  });

  describe('Tracking contexts', () => {
    describe('Client context', () => {
      it('should track event with ide and extension data', () => {
        const clientContext: IClientContext = {
          ide: { name: 'IDE', version: '1.0', vendor: 'Vendor' },
          extension: { name: 'Updated Extension', version: '2.0' },
        };
        const uniqueTrackingId = 'id';
        snowplowTracker.setCodeSuggestionsContext(uniqueTrackingId, mockSuggestion);
        snowplowTracker.setClientContext(clientContext);
        snowplowTracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.LOADED);

        expect(mockTrackStructEvent.mock.calls[1]).toEqual([
          expect.objectContaining({ action: TRACKING_EVENTS.LOADED }),
          expect.arrayContaining([
            expect.objectContaining({
              schema: 'iglu:com.gitlab/ide_extension_version/jsonschema/1-1-0',
              data: {
                ide_name: 'IDE',
                ide_version: '1.0',
                ide_vendor: 'Vendor',
                extension_name: 'Updated Extension',
                extension_version: '2.0',
                language_server_version: '1-0-0',
              },
            }),
          ]),
        ]);
      });
    });

    describe('Code suggestions context', () => {
      it('should track event with the code suggestion data', () => {
        const uniqueTrackingId = SnowplowTracker.uniqueTrackingId();
        snowplowTracker.setCodeSuggestionsContext(uniqueTrackingId, mockSuggestion);

        expect(mockTrackStructEvent).toHaveBeenCalledWith(
          expect.objectContaining({ action: TRACKING_EVENTS.REQUESTED }),
          [
            expect.any(Object),
            {
              schema: 'iglu:com.gitlab/code_suggestions_context/jsonschema/2-6-0',
              data: {
                suffix_length: 11,
                prefix_length: 12,
                gitlab_realm: GitlabRealm.saas,
                language: null,
                model_name: null,
                model_engine: null,
                api_status_code: null,
                suggestion_source: 'network',
                gitlab_instance_id: null,
                gitlab_saas_duo_pro_namespace_ids: null,
                is_streaming: false,
              },
            },
          ],
        );
      });

      it('should track event with proper suggestion source', () => {
        const uniqueTrackingId = SnowplowTracker.uniqueTrackingId();
        const expectedSuggestedSource = 'cache';
        snowplowTracker.setCodeSuggestionsContext(
          uniqueTrackingId,
          mockSuggestion,
          expectedSuggestedSource,
        );

        expect(mockTrackStructEvent).toHaveBeenCalledWith(
          expect.objectContaining({ action: TRACKING_EVENTS.REQUESTED }),
          [
            expect.any(Object),
            {
              schema: 'iglu:com.gitlab/code_suggestions_context/jsonschema/2-6-0',
              data: {
                suffix_length: 11,
                prefix_length: 12,
                gitlab_realm: GitlabRealm.saas,
                language: null,
                model_name: null,
                model_engine: null,
                api_status_code: null,
                suggestion_source: expectedSuggestedSource,
                gitlab_instance_id: null,
                gitlab_saas_duo_pro_namespace_ids: null,
                is_streaming: false,
              },
            },
          ],
        );
      });

      it('should update the Code Suggestion context and track further events with it', () => {
        const uniqueTrackingId = SnowplowTracker.uniqueTrackingId();

        snowplowTracker.setCodeSuggestionsContext(uniqueTrackingId, mockSuggestion);

        expect(mockTrackStructEvent).toHaveBeenCalledWith(
          expect.objectContaining({ action: TRACKING_EVENTS.REQUESTED }),
          [
            expect.any(Object),
            {
              schema: 'iglu:com.gitlab/code_suggestions_context/jsonschema/2-6-0',
              data: {
                suffix_length: 11,
                prefix_length: 12,
                gitlab_realm: GitlabRealm.saas,
                language: null,
                model_name: null,
                model_engine: null,
                api_status_code: null,
                suggestion_source: 'network',
                gitlab_instance_id: null,
                gitlab_saas_duo_pro_namespace_ids: null,
                is_streaming: false,
              },
            },
          ],
        );
        const model: ICodeSuggestionModel = {
          engine: 'vertex-ai',
          name: 'code-gecko@latest',
          lang: 'js',
        };

        const apiStatusCode = 200;

        snowplowTracker.updateCodeSuggestionsContext(uniqueTrackingId, {
          model,
          status: apiStatusCode,
        });

        snowplowTracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.LOADED);
        snowplowTracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.SHOWN);

        expect(mockTrackStructEvent.mock.calls.length).toEqual(3);
        expect(mockTrackStructEvent.mock.calls[2]).toEqual([
          expect.objectContaining({ action: TRACKING_EVENTS.SHOWN }),
          [
            expect.any(Object),
            {
              schema: 'iglu:com.gitlab/code_suggestions_context/jsonschema/2-6-0',
              data: {
                suffix_length: 11,
                prefix_length: 12,
                gitlab_realm: GitlabRealm.saas,
                language: model.lang,
                model_engine: model.engine,
                model_name: model.name,
                api_status_code: apiStatusCode,
                suggestion_source: 'network',
                gitlab_instance_id: null,
                gitlab_saas_duo_pro_namespace_ids: null,
                is_streaming: false,
              },
            },
          ],
        ]);
      });

      it('should update the Code Suggestion context and track further events with it even if the request failed', () => {
        const uniqueTrackingId = SnowplowTracker.uniqueTrackingId();

        snowplowTracker.setCodeSuggestionsContext(uniqueTrackingId, mockSuggestion);
        const model = undefined;
        const apiStatusCode = 400;
        snowplowTracker.updateCodeSuggestionsContext(uniqueTrackingId, {
          model,
          status: apiStatusCode,
        });

        snowplowTracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.ERRORED);

        expect(mockTrackStructEvent.mock.calls.length).toEqual(2);
        expect(mockTrackStructEvent.mock.calls[1]).toEqual([
          expect.objectContaining({ action: TRACKING_EVENTS.ERRORED }),
          [
            expect.any(Object),
            {
              schema: 'iglu:com.gitlab/code_suggestions_context/jsonschema/2-6-0',
              data: {
                suffix_length: 11,
                prefix_length: 12,
                gitlab_realm: GitlabRealm.saas,
                language: null,
                model_name: null,
                model_engine: null,
                api_status_code: apiStatusCode,
                suggestion_source: 'network',
                gitlab_instance_id: null,
                gitlab_saas_duo_pro_namespace_ids: null,
                is_streaming: false,
              },
            },
          ],
        ]);
      });
    });
  });

  describe('canClientTrackEvent', () => {
    beforeEach(async () => {
      await snowplowTracker.reconfigure({
        actions: [{ action: TRACKING_EVENTS.SHOWN }],
      });
    });

    it('should return `true` if Client notified that it can track the event on its side', () => {
      expect(snowplowTracker.canClientTrackEvent(TRACKING_EVENTS.SHOWN)).toBe(true);
    });

    it('should return `false` otherwise', () => {
      expect(snowplowTracker.canClientTrackEvent(TRACKING_EVENTS.NOT_PROVIDED)).toBe(false);
    });
  });

  describe('Schema validation', () => {
    it(`should track events when event's context validated against the schema`, () => {
      mockSchemaValidateFn.mockReturnValue(true);

      const uniqueTrackingId = SnowplowTracker.uniqueTrackingId();
      snowplowTracker.setCodeSuggestionsContext(uniqueTrackingId, mockSuggestion);
      snowplowTracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.LOADED);
      expect(mockTrackStructEvent).toHaveBeenCalledTimes(2);
    });

    it(`should NOT track events when event's context failed to validate against the schema`, () => {
      mockSchemaValidateFn.mockReturnValue(false);

      const uniqueTrackingId = SnowplowTracker.uniqueTrackingId();
      snowplowTracker.setCodeSuggestionsContext(uniqueTrackingId, mockSuggestion);
      snowplowTracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.LOADED);
      expect(mockTrackStructEvent).not.toHaveBeenCalled();
    });
  });

  describe('State management', () => {
    let uniqueTrackingId: string;

    beforeEach(() => {
      uniqueTrackingId = SnowplowTracker.uniqueTrackingId();
    });

    it('should send a state of requested when the suggestion is created', () => {
      snowplowTracker.setCodeSuggestionsContext(uniqueTrackingId, mockSuggestion);

      expect(mockTrackStructEvent).toHaveBeenCalledWith(
        expect.objectContaining({
          action: TRACKING_EVENTS.REQUESTED,
          category: 'code_suggestions',
        }),
        expect.any(Array),
      );
    });

    it('should update state of an existing suggestion when transition is valid', () => {
      snowplowTracker.setCodeSuggestionsContext(uniqueTrackingId, mockSuggestion);
      mockTrackStructEvent.mockClear();
      snowplowTracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.LOADED);

      expect(mockTrackStructEvent).toBeCalledWith(
        expect.objectContaining({ action: TRACKING_EVENTS.LOADED, category: 'code_suggestions' }),
        expect.any(Array),
      );
    });

    it('should remove suggestions after 60s', () => {
      snowplowTracker.setCodeSuggestionsContext(uniqueTrackingId, mockSuggestion);
      mockTrackStructEvent.mockClear();
      jest.advanceTimersByTime(60000);

      snowplowTracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.LOADED);

      expect(mockTrackStructEvent).toBeCalledTimes(0);
    });

    it('should not update state of an existing suggestion when transition is invalid', () => {
      // Sets suggestion state to `suggestion_requested`.
      snowplowTracker.setCodeSuggestionsContext(uniqueTrackingId, mockSuggestion);
      mockTrackStructEvent.mockClear();
      // Invalid state transition: `suggestion_requested` to `suggestion_rejected`.
      snowplowTracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.REJECTED);

      expect(mockTrackStructEvent).toBeCalledTimes(0);
    });

    it('should perform invalid transition when new state is "suggestion_accepted"', () => {
      snowplowTracker.setCodeSuggestionsContext(uniqueTrackingId, mockSuggestion);
      mockTrackStructEvent.mockClear();
      snowplowTracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.ACCEPTED);

      expect(mockTrackStructEvent).toBeCalledTimes(1);
      expect(mockTrackStructEvent).toBeCalledWith(
        expect.objectContaining({ action: TRACKING_EVENTS.ACCEPTED, category: 'code_suggestions' }),
        expect.any(Array),
      );
    });

    it('should not update state if suggestion does not exist', () => {
      const nonExistentID = 'unknown';

      snowplowTracker.updateSuggestionState(nonExistentID, TRACKING_EVENTS.LOADED);

      expect(mockTrackStructEvent).not.toBeCalled();
    });

    describe('Rejection', () => {
      beforeEach(() => {
        snowplowTracker.setCodeSuggestionsContext(uniqueTrackingId, mockSuggestion);
        snowplowTracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.LOADED);
        snowplowTracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.SHOWN);
        mockTrackStructEvent.mockClear();
      });

      it('should reject all open suggestions', async () => {
        await snowplowTracker.reconfigure({ actions: [{ action: TRACKING_EVENTS.ACCEPTED }] });
        snowplowTracker.setCodeSuggestionsContext(
          SnowplowTracker.uniqueTrackingId(),
          mockSuggestion,
        );

        expect(mockTrackStructEvent.mock.calls.length).toEqual(2);
        expect(mockTrackStructEvent.mock.calls[0]).toEqual([
          expect.objectContaining({
            action: TRACKING_EVENTS.REJECTED,
            category: 'code_suggestions',
          }),
          expect.any(Array),
        ]);
        expect(mockTrackStructEvent.mock.calls[1]).toEqual([
          expect.objectContaining({
            action: TRACKING_EVENTS.REQUESTED,
            category: 'code_suggestions',
          }),
          expect.any(Array),
        ]);
      });

      it('should not auto-reject suggestions when client registers that it sends rejections', async () => {
        await snowplowTracker.reconfigure({ actions: [{ action: TRACKING_EVENTS.REJECTED }] });

        snowplowTracker.setCodeSuggestionsContext(
          SnowplowTracker.uniqueTrackingId(),
          mockSuggestion,
        );

        expect(mockTrackStructEvent.mock.calls.length).toEqual(1);
        expect(mockTrackStructEvent).toHaveBeenCalledWith(
          expect.objectContaining({
            action: TRACKING_EVENTS.REQUESTED,
            category: 'code_suggestions',
          }),
          expect.any(Array),
        );
      });

      it('should not auto-reject suggestions when client does not send accepted events', async () => {
        await snowplowTracker.reconfigure({ actions: [] });
        snowplowTracker.setCodeSuggestionsContext(
          SnowplowTracker.uniqueTrackingId(),
          mockSuggestion,
        );

        expect(mockTrackStructEvent.mock.calls.length).toEqual(1);
        expect(mockTrackStructEvent).toHaveBeenCalledWith(
          expect.objectContaining({
            action: TRACKING_EVENTS.REQUESTED,
            category: 'code_suggestions',
          }),
          expect.any(Array),
        );
      });
    });
  });
});
