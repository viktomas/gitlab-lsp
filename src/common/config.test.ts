import { GITLAB_API_BASE_URL } from './api';
import { ConfigProvider } from './config';
import { DEFAULT_TRACKING_ENDPOINT } from './tracking/snowplow_tracker';
import { LOG_LEVEL } from './log';

describe('ConfigProvider', () => {
  let configProvider: ConfigProvider;

  beforeEach(() => {
    configProvider = new ConfigProvider();
  });

  it('starts with default config', async () => {
    expect(configProvider.get()).toEqual({
      baseUrl: GITLAB_API_BASE_URL,
      codeCompletion: {
        enableSecretRedaction: true,
      },
      telemetry: {
        enabled: true,
        trackingUrl: DEFAULT_TRACKING_ENDPOINT,
      },
      logLevel: LOG_LEVEL.INFO,
      ignoreCertificateErrors: false,
      httpAgentOptions: {},
    });
  });

  it('can set a property', async () => {
    configProvider.set('baseUrl', 'http://test.url');

    expect(configProvider.get().baseUrl).toBe('http://test.url');
  });

  it('can merge config with a partial config', async () => {
    configProvider.merge({
      baseUrl: 'http://test.url',
      telemetry: { enabled: false },
    });

    expect(configProvider.get().baseUrl).toBe('http://test.url');
    expect(configProvider.get().telemetry).toEqual({ enabled: false });
  });
});
