import { SuggestionsCache } from './suggestions_cache';
import { ConfigProvider } from '../config';

describe('suggestions cache', () => {
  const mockEntry = {
    request: {
      document: { uri: 't1' },
      context: {
        filename: 'demo',
        prefix: `line-minus-2
        last-line-before-empty

        current = `,
        suffix: `line-plus-1
        line-plus-2`,
        position: { line: 20, character: 10 },
      },
      position: { line: 20, character: 10 },
    },
    suggestions: [
      { text: 'pending-text-1', uniqueTrackingId: 't1' },
      { text: 'other-pending-text-2', uniqueTrackingId: 't2' },
    ],
  };

  let suggestionsCache: SuggestionsCache;
  let configProvider: ConfigProvider;

  beforeEach(() => {
    configProvider = new ConfigProvider();
    configProvider.set('suggestionsCache', {
      enabled: true,
    });
    suggestionsCache = new SuggestionsCache(configProvider);

    suggestionsCache.addToSuggestionCache(mockEntry);
  });

  it('retrieves cached suggestions when input matches', () => {
    const suggestions = suggestionsCache.getCachedSuggestions(mockEntry.request);

    expect(suggestions).toHaveLength(2);
  });

  it('ignores suggestion when line number differs', () => {
    const suggestions = suggestionsCache.getCachedSuggestions({
      ...mockEntry.request,
      position: {
        ...mockEntry.request.position,
        line: mockEntry.request.position.line + 1,
      },
    });

    expect(suggestions).toHaveLength(0);
  });

  it('retrieves correct suggestions when input added matches', () => {
    const EXTRA = 'p';
    const suggestions = suggestionsCache.getCachedSuggestions({
      ...mockEntry.request,
      context: {
        ...mockEntry.request.context,
        prefix: `${mockEntry.request.context.prefix}${EXTRA}`,
      },
      position: {
        ...mockEntry.request.position,
        character: mockEntry.request.position.character + EXTRA.length,
      },
    });

    expect(suggestions).toHaveLength(1);
    expect(suggestions?.at(0)?.text).toBe('ending-text-1');
  });

  it('generates correct tracking id for telemetry purposes', () => {
    const EXTRA = 'p';
    const suggestions = suggestionsCache.getCachedSuggestions({
      ...mockEntry.request,
      context: {
        ...mockEntry.request.context,
        prefix: `${mockEntry.request.context.prefix}${EXTRA}`,
      },
      position: {
        ...mockEntry.request.position,
        character: mockEntry.request.position.character + EXTRA.length,
      },
    });

    expect(suggestions?.at(0)?.uniqueTrackingId).toBeDefined();
    expect(suggestions?.at(0)?.uniqueTrackingId).not.toBe('t1');
  });

  it('does not retrieves suggestions when input added matches but cache is disabled in config', () => {
    const EXTRA = 'p';

    configProvider.set('suggestionsCache', {
      enabled: false,
    });

    const suggestions = suggestionsCache.getCachedSuggestions({
      ...mockEntry.request,
      context: {
        ...mockEntry.request.context,
        prefix: `${mockEntry.request.context.prefix}${EXTRA}`,
      },
      position: {
        ...mockEntry.request.position,
        character: mockEntry.request.position.character + EXTRA.length,
      },
    });

    expect(suggestions).toHaveLength(0);
  });

  it('retrieves no suggestions when input different from all suggestions', () => {
    const EXTRA = 'n';
    const suggestions = suggestionsCache.getCachedSuggestions({
      ...mockEntry.request,
      context: {
        ...mockEntry.request.context,
        prefix: `${mockEntry.request.context.prefix}${EXTRA}`,
      },
      position: {
        ...mockEntry.request.position,
        character: mockEntry.request.position.character + EXTRA.length,
      },
    });

    expect(suggestions).toHaveLength(0);
  });

  it('retrieves suggestions when irrelevant line changed', () => {
    const suggestions = suggestionsCache.getCachedSuggestions({
      ...mockEntry.request,
      context: {
        ...mockEntry.request.context,
        prefix: `line-minus-2
        last-line-before-empty-RELEVANT-LINE-CHANGED

        current = p`,
      },
      position: {
        ...mockEntry.request.position,
        character: mockEntry.request.position.character + 1,
      },
    });

    expect(suggestions).toHaveLength(0);
  });

  it('discards suggestions when relevant line changed', () => {
    const suggestions = suggestionsCache.getCachedSuggestions({
      ...mockEntry.request,
      context: {
        ...mockEntry.request.context,
        prefix: `line-minus-2-IRRELEVANT_LINE-CHANGED
        last-line-before-empty

        current = p`,
      },
      position: {
        ...mockEntry.request.position,
        character: mockEntry.request.position.character + 1,
      },
    });

    expect(suggestions).toHaveLength(1);
    expect(suggestions?.at(0)?.text).toBe('ending-text-1');
  });

  it('drops suggestions if current line differs', () => {
    const suggestions = suggestionsCache.getCachedSuggestions({
      ...mockEntry.request,
      context: {
        ...mockEntry.request.context,
        prefix: `line-minus-2
        last-line-before-empty

        CHANGED = p`,
      },
      position: {
        ...mockEntry.request.position,
        character: mockEntry.request.position.character + 1,
      },
    });

    expect(suggestions).toHaveLength(0);
  });

  it('drops suggestions if current line differs', () => {
    const suggestions = suggestionsCache.getCachedSuggestions({
      ...mockEntry.request,
      context: {
        ...mockEntry.request.context,
        prefix: `line-minus-2
        last-line-before-empty

        CHANGED = p`,
      },
      position: {
        ...mockEntry.request.position,
        character: mockEntry.request.position.character + 1,
      },
    });

    expect(suggestions).toHaveLength(0);
  });

  it('drops suggestions if current line is shorter than cached', () => {
    const suggestions = suggestionsCache.getCachedSuggestions({
      ...mockEntry.request,
      context: {
        ...mockEntry.request.context,
        prefix: `line-minus-2
        last-line-before-empty

        cur`,
      },
      position: {
        ...mockEntry.request.position,
        character: mockEntry.request.position.character + 1,
      },
    });

    expect(suggestions).toHaveLength(0);
  });

  describe('for suggestions that are retrieved twice', () => {
    it('when the position is not the same, the entry is not deleted', () => {
      const requestWithDifferentPosition = {
        ...mockEntry.request,
        position: {
          ...mockEntry.request.position,
          character: mockEntry.request.position.character + 1,
        },
      };
      const firstSuggestion = suggestionsCache.getCachedSuggestions(mockEntry.request);
      const secondSuggestion = suggestionsCache.getCachedSuggestions(requestWithDifferentPosition);

      expect(firstSuggestion).toHaveLength(2);
      expect(secondSuggestion).toHaveLength(2);
    });

    it('when the position is the same, the entry is deleted', () => {
      const firstSuggestion = suggestionsCache.getCachedSuggestions(mockEntry.request);
      const secondSuggestion = suggestionsCache.getCachedSuggestions(mockEntry.request);

      expect(firstSuggestion).toHaveLength(2);
      expect(secondSuggestion).toHaveLength(0);
    });
  });
});
