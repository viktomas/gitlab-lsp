import { TextDocumentIdentifier, Position } from 'vscode-languageserver-protocol';
import { LRUCache } from 'lru-cache';
import { SnowplowTracker } from '../tracking/snowplow_tracker';
import { CodeSuggestionResponseChoice } from '../api';
import { IDocContext } from '../documents';
import { ConfigProvider, ISuggestionsCacheOptions } from '../config';

type TimesRetrievedByPosition = {
  [key: string]: number;
};

type SuggestionCacheEntry = {
  character: number;
  suggestions: CodeSuggestionResponseChoice[];
  currentLine: string;
  timesRetrievedByPosition: TimesRetrievedByPosition;
};

type SuggestionCacheContext = {
  document: TextDocumentIdentifier;
  context: IDocContext;
  position: Position;
};

function isNonEmptyLine(s: string) {
  return s.trim().length > 0;
}

type Required<T> = {
  [P in keyof T]-?: T[P];
};

const DEFAULT_CONFIG: Required<ISuggestionsCacheOptions> = {
  enabled: true,
  maxSize: 1024 * 1024,
  ttl: 60 * 1000,
  prefixLines: 1,
  suffixLines: 1,
};
export class SuggestionsCache {
  #cache: LRUCache<string, SuggestionCacheEntry>;
  #configProvider: ConfigProvider;

  constructor(configProvider: ConfigProvider) {
    this.#configProvider = configProvider;
    const currentConfig = this.#getCurrentConfig();

    this.#cache = new LRUCache<string, SuggestionCacheEntry>({
      ttlAutopurge: true,
      sizeCalculation: (value, key) =>
        value.suggestions.reduce((acc, suggestion) => acc + suggestion.text.length, 0) + key.length,
      ...DEFAULT_CONFIG,
      ...currentConfig,
      ttl: Number(currentConfig.ttl),
    });
  }

  #getCurrentConfig(): Required<ISuggestionsCacheOptions> {
    return {
      ...DEFAULT_CONFIG,
      ...(this.#configProvider.get().suggestionsCache ?? {}),
    };
  }

  #getSuggestionKey(ctx: SuggestionCacheContext) {
    const prefixLines = ctx.context.prefix.split('\n');

    const currentLine = prefixLines.pop() ?? '';
    const indentation = (currentLine.match(/^\s*/)?.[0] ?? '').length;

    const config = this.#getCurrentConfig();

    const cachedPrefixLines = prefixLines
      .filter(isNonEmptyLine)
      .slice(-config.prefixLines)
      .join('\n');
    const cachedSuffixLines = ctx.context.suffix
      .split('\n')
      .filter(isNonEmptyLine)
      .slice(0, config.suffixLines)
      .join('\n');

    return [
      ctx.document.uri,
      ctx.position.line,
      cachedPrefixLines,
      cachedSuffixLines,
      indentation,
    ].join(':');
  }

  #increaseCacheEntryRetrievedCount(suggestionKey: string, character: number) {
    const item = this.#cache.get(suggestionKey);
    if (item && item.timesRetrievedByPosition) {
      item.timesRetrievedByPosition[character] =
        (item.timesRetrievedByPosition[character] ?? 0) + 1;
    }
  }

  addToSuggestionCache(config: {
    request: SuggestionCacheContext;
    suggestions: CodeSuggestionResponseChoice[];
  }) {
    if (!this.#getCurrentConfig().enabled) {
      return;
    }

    const currentLine = config.request.context.prefix.split('\n').at(-1) ?? '';
    this.#cache.set(this.#getSuggestionKey(config.request), {
      character: config.request.position.character,
      suggestions: config.suggestions,
      currentLine,
      timesRetrievedByPosition: {},
    });
  }

  getCachedSuggestions(request: SuggestionCacheContext) {
    if (!this.#getCurrentConfig().enabled) {
      return [];
    }

    const currentLine = request.context.prefix.split('\n').at(-1) ?? '';
    const key = this.#getSuggestionKey(request);
    const candidate = this.#cache.get(key);

    if (!candidate) {
      return [];
    }

    const character = request.position.character;
    if (candidate.timesRetrievedByPosition[character] > 0) {
      // If cache has already returned this suggestion from the same position before, discard it.
      this.#cache.delete(key);

      return [];
    }

    this.#increaseCacheEntryRetrievedCount(key, character);

    return candidate.suggestions
      .map((s) => {
        const currentLength = currentLine.length;
        const previousLength = candidate.currentLine.length;
        const diff = currentLength - previousLength;

        if (diff < 0) {
          return null;
        }

        if (
          currentLine.slice(0, previousLength) !== candidate.currentLine ||
          s.text.slice(0, diff) !== currentLine.slice(previousLength)
        ) {
          return null;
        }

        return { text: s.text.slice(diff), uniqueTrackingId: SnowplowTracker.uniqueTrackingId() };
      })
      .filter((x): x is CodeSuggestionResponseChoice => Boolean(x));
  }
}
