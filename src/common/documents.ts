import { TextDocuments, WorkspaceFolder } from 'vscode-languageserver';
import { Position, TextDocument } from 'vscode-languageserver-textdocument';
import { sanitizeRange } from './utils/sanitize_range';
import { OptionalCompletionContext, isInlineCompletionContext } from './utils/type_utils';

export interface IDocContext {
  prefix: string;
  suffix: string;
  filename: string;
  position: Position;
}

export interface IDocTransformer {
  transform(context: IDocContext): IDocContext;
}

export class DocumentTransformer {
  constructor(
    private documents: TextDocuments<TextDocument>,
    private transformers: IDocTransformer[] = [],
  ) {}

  get(uri: string) {
    return this.documents.get(uri);
  }

  getContext(
    uri: string,
    position: Position,
    workspaceFolders: WorkspaceFolder[],
    completionContext: OptionalCompletionContext,
  ): IDocContext | undefined {
    const doc = this.get(uri);

    if (doc === undefined) {
      return;
    }

    return this.transform(getDocContext(doc, position, workspaceFolders, completionContext));
  }

  transform(context: IDocContext): IDocContext {
    for (const transformer of this.transformers) {
      context = transformer.transform(context);
    }

    return context;
  }
}

export function getDocContext(
  document: TextDocument,
  position: Position,
  workspaceFolders: WorkspaceFolder[],
  completionContext: OptionalCompletionContext,
): IDocContext {
  let prefix;

  if (isInlineCompletionContext(completionContext) && completionContext.selectedCompletionInfo) {
    const { selectedCompletionInfo } = completionContext;
    const range = sanitizeRange(selectedCompletionInfo.range);

    prefix = `${document.getText({
      start: document.positionAt(0),
      end: range.start,
    })}${selectedCompletionInfo.text}`;
  } else {
    prefix = document.getText({ start: document.positionAt(0), end: position });
  }

  const suffix = document.getText({
    start: position,
    end: document.positionAt(document.getText().length),
  });

  let filename = document.uri;
  for (const { uri } of workspaceFolders) {
    if (filename.startsWith(uri)) {
      filename = filename.slice(uri.length).replace(/^\//, '');
      break;
    }
  }

  return {
    prefix,
    suffix,
    filename,
    position,
  };
}
