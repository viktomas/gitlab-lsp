import fetch from 'cross-fetch';
import { GitLabAPI, CodeSuggestionResponse } from './api';
import { CODE_SUGGESTIONS_RESPONSE, FILE_INFO } from './test_utils/mocks';
import { FetchBase, IFetch } from './fetch';
import { createFakeResponse } from './test_utils/create_fake_response';

jest.mock('cross-fetch');

const TEST_CODE_SUGGESTION_REQUEST = {
  prompt_version: 1,
  project_path: '',
  project_id: -1,
  current_file: {
    content_above_cursor: FILE_INFO.prefix,
    content_below_cursor: FILE_INFO.suffix,
    file_name: FILE_INFO.filename,
  },
};

describe('GitLabAPI', () => {
  let gitLabAPI: GitLabAPI;
  const lsFetch: IFetch = new FetchBase();
  const token = 'glpat-1234';
  const nonGitLabBaseUrl = 'http://test.com';
  const gitlabBaseUrl = 'https://gitlab.com';
  const clientInfo = { name: 'MyClient', version: '1.0.0' };

  afterEach(() => {
    (fetch as jest.Mock).mockClear();
  });

  describe('getCodeSuggestions', () => {
    const api = new GitLabAPI(lsFetch, gitlabBaseUrl, token);

    describe('Error path', () => {
      beforeEach(async () => {
        await api.configureApi({ baseUrl: gitlabBaseUrl, token: '' });
      });

      it('should throw an error when no token provided', async () => {
        await expect(api.getCodeSuggestions(TEST_CODE_SUGGESTION_REQUEST)).rejects.toThrow(
          /Token needs to be provided to request Code Suggestions/,
        );
      });
    });

    describe('Success path', () => {
      let response: CodeSuggestionResponse | undefined;

      beforeEach(async () => {
        api.setClientInfo({ name: clientInfo.name, version: clientInfo.version });
        await api.configureApi({ baseUrl: gitlabBaseUrl, token });
        (fetch as jest.Mock).mockResolvedValueOnce(
          createFakeResponse({ status: 200, json: CODE_SUGGESTIONS_RESPONSE }),
        );

        response = await api.getCodeSuggestions(TEST_CODE_SUGGESTION_REQUEST);
      });

      it('should make a request for the code suggestions', () => {
        const [url, params] = (fetch as jest.Mock).mock.calls[0];

        expect(url).toBe(`${gitlabBaseUrl}/api/v4/code_suggestions/completions`);

        expect(params.headers).toMatchObject({
          Authorization: `Bearer ${token}`,
          'User-Agent': `code-completions-language-server-experiment (${clientInfo.name}:${clientInfo?.version})`,
          'Content-Type': 'application/json',
        });
      });

      it('should return code suggestions', async () => {
        expect(response).toEqual({ ...CODE_SUGGESTIONS_RESPONSE, status: 200 });
      });
    });
  });

  describe('checkToken', () => {
    const gitlabPAT = 'glpat-abcdefghijklmno12345';
    const gitlabPATWithNonDefaultPrefix = 'test-abcdefghijklmno12345';
    const oauthToken = 'abcdefghijklmno12345abcdefghijklmno12345abcdefghijklmno12345';

    beforeEach(() => {
      (fetch as jest.Mock).mockReset();
    });

    describe.each`
      tokenType       | PAT
      ${'gitlab'}     | ${gitlabPAT}
      ${'non-gitlab'} | ${gitlabPATWithNonDefaultPrefix}
    `('$tokenType PAT Token', ({ PAT }) => {
      beforeEach(() => {
        gitLabAPI = new GitLabAPI(lsFetch, gitlabBaseUrl, PAT);
      });

      it('should make a request to check the token', async () => {
        gitLabAPI.setClientInfo({ name: clientInfo.name, version: clientInfo.version });
        await gitLabAPI.checkToken(PAT);

        expect(fetch).toHaveBeenCalledWith(`${gitlabBaseUrl}/api/v4/personal_access_tokens/self`, {
          headers: {
            Authorization: `Bearer ${PAT}`,
            'User-Agent': `code-completions-language-server-experiment (${clientInfo.name}:${clientInfo?.version})`,
          },
          method: 'GET',
        });
      });

      it('should return correct message and reason when token is not active', async () => {
        (fetch as jest.Mock).mockResolvedValueOnce({
          ok: true,
          json: () => ({ active: false, scopes: ['read_user'] }),
        });

        const { reason, message } = await gitLabAPI.checkToken(PAT);

        expect(reason).toBe('not_active');
        expect(message).toBe('Token is not active.');
      });

      it('should return correct message and reason when token does not have enough scopes', async () => {
        (fetch as jest.Mock).mockResolvedValueOnce({
          ok: true,
          json: () => ({ active: true, scopes: ['read_api'] }),
        });

        const { reason, message } = await gitLabAPI.checkToken(PAT);

        expect(reason).toBe('invalid_scopes');
        expect(message).toBe(`Token has scope(s) 'read_api' (needs 'api').`);
      });

      it('should return correct message and reason when token check failed', async () => {
        (fetch as jest.Mock).mockRejectedValueOnce('Request failed.');

        const { reason, message } = await gitLabAPI.checkToken(PAT);

        expect(reason).toBe('unknown');
        expect(message).toBe('Failed to check token: Request failed.');
      });

      it('should return that token is valid when it is active and has correct scopes', async () => {
        (fetch as jest.Mock).mockResolvedValueOnce({
          ok: true,
          json: () => ({ active: true, scopes: ['api', 'read_user', 'read_api'] }),
        });

        const { reason, message, valid } = await gitLabAPI.checkToken(PAT);

        expect(valid).toBe(true);
        expect(reason).toBeUndefined();
        expect(message).toBeUndefined();
      });
    });

    describe('OAuth Token', () => {
      it('should make a request to check the token', async () => {
        gitLabAPI.setClientInfo({ name: clientInfo.name, version: clientInfo.version });
        await gitLabAPI.checkToken(oauthToken);

        expect(fetch).toHaveBeenCalledWith(`${gitlabBaseUrl}/oauth/token/info`, {
          headers: {
            Authorization: `Bearer ${oauthToken}`,
            'User-Agent': `code-completions-language-server-experiment (${clientInfo.name}:${clientInfo?.version})`,
          },
          method: 'GET',
        });
      });

      it('should return correct message and reason when token does not have enough scopes', async () => {
        (fetch as jest.Mock).mockResolvedValueOnce({
          ok: true,
          json: () => ({ scope: ['read_user'] }),
        });

        const { reason, message } = await gitLabAPI.checkToken(oauthToken);

        expect(reason).toBe('invalid_scopes');
        expect(message).toBe(`Token has scope(s) 'read_user' (needs 'api').`);
      });

      it('should return correct message and reason when token check failed', async () => {
        (fetch as jest.Mock).mockRejectedValueOnce('Request failed.');

        const { reason, message } = await gitLabAPI.checkToken(oauthToken);

        expect(reason).toBe('unknown');
        expect(message).toBe('Failed to check token: Request failed.');
      });

      it('should return that token is valid when it has correct scopes', async () => {
        (fetch as jest.Mock).mockResolvedValueOnce({
          ok: true,
          json: () => ({ scope: ['api', 'read_user', 'read_api'] }),
        });

        const { reason, message, valid } = await gitLabAPI.checkToken(oauthToken);

        expect(valid).toBe(true);
        expect(reason).toBeUndefined();
        expect(message).toBeUndefined();
      });
    });
  });

  describe('getStreamingCodeSuggestions', () => {
    const original_fetch = global.fetch;

    beforeEach(() => {
      global.fetch = jest.fn();
    });

    afterAll(() => {
      global.fetch = original_fetch;
    });

    it('throws an error when token is not provided', async () => {
      gitLabAPI = new GitLabAPI(lsFetch, nonGitLabBaseUrl, undefined);
      const generator = gitLabAPI.getStreamingCodeSuggestions(TEST_CODE_SUGGESTION_REQUEST);
      await expect(generator.next).rejects.toThrow();
    });

    it('when status is not 200 throws error', async () => {
      (global.fetch as jest.Mock).mockResolvedValueOnce(createFakeResponse({ status: 500 }));

      gitLabAPI = new GitLabAPI(lsFetch, nonGitLabBaseUrl, 'test');
      const generator = gitLabAPI.getStreamingCodeSuggestions(TEST_CODE_SUGGESTION_REQUEST);
      await expect(generator.next).rejects.toThrow();
    });

    it('generator completes when done', async () => {
      (global.fetch as jest.Mock).mockResolvedValueOnce(createFakeResponse({ status: 200 }));

      gitLabAPI = new GitLabAPI(lsFetch, nonGitLabBaseUrl, 'test');
      const generator = gitLabAPI.getStreamingCodeSuggestions(TEST_CODE_SUGGESTION_REQUEST);
      const res = await generator.next();
      expect(res.done).toBe(true);
    });

    it('generator returns multiple chunks from the stream', async () => {
      const read = jest.fn();

      (global.fetch as jest.Mock).mockResolvedValueOnce({
        status: 200,
        ok: true,
        body: {
          getReader: jest.fn(() => ({
            read: read,
          })),
        },
      });

      gitLabAPI = new GitLabAPI(lsFetch, nonGitLabBaseUrl, 'test');
      const generator = gitLabAPI.getStreamingCodeSuggestions(TEST_CODE_SUGGESTION_REQUEST);
      read.mockResolvedValue({ done: false, value: Buffer.from('test1') });
      let res = await generator.next();
      expect(res.done).toBe(false);
      expect(res.value).toBe('test1');
      read.mockResolvedValue({ done: false, value: Buffer.from('test2') });
      res = await generator.next();
      expect(res.done).toBe(false);
      expect(res.value).toBe('test1test2');
      read.mockResolvedValue({ done: true, value: null });
      res = await generator.next();
      expect(res.done).toBe(true);
    });
  });
});
