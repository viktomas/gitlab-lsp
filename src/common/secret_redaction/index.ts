import rules, { IGitleaksRule } from './gitleaks_rules';
import { convertToJavaScriptRegex } from './helpers';
import { IDocContext, IDocTransformer } from '../../common/documents';
import { ConfigProvider } from '../config';

export class SecretRedactor implements IDocTransformer {
  #rules: IGitleaksRule[] = [];
  #configProvider: ConfigProvider;

  constructor(configProvider: ConfigProvider) {
    this.#rules = rules.map((rule) => ({
      ...rule,
      compiledRegex: new RegExp(convertToJavaScriptRegex(rule.regex), 'gmi'),
    }));

    this.#configProvider = configProvider;
  }

  transform(context: IDocContext): IDocContext {
    if (this.#configProvider.get().codeCompletion?.enableSecretRedaction) {
      return {
        prefix: this.redactSecrets(context.prefix),
        suffix: this.redactSecrets(context.suffix),
        filename: context.filename,
        position: context.position,
      };
    }

    return context;
  }

  redactSecrets(raw: string): string {
    return this.#rules.reduce((redacted: string, rule: IGitleaksRule) => {
      return this.#redactRuleSecret(redacted, rule);
    }, raw);
  }

  #redactRuleSecret(str: string, rule: IGitleaksRule): string {
    if (!rule.compiledRegex) return str;

    if (!this.#keywordHit(rule, str)) {
      return str;
    }

    const matches = [...str.matchAll(rule.compiledRegex)];

    return matches.reduce((redacted: string, match: RegExpMatchArray) => {
      const secret = match[rule.secretGroup ?? 0];
      return redacted.replace(secret, '*'.repeat(secret.length));
    }, str);
  }

  #keywordHit(rule: IGitleaksRule, raw: string) {
    if (!rule.keywords?.length) {
      return true;
    }

    for (const keyword of rule.keywords) {
      if (raw.toLowerCase().includes(keyword)) {
        return true;
      }
    }

    return false;
  }
}
