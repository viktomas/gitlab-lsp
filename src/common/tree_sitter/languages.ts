export interface TreeSitterLanguage {
  name: string;
  extensions: string[];
  wasmPath: string;
}
