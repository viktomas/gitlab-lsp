import Parser = require('web-tree-sitter');
import { SuggestionContext } from '../suggestion_client';
import { TreeSitterLanguage } from './languages';
import { log } from '../log';

export enum TreeSitterParserLoadState {
  INIT = 'init',
  ERRORED = 'errored',
  READY = 'ready',
  UNIMPLEMENTED = 'unimplemented',
}

export type Intent = 'completion' | 'generation';

const findLastNonEmptyLineIndex = (lines: Array<string>, startLine: number): number => {
  for (let i = startLine; i >= 0; i--) {
    if (lines[i]?.trim().length > 0) {
      return i;
    }
  }

  return -1;
};

export interface Grammar {
  name: string;
  parser: Parser;
}

export abstract class TreeSitterParser {
  protected loadState: TreeSitterParserLoadState;
  protected languages: TreeSitterLanguage[];
  protected readonly parsers: Record<string, Parser>;

  abstract init(): Promise<void>;

  constructor({ languages }: { languages: TreeSitterLanguage[] }) {
    this.languages = languages;
    this.loadState = TreeSitterParserLoadState.INIT;
    this.parsers = {};
  }

  async getIntent(context: SuggestionContext): Promise<Intent | undefined> {
    let grammar: Grammar | undefined;
    try {
      if (this.loadState === TreeSitterParserLoadState.INIT) {
        await this.init();
      }

      grammar = await this.findGrammar(context.document.filename);
    } catch (err) {
      log.warn('TreeSitterParser: Error initializing an appropriate tree-sitter parser', err);
      this.loadState = TreeSitterParserLoadState.ERRORED;
      return;
    }

    let intent: Intent | undefined;
    if (!grammar?.parser) {
      log.debug(
        `TreeSitterParser: Skipping intent detection using tree-sitter due to missing parser.`,
      );
      return intent;
    }

    const { parser } = grammar;
    if (grammar.name === 'java') {
      let nodeAtPosition = parser.parse(context.document.prefix).rootNode.lastNamedChild;
      while (nodeAtPosition !== null) {
        const body = nodeAtPosition.childForFieldName('body');
        if (body?.lastNamedChild) {
          nodeAtPosition = body.lastNamedChild;
        } else {
          break;
        }
      }

      if (nodeAtPosition?.type === 'block_comment' || nodeAtPosition?.type === 'line_comment') {
        intent = 'generation';
      } else {
        intent = 'completion';
      }
    } else {
      const { prefix, position } = context.document;
      const lines = prefix.split('\n');
      const lastNonEmptyLineIndex = findLastNonEmptyLineIndex(lines, position.line);
      const distance = position.line - lastNonEmptyLineIndex;
      const nodeAtPosition = parser.parse(lines[lastNonEmptyLineIndex] || '').rootNode
        .firstNamedChild;
      if (nodeAtPosition?.type === 'comment' && distance >= 1 && distance < 3) {
        intent = 'generation';
      } else {
        intent = 'completion';
      }
    }

    log.debug(`TreeSitterParser: Detected intent ${intent} using tree-sitter with context.`);
    return intent;
  }

  private async findGrammar(filename: string): Promise<Grammar | undefined> {
    const grammar = this.languages.find((language) =>
      language.extensions.find((ext) => filename.endsWith(ext)),
    );
    if (!grammar) {
      return;
    }
    if (this.parsers[grammar.name]) {
      return {
        name: grammar.name,
        parser: this.parsers[grammar.name],
      };
    }

    try {
      const parser = new Parser();
      const language = await Parser.Language.load(grammar.wasmPath);
      parser.setLanguage(language);
      this.parsers[grammar.name] = parser;
      log.debug(
        `TreeSitterParser: Loaded tree-sitter grammar (tree-sitter-${grammar.name}.wasm present).`,
      );
      return {
        name: grammar.name,
        parser,
      };
    } catch (err) {
      // NOTE: We validate the below is not present in generation.test.ts integration test.
      //       Make sure to update the test appropriately if changing the error.
      log.warn(
        'TreeSitterParser: Unable to load tree-sitter grammar due to an unexpected error.',
        err,
      );
      return undefined;
    }
  }
}
