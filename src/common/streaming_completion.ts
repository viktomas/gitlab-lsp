import { once } from 'lodash';
import { CancellationTokenSource, Connection, NotificationType } from 'vscode-languageserver';
import { IDocContext } from './documents';
import { CodeSuggestionRequest, IGitLabAPI } from './api';
import { log } from './log';
import { CircuitBreaker } from './circuit_breaker/circuit_breaker';
import { SnowplowTracker, TRACKING_EVENTS } from './tracking/snowplow_tracker';
import { isFetchError } from './fetch_error';

export interface StreamWithId {
  /** unique stream ID */
  id: string;
}
export interface StreamingCompletionResponse {
  /** stream ID taken from the request, all stream responses for one request will have the request's stream ID */
  id: string;
  /** most up-to-date generated suggestion, each time LS receives a chunk from LLM, it adds it to this string -> the client doesn't have to join the stream */
  completion?: string;
  done: boolean;
}

export const STREAMING_COMPLETION_RESPONSE_NOTIFICATION = 'streamingCompletionResponse';
export const CANCEL_STREAMING_COMPLETION_NOTIFICATION = 'cancelStreaming';
export const StreamingCompletionResponse = new NotificationType<StreamingCompletionResponse>(
  STREAMING_COMPLETION_RESPONSE_NOTIFICATION,
);
export const CancelStreaming = new NotificationType<StreamWithId>(
  CANCEL_STREAMING_COMPLETION_NOTIFICATION,
);

export const startStreaming = async (
  streamId: string,
  context: IDocContext,
  connection: Connection,
  api: IGitLabAPI,
  circuitBreaker: CircuitBreaker,
  tracker: SnowplowTracker,
  uniqueTrackingId: string,
) => {
  tracker.setCodeSuggestionsContext(uniqueTrackingId, context, 'network', true);

  let streamShown = false;
  let suggestionProvided = false;
  let streamCancelledOrErrored = false;

  const cancellationTokenSource = new CancellationTokenSource();
  const disposeStopStreaming = connection.onNotification(CancelStreaming, (stream) => {
    if (stream.id === streamId) {
      streamCancelledOrErrored = true;
      cancellationTokenSource.cancel();

      if (streamShown) {
        tracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.REJECTED);
      } else {
        tracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.CANCELLED);
      }
    }
  });
  const cancellationToken = cancellationTokenSource.token;

  const endStream = async () => {
    await connection.sendNotification(StreamingCompletionResponse, {
      id: streamId,
      done: true,
    });

    if (!streamCancelledOrErrored) {
      tracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.STREAM_COMPLETED);

      if (!suggestionProvided) {
        tracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.NOT_PROVIDED);
      }
    }
  };

  circuitBreaker.close();

  const request: CodeSuggestionRequest = {
    prompt_version: 1,
    project_path: '',
    project_id: -1,
    current_file: {
      content_above_cursor: context.prefix,
      content_below_cursor: context.suffix,
      file_name: context.filename,
    },
    intent: 'generation',
    stream: true,
  };

  const trackStreamStarted = once(() => {
    tracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.STREAM_STARTED);
  });

  const trackStreamShown = once(() => {
    tracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.SHOWN);
    streamShown = true;
  });

  try {
    for await (const response of api.getStreamingCodeSuggestions(request)) {
      if (cancellationToken.isCancellationRequested) {
        break;
      }

      if (circuitBreaker.isOpen()) {
        break;
      }

      trackStreamStarted();

      await connection.sendNotification(StreamingCompletionResponse, {
        id: streamId,
        completion: response,
        done: false,
      });

      if (response.replace(/\s/g, '').length) {
        trackStreamShown();
        suggestionProvided = true;
      }
    }
  } catch (err) {
    circuitBreaker.error();

    if (isFetchError(err)) {
      tracker.updateCodeSuggestionsContext(uniqueTrackingId, { status: err.status });
    }
    tracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.ERRORED);
    streamCancelledOrErrored = true;

    log.error('Error streaming code suggestions.', err);
  } finally {
    await endStream();
    disposeStopStreaming.dispose();
    cancellationTokenSource.dispose();
  }
};
