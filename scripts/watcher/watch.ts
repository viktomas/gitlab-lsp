import chokidar from 'chokidar';
import { resolve as resolvePath } from 'node:path';
import { performance } from 'node:perf_hooks';
import { cwd } from 'node:process';
import { runScript } from './run_script';

const textColor = '\x1b[36m%s\x1b[0m';
const errorColor = '\x1b[31m%s\x1b[0m';
const successColor = '\x1b[32m%s\x1b[0m';

async function runBuild() {
  try {
    await Promise.all([runScript('npm run compile'), runScript('npm run bundle:desktop')]);
  } catch (e) {
    console.error(errorColor, 'Script execution failed');
    console.error(e);
    return false;
  }
  console.log(successColor, 'Compiled successfully');
  return true;
}

async function postBuildVsCode() {
  const commandCwd = resolvePath(
    cwd(),
    process.env.VSCODE_EXTENSION_RELATIVE_PATH ?? '../gitlab-vscode-extension',
  );
  try {
    await runScript('npx yalc@1.0.0-pre.53 publish');
    console.log(successColor, 'Pushed to yalc');
    await runScript('npx yalc@1.0.0-pre.53 add @gitlab-org/gitlab-lsp', commandCwd);
  } catch (error) {
    console.error(errorColor, 'Whoops, something went wrong...');
    console.error(error);
  }
}

async function postBuild() {
  const editor = process.argv.find((arg) => arg.startsWith('--editor='))?.split('=')[1];
  if (editor === 'vscode') {
    return postBuildVsCode();
  }
  console.warn('No editor specified, skipping post-build steps');
}

async function run() {
  const watchMsg = () => console.log(textColor, `Watching for file changes on ${dir}`);
  const now = performance.now();
  const buildSuccessful = await runBuild();
  if (!buildSuccessful) return watchMsg();
  await postBuild();
  console.log(successColor, `Finished in ${Math.round(performance.now() - now)}ms`);
  watchMsg();
}

let timeout: NodeJS.Timeout | null = null;

const dir = './src';

const watcher = chokidar.watch(dir, {
  ignored: /(^|[/\\])\../, // ignore dotfiles
  persistent: true,
});

let isReady = false;

watcher
  .on('add', async (path) => {
    if (!isReady) return;
    console.log(`File ${path} has been added`);
    await run();
  })
  .on('change', async (path) => {
    if (!isReady) return;
    console.log(`File ${path} has been changed`);
    if (timeout) clearTimeout(timeout);
    // We debounce the run function on change events in the case of many files being changed at once
    timeout = setTimeout(async () => {
      await run();
    }, 1000);
  })
  .on('ready', async () => {
    await run();
    isReady = true;
  })
  .on('unlink', (path) => console.log(`File ${path} has been removed`));

console.log(textColor, 'Starting watcher...');
