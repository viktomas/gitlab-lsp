import { execa } from 'execa';

export async function runScript(script: string, dir?: string): Promise<void> {
  const [file, ...args] = script.split(' ');

  const { exitCode } = await execa(file, args, {
    env: { ...process.env, FORCE_COLOR: 'true' },
    stdio: 'inherit', // Inherit stdio to preserve color and formatting
    cwd: dir,
    extendEnv: true,
    shell: true,
  });

  if (exitCode !== 0) {
    throw new Error(`Script ${script} exited with code ${exitCode}`);
  }
}
