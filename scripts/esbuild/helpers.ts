import { readdirSync, readFileSync } from 'node:fs';
import { extname } from 'node:path';
import type { Loader, Plugin } from 'esbuild';

const wasmEntryPoints = () => {
  const files = readdirSync('vendor/grammars').reduce(
    (acc, file) => {
      if (file.endsWith('.wasm')) {
        acc.push({
          in: `vendor/grammars/${file}`,
          out: `vendor/grammars/${file.replace(/\.wasm$/, '')}`,
        });
      }
      return acc;
    },
    [{ in: 'node_modules/web-tree-sitter/tree-sitter.wasm', out: 'node/tree-sitter' }],
  );

  return files;
};

const fixVendorGrammarsPlugin = {
  name: 'fixVendorGrammars',
  setup(build) {
    build.onLoad({ filter: /.*\/tree_sitter\/languages\.ts/ }, ({ path: filePath }) => {
      if (!filePath.match(/.*\/?node_modules\/.*?/)) {
        let contents = readFileSync(filePath, 'utf8');
        contents = contents.replaceAll(
          "path.join(__dirname, '../../../vendor/grammars",
          "path.join(__dirname, '../vendor/grammars",
        );
        return {
          contents,
          loader: extname(filePath).substring(1) as Loader,
        };
      }
      return null;
    });
  },
} satisfies Plugin;
export { fixVendorGrammarsPlugin, wasmEntryPoints };
