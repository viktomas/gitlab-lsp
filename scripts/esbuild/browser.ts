import * as esbuild from 'esbuild';
import { fixVendorGrammarsPlugin, wasmEntryPoints } from './helpers';

const config: esbuild.BuildOptions = {
  bundle: true,
  entryPoints: [...wasmEntryPoints(), { in: 'src/browser/main.ts', out: 'browser/main-bundle' }],
  external: ['fs', 'path'],
  loader: { '.wasm': 'copy' },
  logLevel: 'info',
  minify: true,
  outbase: '.',
  outdir: 'out',
  platform: 'browser',
  plugins: [fixVendorGrammarsPlugin],
  sourcemap: true,
  target: 'node18.17',
};

async function build() {
  await esbuild.build(config);
}
void build();
