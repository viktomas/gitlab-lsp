import * as esbuild from 'esbuild';
import { fixVendorGrammarsPlugin, wasmEntryPoints } from './helpers';

const config: esbuild.BuildOptions = {
  bundle: true,
  entryNames: '[dir]/[name]',
  entryPoints: [{ in: 'src/node/main.ts', out: 'node/main-bundle' }, ...wasmEntryPoints()],
  loader: { '.wasm': 'copy' },
  logLevel: 'info',
  minify: true,
  outbase: '.',
  outdir: 'out',
  platform: 'node',
  plugins: [fixVendorGrammarsPlugin],
  sourcemap: true,
  target: 'node18.17',
};

async function build() {
  await esbuild.build(config);
}
void build();
